// 'use client';
// import { useState, useEffect } from 'react';
// import { getAllFilteredProjects } from '@/api/properties';
// import useFilterStore from '@/store/filterStore';
// import OffersList from '@/components/properties-page/OffersList';

// const PropertiesList = () => {
//   const [propertiesData, setPropertiesData] = useState<any>([]);
//   const [isLoading, setIsLoading] = useState<boolean>(true);
//   const [visibleProjects, setVisibleProjects] = useState(24);
//   const [allProjectsLength, setAllProjectsLength] = useState<any>(0);
//   const {
//     selectedRadio,
//     searchParams,
//     selectedType,
//     selectedBeds,
//     selectedPrice,
//     selectedSize,
//     selectedArea,
//     tempArea,
//     reset,
//     page,
//     setPage,
//     polygonData,
//   } = useFilterStore();

//   // useEffect(() => {
//   //   if (
//   //     !tempArea ||
//   //     !selectedRadio ||
//   //     !selectedBeds ||
//   //     !selectedType ||
//   //     !selectedSize ||
//   //     !selectedPrice
//   //   ) {
//   //     return;
//   //   }

//   //   const fetchData = async () => {
//   //     try {
//   //       setIsLoading(true);

//   //       let newCompletion = ['Ready'];

//   //       if (selectedRadio === 'newBuilding') {
//   //         newCompletion.push('Off plan');
//   //       } else if (selectedRadio === 'specialOffer') {
//   //         newCompletion.push('Special Offer');
//   //       } else if (!selectedRadio) {
//   //         newCompletion.push('Off plan', 'Special Offer');
//   //       }

//   //       const data = {
//   //         bedrooms:
//   //           selectedBeds.length > 0 ? selectedBeds : ['Studio', '1', '2', '3', '4', '5', '6', '7'],
//   //         completion: newCompletion,
//   //         location: '',
//   //         type: selectedType.length
//   //           ? selectedType
//   //           : ['Apartment', 'Townhouse', 'Villa', 'Duplex', 'Flat', 'Penthouse', 'Ground floor'],
//   //         priceFrom: selectedPrice.min ? selectedPrice.min : 0,
//   //         priceTo: selectedPrice.max ? selectedPrice.max : 999999999,
//   //         areas: tempArea,
//   //         sizeFrom: selectedSize.min ? selectedSize.min : 0,
//   //         sizeTo: selectedSize.max ? selectedSize.max : 99999999999,
//   //         namePrefix: searchParams.length > 0 ? searchParams : '',
//   //       };

//   //       const response = await getAllFilteredProjects(data, page, visibleProjects);

//   //       const convertedData = response.projects.map((project: any) => ({
//   //         ...project,
//   //         projectId: project._id,
//   //       }));

//   //       console.log(`setPropertiesData: ${convertedData}`);
//   //       setPropertiesData(convertedData);
//   //       setIsLoading(false);
//   //       setPage(response.currentPage);
//   //       setAllProjectsLength(response.totalPages);
//   //     } catch (error) {
//   //       console.error('Failed to fetch data', error);
//   //     } finally {
//   //       setIsLoading(false);
//   //     }
//   //   };

//   //   fetchData();
//   // }, [
//   //   tempArea,
//   //   selectedRadio,
//   //   selectedBeds,
//   //   selectedType,
//   //   selectedSize,
//   //   selectedPrice,
//   //   searchParams,
//   //   page,
//   //   reset,
//   // ]);

//   const [requestId, setRequestId] = useState(0);

//   useEffect(() => {
//     if (
//       !tempArea ||
//       !selectedRadio ||
//       !selectedBeds ||
//       !selectedType ||
//       !selectedSize ||
//       !selectedPrice
//     ) {
//       return;
//     }

//     const fetchData = async (currentRequestId: any) => {
//       try {
//         setIsLoading(true);

//         let newCompletion = ['Ready'];
//         if (selectedRadio === 'newBuilding') {
//           newCompletion.push('Off plan');
//         } else if (selectedRadio === 'specialOffer') {
//           newCompletion.push('Special Offer');
//         } else if (!selectedRadio) {
//           newCompletion.push('Off plan', 'Special Offer');
//         }

//         const data = {
//           bedrooms:
//             selectedBeds.length > 0 ? selectedBeds : ['Studio', '1', '2', '3', '4', '5', '6', '7'],
//           completion: newCompletion,
//           location: '',
//           type: selectedType.length
//             ? selectedType
//             : ['Apartment', 'Townhouse', 'Villa', 'Duplex', 'Flat', 'Penthouse', 'Ground floor'],
//           priceFrom: selectedPrice.min ? selectedPrice.min : 0,
//           priceTo: selectedPrice.max ? selectedPrice.max : 999999999,
//           areas: tempArea,
//           sizeFrom: selectedSize.min ? selectedSize.min : 0,
//           sizeTo: selectedSize.max ? selectedSize.max : 99999999999,
//           namePrefix: searchParams.length > 0 ? searchParams : '',
//         };

//         const response = await getAllFilteredProjects(data, page, visibleProjects);
//         const convertedData = response.projects.map((project: any) => ({
//           ...project,
//           projectId: project._id,
//         }));

//         // Проверяем, что текущий запрос - последний
//         if (currentRequestId === requestId) {
//           console.log(`setPropertiesData: ${convertedData}`);
//           setPropertiesData(convertedData);
//           setPage(response.currentPage);
//           setAllProjectsLength(response.totalPages);
//         }
//         setIsLoading(false);
//       } catch (error) {
//         console.error('Failed to fetch data', error);
//         setIsLoading(false);
//       }
//     };

//     const currentRequestId = requestId + 1;
//     setRequestId(currentRequestId);
//     fetchData(currentRequestId);
//   }, [
//     tempArea,
//     selectedRadio,
//     selectedBeds,
//     selectedType,
//     selectedSize,
//     selectedPrice,
//     searchParams,
//     page,
//     reset,
//   ]);

//   useEffect(() => {
//     window.scrollTo(0, 0);
//   }, [page]);

//   return (
//     <OffersList
//       hideButtons={false}
//       showMessage={false && propertiesData && propertiesData.length > 0}
//       offersData={propertiesData}
//       isLoading={isLoading}
//       visibleProjects={visibleProjects}
//       allProjectsLength={allProjectsLength}
//     />
//   );
// };

// export default PropertiesList;

'use client';
import { useState, useEffect } from 'react';
import { getAllFilteredProjects } from '@/api/properties';
import useFilterStore from '@/store/filterStore';
import OffersList from '@/components/properties-page/OffersList';

const PropertiesList = () => {
  const [propertiesData, setPropertiesData] = useState([]);
  const [isLoading, setIsLoading] = useState(true);
  const [visibleProjects, setVisibleProjects] = useState(24);
  const [allProjectsLength, setAllProjectsLength] = useState(0);
  const [showMessage, setShowMessage] = useState<boolean>(false)
  const {
    selectedRadio,
    searchParams,
    selectedType,
    selectedBeds,
    selectedPrice,
    selectedSize,
    selectedArea,
    tempArea,
    reset,
    page,
    setPage,
    polygonData,
  } = useFilterStore();

  useEffect(() => {
    let activeRequest = true;

    const fetchData = async () => {
      if (
        !tempArea ||
        !selectedRadio ||
        !selectedBeds ||
        !selectedType ||
        !selectedSize ||
        !selectedPrice
      ) {
        setIsLoading(false);
        return;
      }

      try {
        setIsLoading(true);

        let newCompletion = ['Ready'];
        if (selectedRadio === 'newBuilding') {
          newCompletion.push('Off plan');
        } else if (selectedRadio === 'specialOffer') {
          newCompletion.push('Special Offer');
        } else if (!selectedRadio) {
          newCompletion.push('Off plan', 'Special Offer');
        }

        const data = {
          bedrooms:
            selectedBeds.length > 0 ? selectedBeds : ['Studio', '1', '2', '3', '4', '5', '6', '7'],
          completion: newCompletion,
          location: '',
          type: selectedType.length
            ? selectedType
            : ['Apartment', 'Townhouse', 'Villa', 'Duplex', 'Flat', 'Penthouse', 'Ground floor'],
          priceFrom: selectedPrice.min ? selectedPrice.min : 0,
          priceTo: selectedPrice.max ? selectedPrice.max : 999999999,
          areas: tempArea,
          sizeFrom: selectedSize.min ? selectedSize.min : 0,
          sizeTo: selectedSize.max ? selectedSize.max : 99999999999,
          namePrefix: searchParams.length > 0 ? searchParams : '',
        };

        const response = await getAllFilteredProjects(data, page, visibleProjects);

        if (activeRequest) {
          const convertedData = response.projects.map((project: any) => ({
            ...project,
            projectId: project._id,
          }));
          setPropertiesData(convertedData);
          setPage(response.currentPage);
          setAllProjectsLength(response.totalPages);
          setShowMessage(!response.showFilteredProjects)
        }
      } catch (error) {
        console.error('Failed to fetch data', error);
      } finally {
        setIsLoading(false);
      }
    };

    fetchData();

    return () => {
      activeRequest = false; 
    };
  }, [
    tempArea,
    selectedRadio,
    selectedBeds,
    selectedType,
    selectedSize,
    selectedPrice,
    searchParams,
    page,
    reset,
  ]);

  useEffect(() => {
    window.scrollTo(0, 0);
  }, [page]);

  return (
    <OffersList
      hideButtons={false}
      showMessage={showMessage}
      offersData={propertiesData}
      isLoading={isLoading}
      visibleProjects={visibleProjects}
      allProjectsLength={allProjectsLength}
    />
  );
};

export default PropertiesList;
