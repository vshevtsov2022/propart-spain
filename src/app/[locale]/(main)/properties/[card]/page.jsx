import ProjectPage from './ProjectPage';

export const metadata = {
  title: 'Project',
};

const Card = ({ params }) => {
  return <ProjectPage params={params} />;
};

export default Card;
