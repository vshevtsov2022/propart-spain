'use client';
import React, { useEffect, useState } from 'react';
import Link from 'next/link';
import { useTranslation } from 'react-i18next';
import { IoIosArrowForward } from 'react-icons/io';
import MainInfo from '../../../../../components/ProjectCard/MainInfo/MainInfo/MainInfo';
import AboutProject from '../../../../../components/ProjectCard/MainInfo/AboutProject/AboutProject';
import RelatedProjects from '../../../../../components/ProjectCard/RelatedProjects/RelatedProjects';
import LegalServices from '../../../../../components/ProjectCard/LegalServices/LegalServices';
import PropertyMap from '@/components/ProjectCard/PropertyMap/PropertyMap';
import '../../../../../styles/global.scss';
import '../../../../../components/ProjectCard/MainInfo/MainInfo/mainInfo.scss';
import OtherProjectsNew from '@/components/ProjectCard/OtherProjectsNew/OtherProjectsNew';
import { allAreasList } from '@/data/allAreasList';
import { useParams, useRouter } from 'next/navigation';
import Head from 'next/head';
import { getProjectById } from '@/api/properties';

const ProjectPage = ({ params }) => {
  const { t } = useTranslation('project-card');
  const { card } = params;
  const [project, setProject] = useState(null);
  const [error, setError] = useState(null);
  const [loading, setLoading] = useState(true);
  const [projectCardItems, setProjectCardItems] = useState([]);
  const [showAdditionalMessage, setShowAdditionalMessage] = useState(false);
  const router = useRouter();

  useEffect(() => {
    const fetchData = async () => {
      try {
        const response = await getProjectById(card);

        const convertedData = {
          ...response,
          projectId: response._id,
        };

        setProject(convertedData);
      } catch (error) {
        console.error(error);
        router.push('/error');
      } finally {
        setLoading(false);
      }
    };

    fetchData();
  }, [card]);

  useEffect(() => {
    let timer;
    if (loading) {
      timer = setTimeout(() => {
        setShowAdditionalMessage(true);
      }, 2000);
    } else {
      setShowAdditionalMessage(false);
    }
    return () => clearTimeout(timer);
  }, [loading]);

  useEffect(() => {
    const fetchData = async () => {
      try {
        const price = project?.price_from || 100000;

        const response = await fetch(`https://server.pro-part.es/project/percent?price=${price}`);
        if (!response.ok) {
          throw new Error('Failed to fetch data');
        }

        const data = await response.json();

        const convertedData = data.map(project => ({
          ...project,
          projectId: project._id,
        }));

        setProjectCardItems(convertedData);
      } catch (error) {
        console.error(error);
        setError(error);
      }
    };

    if (project && project.price_from) {
      fetchData();
    }
  }, [project, project?.price_from]);

  if (loading) {
    return (
      <div
        style={{
          display: 'flex',
          flexDirection: 'column',
          gap: '20px',
          alignItems: 'center',
          justifyContent: 'center',
          padding: '30px',
          fontSize: '30px',
        }}
      >
        <p style={{ color: 'coral' }}>Loading.... wait please</p>
        {showAdditionalMessage && (
          <p style={{ color: '#305131' }}>Do not give up...we are searching for your project</p>
        )}
      </div>
    );
  }

  if (error) return <p>Error loading project: {error.message}</p>;
  if (!project) return <p>There is no project found</p>;

  return (
    <div className="container properties-page-card-container" style={{ paddingTop: '22px' }}>
      <div className="breadcrumbs">
        <Link href="/">{t('card.link-home')}</Link>
        <IoIosArrowForward
          width="6px"
          height="10px"
          color="#676767"
          style={{ display: 'flex', alignSelf: 'center', justifyContent: 'center' }}
        />
        <Link href="/properties">{t('card.link-properties')}</Link>
        <IoIosArrowForward
          width="6px"
          height="10px"
          color="#676767"
          style={{ display: 'flex', alignSelf: 'center', justifyContent: 'center' }}
        />
        <span>{project.name}</span>
      </div>
      <MainInfo item={project} secondary={false} />
      <AboutProject item={project} />
      <PropertyMap lat={project.coordinates?.lat} lng={project.coordinates?.lng} />
      <RelatedProjects projects={project} />
      <LegalServices />
      <OtherProjectsNew
        title={t('card.other.other-projects')}
        offersData={projectCardItems}
        isLoading={loading}
      />
    </div>
  );
};

export default ProjectPage;
