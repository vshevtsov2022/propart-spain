import { MainConsulting } from '@/components/consultingPage/MainConsulting';

import { Metadata } from 'next';

export const metadata: Metadata = {
  title: 'Consulting',
};

const Consulting = () => {
  return <MainConsulting />;
};

export default Consulting;
