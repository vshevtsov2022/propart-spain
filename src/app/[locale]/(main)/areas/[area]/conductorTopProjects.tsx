'use client';
import { getTopProjects } from '@/api';
import { OffersListProjectsAreas } from '@/components/properties-page/OffersList/OffersList';
import React, { useEffect, useState } from 'react';

export const ConductorTopProjects = () => {
  const [topProjectsData, setTopProjectsData] = useState<any>([]);

  useEffect(() => {
    handleGetTopProjects();
  }, []);

  const handleGetTopProjects = async () => {
    const topProjects = await getTopProjects();
    setTopProjectsData(topProjects);
  };

  return <OffersListProjectsAreas hideButtons={true} offersData={topProjectsData} />;
};
