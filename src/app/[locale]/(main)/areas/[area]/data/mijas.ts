const rec1 = '/assets/images/area/mijas/recomend1.png';
const rec2 = '/assets/images/area/mijas/recomend2.png';
const rec3 = '/assets/images/area/mijas/recomend3.png';
const rec4 = '/assets/images/area/mijas/recomend4.png';

const beachBar2 = '/assets/images/area/mijas/beach2.png';
const beachBar4 = '/assets/images/area/mijas/beach4.png';
const beachBar7 = '/assets/images/area/mijas/beach7.png';
const beachBar10 = '/assets/images/area/mijas/beach10.png';

const miSlide1 = '/assets/images/area/mijas/slide1.png';
const miSlide2 = '/assets/images/area/mijas/slide2.png';
const miSlide3 = '/assets/images/area/mijas/slide3.png';
const miSlide4 = '/assets/images/area/mijas/slide4.png';
const miSlide5 = '/assets/images/area/mijas/slide5.png';
const miSlide6 = '/assets/images/area/mijas/slide6.png';
const miSlide7 = '/assets/images/area/mijas/slide7.png';
const miSlide8 = '/assets/images/area/mijas/slide8.png';
const miSlide9 = '/assets/images/area/mijas/slide9.png';

const MainMijas = '/assets/images/area/mijas/MainMijas.png';
const MainMijass = '/assets/images/area/mijas/MainMijass.png';

export const getMijasData = (t: (arg0: string) => any) => {
  return {
    areaName: 'mijas',
    title: t('mijas.title'),
    coords: { lat: '36.5999976', lng: '-4.6333308' },
    prices: ['€500K', '€1M', '€1.2M'],
    slideImages: [miSlide1, miSlide2, miSlide3, miSlide4, miSlide5, miSlide6, miSlide7, miSlide8, miSlide9],
    destination: {
      plane: [
        {
          time: '2,55',
          city: t('mijas.destination.plane.london'),
        },
        {
          time: '10',
          city: t('mijas.destination.plane.dubai'),
        },
        {
          time: '9',
          city: t('mijas.destination.plane.newYork'),
        },
        {
          time: '1',
          city: t('mijas.destination.plane.madrid'),
        },
      ],
      car: [
        {
          time: '0,30',
          city: t('mijas.destination.car.malaga'),
        },
        {
          time: '2,15',
          city: t('mijas.destination.car.sevilla'),
        },
      ],
    },
    geographicalInfo: [
      t('mijas.geographicalInfo.0'),
      t('mijas.geographicalInfo.1'),
      t('mijas.geographicalInfo.2'),
      t('mijas.geographicalInfo.3'),
      t('mijas.geographicalInfo.4'),
    ],
    aboutLocation: {
      gallery: [MainMijas, MainMijass],
      paragraphs: [
        t('mijas.aboutLocation.paragraphs.0'),
        t('mijas.aboutLocation.paragraphs.1'),
        t('mijas.aboutLocation.paragraphs.2'),
        t('mijas.aboutLocation.paragraphs.3'),
        t('mijas.aboutLocation.paragraphs.4'),
      ],
    },
    recommendation: [
      {
        title: t('mijas.recommendation.0'),
        subTitle: '7:00 AM - 7:30 PM',
        photo: rec1,
      },
      {
        title: t('mijas.recommendation.1'),
        subTitle: '8:00 AM - 8:00 PM',
        photo: rec2,
      },
      {
        title: t('mijas.recommendation.2'),
        subTitle: '8:00 AM - 8:30 PM',
        photo: rec3,
      },
      {
        title: t('mijas.recommendation.3'),
        subTitle: '8:00 AM - 7:00 PM',
        photo: rec4,
      },
    ],
    entertainment: [
      {
        title: t('mijas.entertainment.0.title'),
        photo: beachBar2,
        modalText: t('mijas.entertainment.0.modalText'),
        modalTitle: t('mijas.entertainment.0.modalTitle'),
      },
      {
        title: t('mijas.entertainment.1.title'),
        photo: beachBar10,
        modalText: t('mijas.entertainment.1.modalText'),
        modalTitle: t('mijas.entertainment.1.modalTitle'),
      },
      {
        title: t('mijas.entertainment.2.title'),
        photo: beachBar7,
        modalText: t('mijas.entertainment.2.modalText'),
        modalTitle: t('mijas.entertainment.2.modalTitle'),
      },
      {
        title: t('mijas.entertainment.3.title'),
        photo: beachBar4,
        modalText: t('mijas.entertainment.3.modalText'),
        modalTitle: t('mijas.entertainment.3.modalTitle'),
      },
    ],
    market: {
      economic: t('mijas.market.economic'),
      trends: t('mijas.market.trends'),
    },
  };
};
