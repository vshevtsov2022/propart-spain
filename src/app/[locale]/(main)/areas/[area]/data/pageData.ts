import { OfferCardT } from '@/components/properties-page/OfferCard/OfferCard';
import { getEsteponaData } from './estepona';
import { getMarbellaData } from './marbella';
import { getBenahavisData } from './benhavis';
import { getFuengirolaData } from './fuengirola';
import { getGoldenMileData } from './golden-mile';
import { getNewGoldenMileData } from './new-golden-mile';
import { getMijasData } from './mijas';
import { getNuevaData } from './nueva-andalucía';

export const getLocationData = (t: (arg0: string) => any) => [
  getEsteponaData(t),
  getMarbellaData(t),
  getBenahavisData(t),
  getFuengirolaData(t),
  getGoldenMileData(t),
  getNewGoldenMileData(t),
  getMijasData(t),
  getNuevaData(t),
];

export const offersData: OfferCardT[] = [
  {
    id: '1',
    beds: [3, 4],
    costPerSquareMeter: 3236,
    currency: 'eur',
    flors: 5,
    price: '725,000', // Temp, make number
    title: 'Oasis 325',
    image: '/assets/images/offers/salvia.jpg',
    location: 'San Pedro Alcantara',
  },
  {
    id: '2',
    beds: [2, 3],
    costPerSquareMeter: 2560,
    currency: 'eur',
    flors: 4,
    price: '348,000',
    title: 'Capri',
    image: '/assets/images/offers/capri.jpg',
    location: 'Estepona, Spain',
  },
  {
    id: '3',
    beds: [2, 3],
    costPerSquareMeter: 3833,
    currency: 'eur',
    flors: 4,
    price: '414,000',
    title: 'Oasis 325',
    image: '/assets/images/offers/oasis.jpg',
    location: 'Marbella, Spain',
  },
  {
    id: '4',
    beds: [4],
    costPerSquareMeter: 4233,
    currency: 'eur',
    flors: 1,
    price: '414,000',
    title: 'Marein Nat',
    image: '/assets/images/offers/mareln.jpg',
    location: 'Estepona',
  },
  {
    id: '6',
    beds: [2],
    costPerSquareMeter: 4259,
    currency: 'eur',
    flors: 1,
    price: '2,100,000',
    title: 'The Edge',
    image: '/assets/images/offers/edge.jpg',
    location: 'Marbella, Spain',
  },
];
