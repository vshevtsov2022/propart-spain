const marSlide1 = '/assets/images/area/marbella/slide1.png';
const marSlide2 = '/assets/images/area/marbella/slide2.png';
const marSlide3 = '/assets/images/area/marbella/slide3.png';
const marSlide4 = '/assets/images/area/marbella/slide4.png';
const marSlide5 = '/assets/images/area/marbella/slide5.png';
const marSlide6 = '/assets/images/area/marbella/slide6.png';
// const marSlide7 = '/assets/images/area/marbella/slide7.png';
const marSlide8 = '/assets/images/area/marbella/slide8.png';

const rec1 = '/assets/images/area/marbella/recomend1.png';
const rec2 = '/assets/images/area/marbella/recomend2.png';
const rec3 = '/assets/images/area/marbella/recomend3.png';
const rec4 = '/assets/images/area/marbella/recomend4.png';

const beachBar1 = '/assets/images/area/marbella/beach1.png';
const beachBar2 = '/assets/images/area/marbella/beach2.png';
const beachBar3 = '/assets/images/area/marbella/beach3.png';
const beachBar4 = '/assets/images/area/marbella/beach4.png';

const MainMarbellas = '/assets/images/area/marbella/MainMarbellas.png';
const MainMarbella = '/assets/images/area/marbella/MainMarbella.png';

export const getMarbellaData = (t: (arg0: string) => any) => {
  return {
    areaName: 'marbella',
    coords: { lat: '36.516666', lng: '-4.883333' },
    title: t('marbella.title'),
    prices: ['€500K', '€1M', '€1.5M', '€2M', '€4M', '€5M'],
    slideImages: [
      marSlide1,
      marSlide2,
      marSlide3,
      marSlide4,
      marSlide5,
      marSlide6,
      // marSlide7,
      marSlide8,
    ],
    destination: {
      plane: [
        {
          time: '2,55',
          city: t('marbella.destination.plane.london'),
        },
        {
          time: '10',
          city: t('marbella.destination.plane.dubai'),
        },
        {
          time: '9',
          city: t('marbella.destination.plane.newYork'),
        },
        {
          time: '1',
          city: t('marbella.destination.plane.madrid'),
        },
      ],
      car: [
        {
          time: '0,30',
          city: t('marbella.destination.car.malaga'),
        },
        {
          time: '2,15',
          city: t('marbella.destination.car.sevilla'),
        },
      ],
    },
    geographicalInfo: [
      t('marbella.geographicalInfo.0'),
      t('marbella.geographicalInfo.1'),
      t('marbella.geographicalInfo.2'),
      t('marbella.geographicalInfo.3'),
      t('marbella.geographicalInfo.4'),
    ],
    aboutLocation: {
      gallery: [MainMarbellas, MainMarbella],
      paragraphs: [
        t('marbella.aboutLocation.paragraphs.0'),
        t('marbella.aboutLocation.paragraphs.1'),
        t('marbella.aboutLocation.paragraphs.2'),
        t('marbella.aboutLocation.paragraphs.3'),
        t('marbella.aboutLocation.paragraphs.4'),
      ],
    },
    recommendation: [
      {
        title: t('marbella.recommendation.0'),
        subTitle: '7:00 AM - 7:30 PM',
        photo: rec1,
      },
      {
        title: t('marbella.recommendation.1'),
        subTitle: '8:00 AM - 8:00 PM',
        photo: rec2,
      },
      {
        title: t('marbella.recommendation.2'),
        subTitle: '8:00 AM - 8:30 PM',
        photo: rec3,
      },
      {
        title: t('marbella.recommendation.3'),
        subTitle: '8:00 AM - 7:00 PM',
        photo: rec4,
      },
    ],
    entertainment: [
      {
        title: t('marbella.entertainment.0.title'),
        photo: beachBar1,
        modalText: t('marbella.entertainment.0.modalText'),
        modalTitle: t('marbella.entertainment.0.modalTitle'),
      },
      {
        title: t('marbella.entertainment.1.title'),
        photo: beachBar2,
        modalText: t('marbella.entertainment.1.modalText'),
        modalTitle: t('marbella.entertainment.1.modalTitle'),
      },
      {
        title: t('marbella.entertainment.2.title'),
        photo: beachBar3,
        modalText: t('marbella.entertainment.2.modalText'),
        modalTitle: t('marbella.entertainment.2.modalTitle'),
      },
      {
        title: t('marbella.entertainment.3.title'),
        photo: beachBar4,
        modalText: t('marbella.entertainment.3.modalText'),
        modalTitle: t('marbella.entertainment.3.modalTitle'),
      },
    ],
    market: {
      economic: t('marbella.market.economic'),
      trends: t('marbella.market.trends'),
    },
  };
};
