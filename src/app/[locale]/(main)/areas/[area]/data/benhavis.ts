const benSlide1 = '/assets/images/area/benahavis/slide1.png';
const benSlide2 = '/assets/images/area/benahavis/slide2.png';
const benSlide3 = '/assets/images/area/benahavis/slide3.png';
const benSlide5 = '/assets/images/area/benahavis/slide5.png';
const benSlide4 = '/assets/images/area/benahavis/slide4.png';
const benSlide6 = '/assets/images/area/benahavis/slide6.jpeg';
const benSlide7 = '/assets/images/area/benahavis/slide7.png';
const benSlide8 = '/assets/images/area/benahavis/slide8.png';

const MainBenahavis = '/assets/images/area/benahavis/MainBenahavis.png';
const MainBenahaviss = '/assets/images/area/benahavis/MainBenahaviss.png';

const beachBar4 = '/assets/images/area/benahavis/beach4.png';
const beachBar5 = '/assets/images/area/benahavis/beach5.png';
const beachBar6 = '/assets/images/area/benahavis/beach6.png';
const beachBar7 = '/assets/images/area/benahavis/beach7.png';

const rec1 = '/assets/images/area/benahavis/recomend1.png';
const rec2 = '/assets/images/area/benahavis/recomend2.png';
const rec3 = '/assets/images/area/benahavis/recomend3.png';
const rec4 = '/assets/images/area/benahavis/recomend4.png';

export const getBenahavisData = (t: (arg0: string) => any) => {
  return {
    areaName: 'benahavis',
    title: t('benahavis.title'),
    coords: { lat: '36.520489', lng: '-5.045557' },
    prices: ['€500K', '€1M', '€1.7M', '€2.4M'],
    slideImages: [
      benSlide1,
      benSlide2,
      benSlide3,
      benSlide4,
      benSlide5,
      benSlide6,
      benSlide7,
      benSlide8,
    ],
    destination: {
      plane: [
        {
          time: '4,3',
          city: t('benahavis.destination.plane.london'),
        },
        {
          time: '10,30',
          city: t('benahavis.destination.plane.dubai'),
        },
        {
          time: '9,3',
          city: t('benahavis.destination.plane.newYork'),
        },
        {
          time: '2',
          city: t('benahavis.destination.plane.madrid'),
        },
      ],
      car: [
        {
          time: '1',
          city: t('benahavis.destination.car.malaga'),
        },
        {
          time: '2,40',
          city: t('benahavis.destination.car.sevilla'),
        },
      ],
    },
    geographicalInfo: [
      t('benahavis.geographicalInfo.0'),
      t('benahavis.geographicalInfo.1'),
      t('benahavis.geographicalInfo.2'),
      t('benahavis.geographicalInfo.3'),
      // t('benahavis.geographicalInfo.4'),
    ],
    aboutLocation: {
      gallery: [MainBenahavis, MainBenahaviss],
      paragraphs: [
        t('benahavis.aboutLocation.paragraphs.0'),
        t('benahavis.aboutLocation.paragraphs.1'),
        t('benahavis.aboutLocation.paragraphs.2'),
        t('benahavis.aboutLocation.paragraphs.3'),
        t('benahavis.aboutLocation.paragraphs.4'),
      ],
    },
    recommendation: [
      {
        title: t('benahavis.recommendation.0'),
        subTitle: '7:00 AM - 7:30 PM',
        photo: rec1,
      },
      {
        title: t('benahavis.recommendation.1'),
        subTitle: '8:00 AM - 8:00 PM',
        photo: rec2,
      },
      {
        title: t('benahavis.recommendation.2'),
        subTitle: '8:00 AM - 8:30 PM',
        photo: rec3,
      },
      {
        title: t('benahavis.recommendation.3'),
        subTitle: '8:00 AM - 7:00 PM',
        photo: rec4,
      },
    ],
    entertainment: [
      {
        title: t('benahavis.entertainment.0.title'),
        photo: beachBar5,
        modalText: t('benahavis.entertainment.0.modalText'),
        modalTitle: t('benahavis.entertainment.0.modalTitle'),
      },
      {
        title: t('benahavis.entertainment.1.title'),
        photo: beachBar6,
        modalText: t('benahavis.entertainment.1.modalText'),
        modalTitle: t('benahavis.entertainment.1.modalTitle'),
      },
      {
        title: t('benahavis.entertainment.2.title'),
        photo: beachBar7,
        modalText: t('benahavis.entertainment.2.modalText'),
        modalTitle: t('benahavis.entertainment.2.modalTitle'),
      },
      {
        title: t('benahavis.entertainment.3.title'),
        photo: beachBar4,
        modalText: t('benahavis.entertainment.3.modalText'),
        modalTitle: t('benahavis.entertainment.3.modalTitle'),
      },
    ],
    market: {
      economic: t('benahavis.market.economic'),
      trends: t('benahavis.market.trends'),
    },
  };
};
