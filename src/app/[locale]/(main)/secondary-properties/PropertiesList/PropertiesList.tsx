'use client';
import { useState, useEffect } from 'react';
import { getAllFilteredProjects, getSecondaryProjects } from '@/api/properties';
import useFilterStore from '@/store/filterStore';
import { SecondaryOffersList } from '@/components/properties-page/OffersList/OffersList';

const SecondaryPropertiesList = () => {
  const [propertiesData, setPropertiesData] = useState([]);
  const [isLoading, setIsLoading] = useState(true);
  const [visibleProjects, setVisibleProjects] = useState(24);
  const [allProjectsLength, setAllProjectsLength] = useState(0);
  const { searchSecondary, reset, pageSecondary, setPageSecondary } = useFilterStore();

  useEffect(() => {
    let activeRequest = true;

    const fetchData = async () => {
      try {
        setIsLoading(true);

        const response = await getSecondaryProjects(searchSecondary, pageSecondary, visibleProjects);

        if (activeRequest) {
          const convertedData = response.projects.map((project: any) => ({
            ...project,
            projectId: project._id,
          }));
          setPropertiesData(convertedData);
          setPageSecondary(response.currentPage);
          setAllProjectsLength(response.totalPages);
        }
      } catch (error) {
        console.error('Failed to fetch data', error);
      } finally {
        setIsLoading(false);
      }
    };

    fetchData();

    return () => {
      activeRequest = false;
    };
  }, [searchSecondary, pageSecondary, reset]);

  useEffect(() => {
    window.scrollTo(0, 0);
  }, [pageSecondary]);

  return (
    <SecondaryOffersList
      offersData={propertiesData}
      isLoading={isLoading}
      visibleProjects={visibleProjects}
      allProjectsLength={allProjectsLength}
    />
  );
};

export default SecondaryPropertiesList;
