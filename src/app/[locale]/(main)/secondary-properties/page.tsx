import { SmallFilterBlockProperties } from '@/components/properties-page/FilterBlockProperties/FilterBlockProperties';
import AreasList from '@/components/properties-page/AreasList';
import HelpForm from '@/components/properties-page/HelpForm';
import './Properties.scss';
import initTranslations from '@/app/i18n';
import { Metadata } from 'next';
import SecondaryPropertiesList from './PropertiesList/PropertiesList';

export interface ParamsPageProps {
  params: { locale: string };
}

export const metadata: Metadata = {
  title: 'Property',
};

const Properties: React.FC<ParamsPageProps> = async ({ params: { locale } }) => {
  const { t } = await initTranslations(locale, ['properties']);

  return (
    <>
      <SmallFilterBlockProperties />
      <div className="properties container">
        <SecondaryPropertiesList />
        <AreasList allAreas={true} title={t('areas-list.title')} />
        <HelpForm />
      </div>
    </>
  );
};

export default Properties;
