import ProjectsMapContainer from '@/components/projects-map/ProjectsMapContainer/ProjectsMapContainer';
import TopProjects from '@/components/mainPage/TopProjects/TopProjects';
import Sights from '@/components/Areas/Sights/Sights';
import initTranslations from '@/app/i18n';
import './projects-map.scss';
import { getTopProjects } from '@/api';
import { Metadata } from 'next';

export const metadata: Metadata = {
  title: 'Map',
};

const ProjectsMap = async ({ params: { locale } }: any) => {
  const { t } = await initTranslations(locale, ['projects-map']);
  return (
    <>
      <ProjectsMapContainer />
      <div className="projects-map__top-projects">
        <TopProjects />
      </div>
      <div className="projects-map__areas container">
        <h2 className="map-areas-title">{t('map-areas-title')}</h2>
        <Sights locale={locale} />
      </div>
    </>
  );
};

export default ProjectsMap;
