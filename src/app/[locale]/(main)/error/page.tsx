import initTranslations from '@/app/i18n';
import Link from 'next/link';
import './ErrorPage.scss';

const Error = async ({ params: { locale } }: any) => {
  const { t } = await initTranslations(locale, ['error-page']);

  return (
    <div className="errorPage container">
      <span className="errorPage__ops">{t('ops')}</span>
      <h1 className="errorPage__title">{t('title')}</h1>
      <p className="errorPage__descr">{t('descr2')}</p>
      <Link href="/">
        <button className="errorPage__btn">{t('btn')}</button>
      </Link>
    </div>
  );
};
export default Error;
