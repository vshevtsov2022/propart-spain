import type { Metadata } from 'next';
import { ReactNode } from 'react';

import { Footer } from '@/layout/Footer';
import { Header } from '@/layout/Header';

export const metadata: Metadata = {
  title: 'Property in Costa Del Sol, Spain | ProPart Spain',
  description: 'Property in Costa Del Sol, Spain |Real estate company Spain | ProPart Spain',
};
interface LayoutProps {
  children: ReactNode;
  params: { locale: string };
}

export default async function RootLayout({ children, params: { locale } }: LayoutProps) {
  return (
    <>
      <Header />
      <main>{children}</main>
      <Footer />
    </>
  );
}
