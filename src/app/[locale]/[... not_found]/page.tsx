import { redirect } from 'next/navigation';
const NotFoundPage = () => {
  redirect('/error');
};
export default NotFoundPage;
//  return(<div>not found</div>)

// 'use client';
// import { redirect, usePathname } from 'next/navigation';

// const NotFoundPage = () => {
//   const pathname = usePathname();

//   if (pathname !== '/wp-admin') {
//     redirect('/');
//   }

//   return null;
// };

// export default NotFoundPage;
