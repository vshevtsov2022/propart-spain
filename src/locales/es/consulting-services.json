{
  "pageDescription": "Este contenido contiene información en tiempo real sobre servicios legales en España",
  "sections": {
    "realEstate": {
      "title": "Somos expertos en todas las áreas relacionadas con la adquisición de varios tipos de bienes raíces",
      "description": "Tenemos conocimientos y experiencia en los aspectos legales y fiscales relacionados tanto con las ventas y adquisiciones, como con la financiación y el establecimiento de garantías.",
      "competenciesTitle": "Nuestras competencias incluyen:",
      "competencies": [
        {
          "title": "Asesoramiento legal en transacciones inmobiliarias",
          "description": "Ofrecemos asesoramiento legal experto en transacciones inmobiliarias, incluyendo análisis legal y estructuración de transacciones."
        },
        {
          "title": "Asesoramiento fiscal",
          "description": "Proporcionamos asesoramiento fiscal en el contexto de la adquisición y posesión de bienes inmuebles, ayudando a optimizar la carga tributaria de los clientes."
        },
        {
          "title": "Financiación y garantías",
          "description": "Brindamos apoyo en la financiación de adquisiciones inmobiliarias y el establecimiento de garantías adecuadas."
        },
        {
          "title": "Evaluación de riesgos",
          "description": "Analizamos los riesgos de las transacciones inmobiliarias y ofrecemos recomendaciones para mitigarlos."
        },
        {
          "title": "Negociaciones comerciales",
          "description": "Participamos en negociaciones comerciales con contrapartes y otras partes involucradas en las transacciones."
        },
        {
          "title": "Soporte en transacciones",
          "description": "Proporcionamos apoyo legal completo durante el proceso de transacción, incluyendo la preparación de documentos, la celebración de contratos y la inscripción de transacciones."
        },
        {
          "title": "Organización de financiamiento",
          "description": "Asistimos en la organización de financiamiento para transacciones inmobiliarias, incluyendo la negociación con instituciones financieras y la legalización de transacciones."
        }
      ],
      "commitment": "Nos comprometemos a garantizar que nuestros clientes comprendan plenamente los aspectos legales y fiscales involucrados en las adquisiciones inmobiliarias y ofrecemos una solución integral para transacciones exitosas."
    },
    "taxAdvice": {
      "title": "Asesoramiento fiscal, financiero y contable",
      "description": "Trabajamos con particulares y empresas, residentes y no residentes, ayudándoles en sus operaciones diarias, proporcionando presentación de impuestos, asesoramiento fiscal y financiero, así como contabilidad y administración.",
      "optimization": "Con el fin de optimizar la situación fiscal de nuestros clientes y ser conscientes de la necesidad de estar al día con la legislación fiscal y contable actual en el ámbito personal y empresarial, contamos con asesores fiscales que exploran nuevas oportunidades de optimización fiscal en diversas situaciones.",
      "servicesTitle": "Nuestros servicios incluyen:",
      "services": [
        "Consultoría fiscal, financiera y contable para personas físicas y jurídicas.",
        "Representación de empresas extranjeras en España.",
        "Declaraciones de obligaciones ante las autoridades fiscales para personas físicas y jurídicas.",
        "Impuesto sobre sociedades e impuesto sobre el valor añadido.",
        "Cálculo de retenciones.",
        "Declaraciones de renta y patrimonio para personas no residentes.",
        "Planificación fiscal para empresas y particulares.",
        "Asesoramiento en procesos de reestructuración corporativa.",
        "Fiscalidad inmobiliaria.",
        "Asesoramiento en la planificación del impuesto sobre sucesiones.",
        "Representación y tramitación de casos ante la Agencia Tributaria.",
        "Preparación y legalización de libros contables e informes anuales.",
        "Interacción con bancos, notarios, el Registro de la Propiedad y Comercio, y otros organismos gubernamentales."
      ]
    },
    "companyAdministration": {
      "title": "Administración de empresas",
      "description": "Nuestro enfoque está dirigido a proporcionar a nuestros clientes una solución integral para la gestión efectiva de empresas.",
      "administrativeServices": "Ofrecemos una variedad de servicios que incluyen realizar pagos, mantener arrendamientos, pagar impuestos locales y estatales, tarifas, seguros y otras facturas como tarifas de construcción, con pleno apoyo legal, fiscal y contable. Además de estos servicios administrativos, brindamos apoyo legal, fiscal y contable para garantizar el cumplimiento de todas las regulaciones y normas aplicables.",
      "servicesTitle": "Entre los servicios proporcionados se encuentran:",
      "services": [
        {
          "title": "Realización de Pagos",
          "description": "Encargarnos de realizar pagos en nombre de las empresas administradas. Esto puede incluir pagos a proveedores, servicios públicos, salarios y otros gastos."
        },
        {
          "title": "Mantenimiento de Contratos de Arrendamiento",
          "description": "Gestionamos y mantenemos los contratos de arrendamiento en nombre de la empresa, asegurando que se cumplan los términos y acuerdos."
        },
        {
          "title": "Pago de Impuestos y Tasas",
          "description": "Asumimos la obligación de pagar impuestos locales y estatales y otras tarifas aplicables, garantizando el cumplimiento de las obligaciones fiscales de la comunidad."
        },
        {
          "title": "Gestión de Seguros",
          "description": "Nos encargamos de los pagos de primas y asuntos relacionados con las pólizas, asegurando una protección adecuada y pública."
        },
        {
          "title": "Otras Cuentas",
          "description": "Gestionar y realizar pagos relacionados con otras cuentas, como trabajos de construcción u otros gastos específicos de la sociedad."
        }
      ]
    },
    "laborLaw": {
      "title": "Derecho laboral",
      "description": "Debido a la complejidad del derecho laboral y de la seguridad social, el Asesoramiento Legal en cuestiones laborales, ya sea al inicio, durante el transcurso de la relación laboral o en su terminación, se ha vuelto cada vez más importante desde la perspectiva tanto del empleador como del empleado. Nos comprometemos a proporcionar una amplia gama de servicios de derecho laboral y de seguridad social para garantizar relaciones laborales efectivas y conformes a la ley dentro de una organización.",
      "servicesTitle": "Entre los servicios proporcionados se encuentran:",
      "services": [
        {
          "title": "Asesoramiento legal en derecho laboral y Gestión de relaciones laborales en empresas",
          "description": "Proporcionar consultoría experta y gestión de relaciones laborales en empresas, y regular cuestiones de gestión de recursos humanos."
        },
        {
          "title": "Audiencia social y laboral",
          "description": "Realizamos auditorías sociales laborales, proporcionando una evaluación integral de la situación social y laboral en la organización."
        },
        {
          "title": "Aplazamientos",
          "description": "Nuestro equipo brinda apoyo para los aplazamientos fiscales y de cotizaciones."
        },
        {
          "title": "Representación en tribunales laborales",
          "description": "Proporcionamos representación en tribunales laborales, protegiendo los intereses de los clientes en disputas legales."
        },
        {
          "title": "Consolidaciones",
          "description": "Ayudamos con los procedimientos de conciliación, buscando resolver disputas laborales de manera amistosa."
        },
        {
          "title": "Conflictos laborales colectivos",
          "description": "Nuestro equipo se dedica a resolver conflictos laborales colectivos."
        },
        {
          "title": "Regulación de procedimientos de resolución de empleo (ERES y ERTES)",
          "description": "Proporcionamos servicios para los procedimientos de resolución de empleo, como despidos colectivos y reducciones temporales de jornada."
        },
        {
          "title": "Representación ante sindicatos / Acuerdos colectivos",
          "description": "Representamos a los clientes ante los sindicatos laborales y participamos en negociaciones de convenios colectivos."
        },
        {
          "title": "Gestión de nóminas, contratos y más",
          "description": "Brindamos gestión de nóminas, redacción de contratos y otros servicios de relaciones laborales."
        },
        {
          "title": "Proceso y redacción de contratos laborales y renovaciones",
          "description": "Manejamos el procesamiento y la redacción de contratos laborales y sus renovaciones."
        },
        {
          "title": "Procesamiento de procedimientos de jubilación, prestaciones por discapacidad, etc.",
          "description": "Brindamos servicios para procesar la jubilación, prestaciones por discapacidad y otros aspectos de la Seguridad Social."
        },
        {
          "title": "Asuntos de migración de extranjeros",
          "description": "También brindamos asesoramiento y servicios sobre aspectos migratorios para trabajadores extranjeros."
        }
      ]
    },
    "bankAccount": {
      "title": "Apertura de una cuenta bancaria",
      "description": "Apertura de una cuenta bancaria en España para un no residente con ProPart:",
      "content": "Si está buscando asesoramiento profesional y asistencia para abrir una cuenta bancaria en España como no residente, nuestros expertos no solo le proporcionarán la información que necesita, sino que también le brindarán apoyo completo en el proceso de apertura de la cuenta, incluida la recopilación de todos los documentos necesarios, incluidas las traducciones juradas, y lo acompañarán al banco.",
      "difficulty": "Debido al endurecimiento de las políticas contra el lavado de dinero y los cambios en la legislación bancaria, abrir una cuenta bancaria en España para no residentes se está volviendo cada vez más difícil. La apertura exitosa de la cuenta depende de la lealtad de un banco en particular y ofrecemos nuestra experiencia para facilitar este proceso.",
      "support": "Cooperamos con bancos confiables y brindamos a nuestros clientes apoyo en todas las etapas de la apertura de una cuenta en España, que incluyen:",
      "steps": [
        "Consulta sobre todos los aspectos del procedimiento de apertura de la cuenta.",
        "Asistencia en la recopilación de los documentos necesarios.",
        "Organización de traducciones juradas de documentos.",
        "Acompañamiento al banco para firmar el contrato de cuenta bancaria."
      ],
      "note": "Nota: El costo de las traducciones juradas se cobra por separado.",
      "additionalQuestions": "Si tiene preguntas adicionales o necesita más información, deje una solicitud en línea y nos pondremos en contacto con usted pronto para una consulta."
    }
  }
}
