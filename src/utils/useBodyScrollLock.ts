import { useEffect } from 'react';

const useBodyScrollLock = (isLocked: boolean) => {
  useEffect(() => {
    const htmlElement = document.documentElement;
    if (isLocked) {
      htmlElement.style.overflow = 'hidden';
    } else {
      htmlElement.style.overflow = 'auto';
    }
    return () => {
      htmlElement.style.overflow = 'auto';
    };
  }, [isLocked]);
};

export default useBodyScrollLock;
