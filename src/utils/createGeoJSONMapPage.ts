import { FeatureCollection, GeoJsonProperties, Point } from 'geojson';

export const createGeoJSONMapPage = (
  dataMarker: any
): FeatureCollection<Point, GeoJsonProperties> => {
  return {
    type: 'FeatureCollection',
    features: dataMarker.map((item: any) => ({
      type: 'Feature',
      properties: {
        id: item.projectId,
        photo: item.picture,
        name: item.name,
        price: item.price_from,
        nameId: item.nameId,
      },
      geometry: {
        type: 'Point',
        coordinates: [item.coordinates?.[0], item.coordinates?.[1]],
      },
    })),
  };
};
