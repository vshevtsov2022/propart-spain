import axios from 'axios';
const baseUrl = process.env.NEXT_PUBLIC_BASE_URL_API;

export interface PostFormData {
  name: string;
  phone: string;
}
export interface PostFormDataSavedProjects {
  name: string;
  phone: string;
  selectedOption?: string;
  savedProjects?: number[];
}

//відправка форми
export const postFormData = async (formData: PostFormData): Promise<void> => {
  try {
    const response = await axios.post(`${baseUrl}support`, formData, {
      headers: {
        'Content-Type': 'multipart/form-data',
      },
    });

    if (response.status === 200) {
      console.log('Success:', response.data);
    } else {
      console.log('failed:', response.data);
    }
  } catch (error) {
    console.error('failed:', error);
  }
};

export const postFormDataSavedProjects = async (formData: PostFormDataSavedProjects): Promise<void> => {
  try {
    console.log(formData);
    const response = await axios.post(`${baseUrl}support/projects`, formData, {
      headers: {
        'Content-Type': 'multipart/form-data',
      },
    });

    if (response.status === 200) {
      console.log('Success:', response.data);
    } else {
      console.log('failed:', response.data);
    }
  } catch (error) {
    console.error('failed:', error);
  }
};