export const getTopProjects = async () => {
  try {
    const response = await fetch('https://server.pro-part.es/project/top-projects');
    if (!response.ok) {
      console.error('Failed to fetch data');
    }
    return response.json();
  } catch (error) {
    console.error(error);
  }
};
