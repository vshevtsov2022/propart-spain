import axios from 'axios';
const baseUrl = process.env.NEXT_PUBLIC_BASE_URL_API;

export interface SearchParamsType {
  sortField?: string;
  sortOrder?: string;
  searchParams?: string;
  selectedRadio?: string;
  selectedType?: string[];
  selectedBeds?: string[];
  selectedPrice?: { min: number | null; max: number | null };
  selectedSize?: { min: number | null; max: number | null };
  selectedArea?: string[];
  page?: number;
  size?: number;
  firstRequest?: string;
}

export interface SearchData {
  bedrooms: string[];
  completion: string[];
  location: string;
  type: string[];
  priceFrom: number;
  priceTo: number;
  areas: string[];
  sizeFrom: number;
  sizeTo: number;
  namePrefix: string;
}

export const getAllFilteredProjects = async (
  data: SearchData,
  page: number,
  size: number
): Promise<any> => {
  try {
    const url = `https://server.pro-part.es/project?page=${page}&size=${size}`;
    const response = await axios.post(url, data, {
      headers: {
        'Content-Type': 'application/json',
      },
    });
    return response.data;
  } catch (error) {
    console.error('Failed to fetch data from server', error);
    return null;
  }
};

export const getProjectById = async (id: string) => {
  try {
    const response = await fetch(`https://server.pro-part.es/project/project/${id}`);
    if (!response.ok) {
      console.error('Failed to fetch data');
    }
    return response.json();
  } catch (error) {
    console.error(error);
  }
};

export const getProjectByMap = async (data: SearchData): Promise<any> => {
  try {
    const url = `https://server.pro-part.es/project/map`;
    const response = await axios.post(url, data, {
      headers: {
        'Content-Type': 'application/json',
      },
    });
    return response.data;
  } catch (error) {
    console.error('Failed to fetch data from server', error);
    return null;
  }
};

export const getProjectNameById = async (id: string) => {
  try {
    const response = await fetch(`https://server.pro-part.es/project/project-name/${id}`);
    if (!response.ok) {
      console.error('Failed to fetch data');
    }
    return response.json();
  } catch (error) {
    console.error(error);
  }
};

export const getSecondaryProjects = async (name: string, page: number, size: number) => {
  try {
    const response = await fetch(
      `https://server.pro-part.es/project/hidden?name=${name}&page=${page}&size=${size}`
    );
    if (!response.ok) {
      console.error('Failed to fetch data');
    }
    return response.json();
  } catch (error) {
    console.error(error);
  }
};
