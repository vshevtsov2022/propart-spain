//Map------------------------
import { Marker } from 'mapbox-gl';
export interface UserDataMarker {
  content: string;
  lng: number;
  lat: number;
}

export interface MarkerElement {
  id: number;
  element: Marker;
}

//-------------------
//data
export interface ProjectDataType {
  _id: { $oid: string };
  name: string;
  completion: string;
  size: number;
  hidden: boolean;
  price_from: number;
  size_m2: number;
  location: string;
  short_description: {
    total_floors: number;
    type: string;
    rooms: number;
  };
  pictures: string[];
  coordinates: {
    lng: string;
    lat: string;
  };
  area: string;
  about: Record<string, { title: string; description: string }>;
  floorPlans: Record<string, any>;
  _class: string;
}

//-------------------

export interface SelectedSwiperCardTypes {
  projectId: number | string;
  name: string;
  area: string;
  price_from: string;
  floorPlanNames: string[];
  value: string | number;
  total_floors: number;
  pictures: string[];
  size_m2: number;
  location: string;
}
/*
export interface SelectedSwiperCardTypes {
  id: number | string;
  name: string;
  area: string;
  price: string | number;
  bed: string[] | number[];
  value: string | number;
  floors: string | number;
  src: string;
}
*/
export interface FilterProjectNameType {
  id: string;
  name: string;
  coordinates: [number, number];
}
