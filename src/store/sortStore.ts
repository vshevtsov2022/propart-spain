import { create } from 'zustand';
import { PersistOptions, createJSONStorage, persist } from 'zustand/middleware';

export interface SortState {
  sortField: string;
  sortOrder: string;
  reset: boolean;
  setSort: (field: keyof SortState, value: any) => void;
  toggleReset: () => void;
  clearSort: () => void;
}

type SortStateCreator = (
  set: (
    partial: Partial<SortState> | ((state: SortState) => Partial<SortState>),
    replace?: boolean
  ) => void,
  get: () => SortState,
  api: any
) => SortState;

const createSortState: SortStateCreator = set => ({
  sortField: '',
  sortOrder: '',
  reset: false,
  setSort: (field, value) => set(state => ({ ...state, [field]: value })),
  toggleReset: () => set(state => ({ reset: !state.reset })),
  clearSort: () =>
    set({
      sortField: '',
      sortOrder: '',
    }),
});

const useSortStore = create(
  persist<SortState>(createSortState, {
    name: 'sort-storage',
    storage: createJSONStorage(() => sessionStorage),
  } as PersistOptions<SortState>)
);

export default useSortStore;

/*
import { create } from 'zustand';
import { PersistOptions, createJSONStorage, persist } from 'zustand/middleware';
export interface SortState {
  selectedSort: string;
  reset: boolean;
  setSort: (field: keyof SortState, value: any) => void;
  toggleReset: () => void;
  clearSort: () => void;
}

type SortStateCreator = (
  set: (
    partial: Partial<SortState> | ((state: SortState) => Partial<SortState>),
    replace?: boolean
  ) => void,
  get: () => SortState,
  api: any
) => SortState;

const createSortState: SortStateCreator = set => ({
  selectedSort: '',
  reset: false,
  setSort: (field, value) => set(state => ({ ...state, [field]: value })),
  toggleReset: () => set(state => ({ reset: !state.reset })),
  clearSort: () =>
    set({
      selectedSort: '',
    }),
});

const useSortStore = create(
  persist<SortState>(createSortState, {
    name: 'sort-storage',
    storage: createJSONStorage(() => sessionStorage),
  } as PersistOptions<SortState>)
);

export default useSortStore;
*/