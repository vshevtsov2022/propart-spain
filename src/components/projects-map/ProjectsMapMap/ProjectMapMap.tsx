'use client';
import React, { useEffect, useRef, useState } from 'react';
import Mapboxgl from 'mapbox-gl';
import MapboxDraw from '@mapbox/mapbox-gl-draw';
import 'mapbox-gl/dist/mapbox-gl.css';
import '@mapbox/mapbox-gl-draw/dist/mapbox-gl-draw.css';
import { createGeoJSONMapPage } from '@/utils/createGeoJSONMapPage';
import { clastersLayer, clastersCountLayer, unclastersLayer } from '@/constants/map';
import './ProjectMapMap.scss';
import useFilterStore from '@/store/filterStore';
import { useMediaQuery } from 'react-responsive';
import { priceFormatter } from '@/utils/formatter';
import * as turf from '@turf/turf';

if (process.env.NEXT_PUBLIC_MAPBOX_ACCESS_TOKEN) {
  Mapboxgl.accessToken = process.env.NEXT_PUBLIC_MAPBOX_ACCESS_TOKEN;
} else {
  console.error('Mapbox access token is not provided or invalid.');
}

interface ProjectMapMapProps {
  mapData: any;
  isFullScreenMap: boolean;
}

const ProjectMapMap = ({ mapData, isFullScreenMap }: ProjectMapMapProps) => {
  const currentPopup = useRef<Mapboxgl.Popup | null>(null);
  const isTablet = useMediaQuery({ maxWidth: 1023 });
  const mapContainer = useRef<HTMLDivElement | null>(null);
  const map = useRef<Mapboxgl.Map | null>(null);
  const draw = useRef<MapboxDraw | null>(null);
  const { mapCoordinates, mapZoom, reset, polygonData, setPolygonData, setPolygonsProjects } =
    useFilterStore();
  const [initialDataLoaded, setInitialDataLoaded] = useState(false);

  const handleClusterClick = (event: any) => {
    const features = map.current?.queryRenderedFeatures(event.point, {
      layers: ['clusters'],
    });
    if (!features || !features.length) {
      return;
    }
    const feature = features[0];
    const clusterId = feature.properties?.cluster_id;
    if (!clusterId) {
      return;
    }
    const source = map.current?.getSource('projects') as Mapboxgl.GeoJSONSource;
    source.getClusterExpansionZoom(clusterId, (err, zoom) => {
      if (err) return;
      const coordinates = (feature.geometry as GeoJSON.Point).coordinates;
      map.current?.easeTo({
        center: coordinates as [number, number],
        zoom: zoom + 1,
      });
    });
  };

  const handleMarkerClick = (event: any) => {
    const features = map.current?.queryRenderedFeatures(event.point, {
      layers: ['unclustered-point'],
    });
    if (!features || !features.length) {
      return;
    }
    const feature = features[0];
    if (feature.properties && feature.geometry.type === 'Point') {
      if (currentPopup.current) {
        currentPopup.current.remove();
        currentPopup.current = null;
      }
      const popup = new Mapboxgl.Popup({ offset: [0, -15] })
        .setLngLat(feature.geometry.coordinates as [number, number])
        .setHTML(
          `<a class='link' href="/properties/${feature.properties.nameId}">
            <div class='image'>
              <img src=${
                feature.properties.photo ? feature.properties.photo : '/icons/icons-villa.png'
              } alt="React Image" />
            </div>
            <div class='info'>
              <span class='title'>${feature.properties.name}</span>
              <p>${priceFormatter(feature.properties.price, 'EUR')} €</p>
            </div>
          </a>`
        )
        .addTo(map.current!);
      currentPopup.current = popup;
    }
  };

  const filterProjectsByPolygon = (projects: any[], polygon: any) => {
    if (!polygon || !polygon.coordinates) {
      setPolygonsProjects([]);
      return projects;
    }

    const polygonFeature = turf.polygon(polygon.coordinates);
    const filteredProjects = projects.filter(project => {
      if (Array.isArray(project.coordinates)) {
        const [lng, lat] = project.coordinates;
        if (isNaN(lng) || isNaN(lat)) {
          return false;
        }

        const projectPoint = turf.point([lng, lat]);
        const isInPolygon = turf.booleanPointInPolygon(projectPoint, polygonFeature);
        return isInPolygon;
      } else {
        return false;
      }
    });

    const transformedProjects = filteredProjects.map(project => ({
      ...project,
      coordinates: {
        lng: project.coordinates[0].toString(),
        lat: project.coordinates[1].toString(),
      },
    }));

    setPolygonsProjects(transformedProjects);
    return filteredProjects;
  };

  useEffect(() => {
    if (!map.current) {
      map.current = new Mapboxgl.Map({
        container: mapContainer.current!,
        style: 'mapbox://styles/abiespana/cltantp6a00ut01pj4v9h0srk',
        center: mapCoordinates,
        zoom: mapZoom,
        minZoom: 5,
        maxZoom: 18,
      });

      draw.current = new MapboxDraw({
        displayControlsDefault: false,
        controls: {
          polygon: true,
          trash: true,
        },
      });

      map.current.addControl(draw.current, 'top-left');

      map.current.on('load', () => {
        setInitialDataLoaded(true);
      });

      map.current.on('click', 'clusters', (event: any) => {
        handleClusterClick(event);
      });

      map.current.on('click', 'unclustered-point', (event: any) => {
        handleMarkerClick(event);
      });

      map.current.on('touchend', 'clusters', (event: any) => {
        handleClusterClick(event);
      });

      map.current.on('touchend', 'unclustered-point', (event: any) => {
        handleMarkerClick(event);
      });

      map.current.on('mouseenter', ['clusters', 'unclustered-point'], () => {
        map.current!.getCanvas().style.cursor = 'pointer';
      });

      map.current.on('mouseleave', ['clusters', 'unclustered-point'], () => {
        map.current!.getCanvas().style.cursor = '';
      });

      map.current.addControl(new Mapboxgl.NavigationControl(), 'top-right');

      map.current.on('draw.create', updateArea);
      map.current.on('draw.update', updateArea);
      map.current.on('draw.delete', () => {
        setPolygonData(null);
        setPolygonsProjects([]);
      });
    }
  }, []);

  const redrawPolygons = () => {
    if (polygonData && draw.current) {
      draw.current.deleteAll();
      draw.current.add({
        type: 'Feature',
        properties: {},
        geometry: polygonData,
      });
    }
  };

  useEffect(() => {
    if (map.current && initialDataLoaded) {
      redrawPolygons();
    }
  }, [polygonData, initialDataLoaded]);

  useEffect(() => {
    if (initialDataLoaded && map.current && mapData) {
      const filteredData = filterProjectsByPolygon(mapData, polygonData);
      const source = map.current.getSource('projects') as Mapboxgl.GeoJSONSource;
      if (source) {
        source.setData(createGeoJSONMapPage(filteredData));
      } else {
        map.current.addSource('projects', {
          type: 'geojson',
          data: createGeoJSONMapPage(filteredData),
          cluster: true,
          clusterRadius: 80,
        });
        map.current.addLayer(clastersLayer);
        map.current.addLayer(clastersCountLayer);
        map.current.addLayer(unclastersLayer);
      }
    }
  }, [initialDataLoaded, mapData, polygonData]);

  useEffect(() => {
    if (polygonData && draw.current) {
      draw.current.deleteAll();
      draw.current.add({
        type: 'Feature',
        properties: {},
        geometry: polygonData,
      });
    }
  }, [polygonData]);

  useEffect(() => {
    if (map.current) {
      map.current.resize();
    }
  }, [isFullScreenMap]);

  useEffect(() => {
    if (map.current) {
      map.current.resize();
    }
  }, []);

  useEffect(() => {
    if (map.current) {
      map.current.flyTo({
        center: mapCoordinates,
        zoom: mapZoom,
      });
    }
  }, [mapCoordinates, mapZoom]);

  useEffect(() => {
    if (map.current && draw.current) {
      draw.current.deleteAll();
    }
  }, [reset]);

  const updateArea = (e: any) => {
    const data = draw.current?.getAll();
    const features = data?.features;

    if (features && features.length > 0) {
      const geometry = features[0].geometry;

      if (geometry.type === 'Polygon') {
        const coordinates = geometry.coordinates;
        if (coordinates) {
          setPolygonData(geometry);
        }
      }
    }
  };

  const handleMapClick = (event: any) => {
    // Проверяем, есть ли объекты под местом клика
    const features = map.current?.queryRenderedFeatures(event.point);

    // Если объекты есть, значит клик был на интерактивном элементе, игнорируем его для закрытия попапа
    if (features && features.length) {
      return;
    }

    // Если текущий попап открыт, закрываем его
    if (currentPopup.current) {
      currentPopup.current.remove();
      currentPopup.current = null;
    }
  };

  useEffect(() => {
    if (map.current) {
      // Добавляем обработчик кликов по карте
      map.current.on('click', handleMapClick);

      // Добавляем обработчик кликов по интерактивным слоям для открытия карточек
      map.current.on('click', 'clusters', handleClusterClick);
      map.current.on('click', 'unclustered-point', handleMarkerClick);

      // Добавляем аналогичные обработчики для touch событий
      map.current.on('touchend', handleMapClick);
      map.current.on('touchend', 'clusters', handleClusterClick);
      map.current.on('touchend', 'unclustered-point', handleMarkerClick);

      return () => {
        // Очищаем обработчики при размонтировании компонента
        if (map.current) {
          map.current.off('click', handleMapClick);
          map.current.off('click', 'clusters', handleClusterClick);
          map.current.off('click', 'unclustered-point', handleMarkerClick);
          map.current.off('touchend', handleMapClick);
          map.current.off('touchend', 'clusters', handleClusterClick);
          map.current.off('touchend', 'unclustered-point', handleMarkerClick);
        }
      };
    }
  }, [map.current]);

  return (
    <div
      ref={mapContainer}
      className={`main-map-container ${isFullScreenMap ? 'full-screen' : ''}`}
    ></div>
  );
};

export default ProjectMapMap;
