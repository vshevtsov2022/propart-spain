'use client';
import { useState, useEffect } from 'react';
import Image from 'next/image';
import MapSearch from '@/components/ui/MapSearch/MapSearch';
import DropdownChecker from '@/components/ui/DropdownChecker/DropdownChecker';
import PriceChecker from '@/components/ui/PriceChecker/PriceChecker';
import useFilterStore, { PriceType, FilterState } from '@/store/filterStore';
import useSortStore from '@/store/sortStore';
import { useTranslation } from 'react-i18next';
import './FilterBlockMap.scss';
//для запиту отримання списку проектів
import {
  getTypeOptions,
  getBedroomOptions,
  priceOptions,
  sizeOptions,
  areasOptions,
} from '@/constants/filters';

interface FilterBlockMapProps {
  filteredProperties: any;
}

const FilterBlockMap = ({ filteredProperties }: FilterBlockMapProps) => {
  const [isClient, setIsClient] = useState<boolean>(false);
  useEffect(() => {
    setIsClient(true);
  }, []);
  const { t } = useTranslation('map-filters');
  const typeOptions = getTypeOptions(t);
  const bedroomOptions = getBedroomOptions(t);

  const {
    projects, //список проектів
    polygonData,
    selectedType,
    selectedBeds,
    selectedPrice,
    selectedSize,
    selectedArea,
    reset,
    setMapObject,
    setFilter,
    toggleReset,
    clearFilters,
    setProjects,
    setTempArea,
  } = useFilterStore();
  const { clearSort } = useSortStore();
  const handleFilterChange = (field: keyof FilterState, value: any) => {
    setFilter(field, value);
  };
  const handleClearFilter = () => {
    clearFilters();
    toggleReset();
    clearSort();
  };

  return (
    isClient && (
      <div className="filter-block-map container">
        <div className="search">
          <MapSearch
            handleSearch={value => handleFilterChange('searchParams', value)}
            handleNameChange={setMapObject}
            projectsNameData={filteredProperties}
            reset={reset}
          />
        </div>
        <div className="dropdown">
          <DropdownChecker
            data={typeOptions}
            title={t('type_title')}
            selectValue={selectedType}
            action={(values: string[]) => handleFilterChange('selectedType', values)}
            isVisibleText={false}
          />
        </div>

        <div className="dropdown">
          <DropdownChecker
            data={bedroomOptions}
            title={t('beds_title')}
            selectValue={selectedBeds}
            action={(values: string[]) => handleFilterChange('selectedBeds', values)}
            isVisibleText={false}
          />
        </div>

        <div className="dropdown">
          <PriceChecker
            data={priceOptions}
            title={t('price_title')}
            type="Price"
            selectValue={selectedPrice}
            action={(values: PriceType) => handleFilterChange('selectedPrice', values)}
          />
        </div>

        <div className="dropdown">
          <PriceChecker
            data={sizeOptions}
            title={t('size_title')}
            type="Size"
            selectValue={selectedSize}
            action={(values: PriceType) => handleFilterChange('selectedSize', values)}
            isSize={true}
          />
        </div>

        <div className="dropdown">
          <DropdownChecker
            data={areasOptions}
            title={t('area_title')}
            selectValue={selectedArea}
            action={(values: string[]) => {
              handleFilterChange('selectedArea', values);
              setTempArea(values);
            }}
            isVisibleText={false}
          />
        </div>

        <button onClick={handleClearFilter} className="clear">
          <Image src="/assets/icons/clear.svg" alt="Clear Icon" width={20} height={20} />
          {t('clean')}
        </button>
      </div>
    )
  );
};

export default FilterBlockMap;
