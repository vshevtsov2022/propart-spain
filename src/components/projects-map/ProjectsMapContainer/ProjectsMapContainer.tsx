// 'use client';
// import { useState, useEffect } from 'react';
// import Image from 'next/image';
// import { useMediaQuery } from 'react-responsive';
// import FilterBlockMap from '@/components/projects-map/FilterBlockMap/FilterBlockMap';
// import ProjectMapMap from '../ProjectsMapMap/ProjectMapMap';
// import MapButtons from '../MapButtons/MapButtons';
// import useBodyScrollLock from '@/utils/useBodyScrollLock';
// import useFilterStore from '@/store/filterStore';
// import { useTranslation } from 'react-i18next';
// import '../../../app/[locale]/(main)/projects-map/projects-map.scss';
// import { getProjectByMap } from '@/api/properties';
// import { allAreasList } from '@/data/allAreasList';
// const ProjectsMapContainer = () => {
//   const { t } = useTranslation('projects-map');
//   const [isClient, setIsClient] = useState<boolean>(false);
//   const {
//     searchParams,
//     selectedType,
//     selectedBeds,
//     selectedPrice,
//     selectedSize,
//     selectedArea,
//     polygonData,
//     toggleReset,
//     clearFilters,
//     tempArea,
//   } = useFilterStore();

//   useEffect(() => {
//     setIsClient(true);
//   }, []);

//   const isMobile = useMediaQuery({ maxWidth: 767 });
//   const [isFullScreenMap, setIsFullScreenMap] = useState(false);
//   useBodyScrollLock(isFullScreenMap);
//   useBodyScrollLock(isMobile);

//   const handleFoolScreenMap = () => {
//     setIsFullScreenMap(!isFullScreenMap);
//   };

//   useEffect(() => {
//     if (isMobile) {
//       setIsFullScreenMap(true);
//     }
//   }, []);

//   const [filteredProperties, setFilteredProperties] = useState<any>([]);

//   useEffect(() => {
//     const fetchData = async () => {
//       try {
//         const data = {
//           bedrooms:
//             selectedBeds.length > 0 ? selectedBeds : ['Studio', '1', '2', '3', '4', '5', '6', '7'],
//           completion: ['Ready', 'Off plan', 'Special Offer'],
//           location: '',
//           type: selectedType.length
//             ? selectedType
//             : ['Apartment', 'Townhouse', 'Villa', 'Duplex', 'Flat', 'Penthouse', 'Ground floor'],
//           priceFrom: selectedPrice.min ? selectedPrice.min : 0,
//           priceTo: selectedPrice.max ? selectedPrice.max : 999999999,
//           areas: tempArea,
//           sizeFrom: selectedSize.min ? selectedSize.min : 0,
//           sizeTo: selectedSize.max ? selectedSize.max : 99999999999,
//           namePrefix: searchParams.length > 0 ? searchParams : '',
//         };

//         const response = await getProjectByMap(data);

//         const convertedData = response.map((project: any) => ({
//           ...project,
//           nameId: project.projectName,
//           projectId: project._id,
//           picture:
//             project.pictures && project.pictures.length > 0
//               ? project.pictures[0]
//               : '/path/to/default/image.png',
//           coordinates:
//             project.coordinates && project.coordinates.lng && project.coordinates.lat
//               ? [parseFloat(project.coordinates.lng), parseFloat(project.coordinates.lat)]
//               : [-4.919074478458782, 36.511572005181534],
//         }));

//         setFilteredProperties(convertedData);
//       } catch (error) {
//         console.error('Failed to fetch data', error);
//       }
//     };

//     fetchData();
//   }, [tempArea, selectedBeds, selectedType, selectedSize, selectedPrice, searchParams]);

//   return (
//     <section
//       className={`map-fool-screen-container ${isFullScreenMap ? 'full-screen' : ''} ${
//         isClient && isMobile ? 'full-screen-mobile' : ''
//       }`}
//     >
//       {isClient && !isMobile && (
//         <div className="filter-block-map-container">
//           <FilterBlockMap filteredProperties={filteredProperties} />
//         </div>
//       )}
//       <div className="map-container">
//         <ProjectMapMap mapData={filteredProperties} isFullScreenMap={isFullScreenMap} />
//         <MapButtons action={handleFoolScreenMap} isFullScreenMap={isFullScreenMap} />
//       </div>
//     </section>
//   );
// };

// export default ProjectsMapContainer;

'use client';
import { useState, useEffect } from 'react';
import Image from 'next/image';
import { useMediaQuery } from 'react-responsive';
import FilterBlockMap from '@/components/projects-map/FilterBlockMap/FilterBlockMap';
import ProjectMapMap from '../ProjectsMapMap/ProjectMapMap';
import MapButtons from '../MapButtons/MapButtons';
import useBodyScrollLock from '@/utils/useBodyScrollLock';
import useFilterStore from '@/store/filterStore';
import { useTranslation } from 'react-i18next';
import '../../../app/[locale]/(main)/projects-map/projects-map.scss';
import { getProjectByMap } from '@/api/properties';
import { allAreasList } from '@/data/allAreasList';

const ProjectsMapContainer = () => {
  const { t } = useTranslation('projects-map');
  const [isClient, setIsClient] = useState<boolean>(false);
  const {
    searchParams,
    selectedType,
    selectedBeds,
    selectedPrice,
    selectedSize,
    selectedArea,
    polygonData,
    toggleReset,
    clearFilters,
    tempArea,
  } = useFilterStore();

  useEffect(() => {
    setIsClient(true);
  }, []);

  const isMobile = useMediaQuery({ maxWidth: 767 });
  const [isFullScreenMap, setIsFullScreenMap] = useState(false);
  useBodyScrollLock(isFullScreenMap);
  useBodyScrollLock(isMobile);

  const handleFoolScreenMap = () => {
    setIsFullScreenMap(!isFullScreenMap);
  };

  useEffect(() => {
    if (isMobile) {
      setIsFullScreenMap(true);
    }
  }, [isMobile]);

  const [filteredProperties, setFilteredProperties] = useState<any>([]);

  useEffect(() => {
    let activeRequest = true;

    const fetchData = async () => {
      try {
        const data = {
          bedrooms:
            selectedBeds.length > 0 ? selectedBeds : ['Studio', '1', '2', '3', '4', '5', '6', '7'],
          completion: ['Ready', 'Off plan', 'Special Offer'],
          location: '',
          type: selectedType.length
            ? selectedType
            : ['Apartment', 'Townhouse', 'Villa', 'Duplex', 'Flat', 'Penthouse', 'Ground floor'],
          priceFrom: selectedPrice.min ? selectedPrice.min : 0,
          priceTo: selectedPrice.max ? selectedPrice.max : 999999999,
          areas: tempArea,
          sizeFrom: selectedSize.min ? selectedSize.min : 0,
          sizeTo: selectedSize.max ? selectedSize.max : 99999999999,
          namePrefix: searchParams.length > 0 ? searchParams : '',
        };

        const response = await getProjectByMap(data);

        if (activeRequest) {
          const convertedData = response.map((project: any) => ({
            ...project,
            nameId: project._id,
            projectId: project._id,
            picture:
              project.pictures && project.pictures.length > 0
                ? project.pictures[0]
                : '/path/to/default/image.png',
            coordinates:
              project.coordinates && project.coordinates.lng && project.coordinates.lat
                ? [parseFloat(project.coordinates.lng), parseFloat(project.coordinates.lat)]
                : [-4.919074478458782, 36.511572005181534],
          }));
          setFilteredProperties(convertedData);
        }
      } catch (error) {
        console.error('Failed to fetch data', error);
        if (activeRequest) {
          setFilteredProperties([]);
        }
      }
    };

    fetchData();

    return () => {
      activeRequest = false; 
    };
  }, [tempArea, selectedBeds, selectedType, selectedSize, selectedPrice, searchParams]);

  return (
    <section
      className={`map-fool-screen-container ${isFullScreenMap ? 'full-screen' : ''} ${
        isClient && isMobile ? 'full-screen-mobile' : ''
      }`}
    >
      {isClient && !isMobile && (
        <div className="filter-block-map-container">
          <FilterBlockMap filteredProperties={filteredProperties} />
        </div>
      )}
      <div className="map-container">
        <ProjectMapMap mapData={filteredProperties} isFullScreenMap={isFullScreenMap} />
        <MapButtons action={handleFoolScreenMap} isFullScreenMap={isFullScreenMap} />
      </div>
    </section>
  );
};

export default ProjectsMapContainer;
