import { useEffect, useRef } from 'react';
import './popupErrorPolygons.scss';
import Popup from '../ui/Popup';
import { useTranslation } from 'react-i18next';

interface PopupErrorPolygonsProps {
  handler: (isVisible: boolean) => void;
  isVisible: boolean;
}

const PopupErrorPolygons = ({ handler, isVisible }: PopupErrorPolygonsProps) => {
  const contentRef = useRef<HTMLDivElement | null>(null);
  const { t } = useTranslation('properties');

  useEffect(() => {
    const handleClickOutside = (event: MouseEvent) => {
      if (contentRef.current && !contentRef.current.contains(event.target as Node)) {
        handler(false);
      }
    };

    if (isVisible) {
      document.addEventListener('mousedown', handleClickOutside);
    } else {
      document.removeEventListener('mousedown', handleClickOutside);
    }

    return () => {
      document.removeEventListener('mousedown', handleClickOutside);
    };
  }, [isVisible, handler]);

  return (
    <div className="popupFloorPlan" style={{ position: 'relative' }}>
      <Popup isVisible={isVisible}>
        <div className="popupFloorPlan__content" ref={contentRef}>
          <p className="popupFloorPlan__content-title">{t('offers-list.showingBtn')}</p>
          <button className="popupFloorPlan__content-btn" onClick={() => handler(false)}>
            {t('offers-list.okBtn')}
          </button>
        </div>
      </Popup>
    </div>
  );
};

export default PopupErrorPolygons;
