'use client';
import SelectedSwiperSwiper from './SelectedSwiperSwiper';
import './SelectedSwiper.scss';
import { useEffect, useState } from 'react';
import { allAreasList } from '@/data/allAreasList';
import { getAllFilteredProjects } from '@/api/properties';
const SelectedSwiper = () => {
  const [isLoading, setIsLoading] = useState<boolean>(true);

  const [data, setData] = useState<any>([]);

  useEffect(() => {
    const fetchData = async () => {
      try {
        setIsLoading(true);

        const data = {
          bedrooms: ['Studio', '1', '2', '3', '4', '5', '6', '7'],
          completion: ['Ready', 'Off plan', 'Special Offer'],
          location: '',
          type: ['Apartment', 'Townhouse', 'Villa', 'Duplex', 'Flat', 'Penthouse', 'Ground floor'],
          priceFrom: 0,
          priceTo: 999999999,
          areas: allAreasList,
          sizeFrom: 0,
          sizeTo: 99999999999,
          namePrefix: '',
        };

        const response = await getAllFilteredProjects(data, 0, 20);

        const convertedData = response.projects.map((project: any) => ({
          ...project,
          projectId: project._id,
        }));

        setData(convertedData);
      } catch (error) {
        console.error('Failed to fetch data', error);
        setData([]);
      } finally {
        setIsLoading(false);
      }
    };

    fetchData();
  }, []);

  return <SelectedSwiperSwiper data={data} />;
};
export default SelectedSwiper;
