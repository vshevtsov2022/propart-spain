'use client';
import { useState, useRef } from 'react';
import { useClickOutside } from '@/utils/useClickOutside';
import './HeroFilterSelect.scss';

interface HeroFilterSelectProps {
  action: (value: string[]) => void;
  areasData: any;
  selectedAreas: string[];
  value: string;
}

const HeroFilterSelect = ({ action, areasData, selectedAreas, value }: HeroFilterSelectProps) => {
  const triggerRef = useRef(null);
  const menuRef = useRef(null);
  const [showAreas, setShowAreas] = useState(false);
  const [expandedArea, setExpandedArea] = useState(null);

  useClickOutside(showAreas, [triggerRef, menuRef], () => setShowAreas(false));

  const handleAreaClick = (area: any, hasSubAreas: any) => {
    if (hasSubAreas) {
      toggleSubAreas(area);
    } else {
      let updatedSelectedAreas;
      if (selectedAreas.includes(area)) {
        // Remove if exists in selectedAreas
        updatedSelectedAreas = selectedAreas.filter(selected => selected !== area);
      } else {
        // Add if doesn't exist in selectedAreas
        updatedSelectedAreas = [...selectedAreas, area];
      }
      action(updatedSelectedAreas);
    }
  };

  const toggleSubAreas = (area: any) => {
    if (expandedArea === area) {
      setExpandedArea(null);
    } else {
      setExpandedArea(area);
    }
  };

  return (
    <>
      <button
        ref={triggerRef}
        className="hero-dropdownList__trigger"
        onClick={event => {
          event.preventDefault();
          setShowAreas(!showAreas);
        }}
      >
        <span className="text">{selectedAreas.length > 0 ? selectedAreas.join(', ') : value}</span>
        <span className={`icon ${showAreas ? 'active' : ''}`}>
          <svg
            width="15"
            height="8"
            viewBox="0 0 15 8"
            fill="none"
            xmlns="http://www.w3.org/2000/svg"
          >
            <path
              d="M7.8538 7.8351L14.2835 1.20467C14.6847 0.79094 14.4415 0 13.9131 0H1.0537C0.525289 0 0.282089 0.79094 0.683299 1.20467L7.113 7.8351C7.3261 8.0549 7.6407 8.0549 7.8538 7.8351Z"
              fill="currentColor"
            />
          </svg>
        </span>
      </button>

      <div ref={menuRef} className={`hero-dropdownList__menu ${showAreas ? 'active' : ''}`}>
        {areasData && Array.isArray(areasData) && areasData.length > 0 && (
          <ul>
            {areasData.map((item, index) => (
              <li key={index}>
                <div
                  className="hero-dropdownList__list__item"
                  onClick={() =>
                    handleAreaClick(item.label, item.subAreas && item.subAreas.length > 0)
                  }
                >
                  {item.label}
                  {item.subAreas.length === 0 ? (
                    <span
                      className={`icon ${selectedAreas.includes(item.label) ? 'selected' : ''}`}
                    />
                  ) : (
                    <svg
                      width="12"
                      height="6"
                      viewBox="0 0 15 8"
                      fill="none"
                      xmlns="http://www.w3.org/2000/svg"
                    >
                      <path
                        d="M7.8538 7.8351L14.2835 1.20467C14.6847 0.79094 14.4415 0 13.9131 0H1.0537C0.525289 0 0.282089 0.79094 0.683299 1.20467L7.113 7.8351C7.3261 8.0549 7.6407 8.0549 7.8538 7.8351Z"
                        fill="currentColor"
                      />
                    </svg>
                  )}
                </div>
                {expandedArea === item.label && item.subAreas && item.subAreas.length > 0 && (
                  <ul className="hero-dropdownList__subareas">
                    {item.subAreas.map((subItem: any, subIndex: any) => (
                      <li
                        className="hero-dropdownList__list__item"
                        onClick={() => handleAreaClick(subItem.label, false)}
                        key={subIndex}
                      >
                        <span className="dropdownList__subareas_item">{subItem.label}</span>
                        <span
                          className={`icon ${
                            selectedAreas.includes(subItem.label) ? 'selected' : ''
                          }`}
                        />
                      </li>
                    ))}
                  </ul>
                )}
              </li>
            ))}
          </ul>
        )}
      </div>
    </>
  );
};

export default HeroFilterSelect;
