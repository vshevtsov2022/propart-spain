'use client';
import './HeroFilterRadioButtons.scss';

interface HeroFilterRadioButtonsProps {
  radio1: string;
  radio2: string;
  selectedType: string;
  action: (value: string) => void;
}

const HeroFilterRadioButtons = ({  selectedType, action, radio1, radio2 }: HeroFilterRadioButtonsProps) => {
  return (
    <div className="hero-form-buttons">
      <button
        className={`form-button ${selectedType === 'newBuilding' ? 'active' : ''}`}
        onClick={event => {
          event.preventDefault();
          action('newBuilding');
        }}
      >
        <span className="radio"></span>
        <span className="title">{radio1}</span>
      </button>
      <span className='middle'></span>
      <button
        className={`form-button ${selectedType === 'specialOffer' ? 'active' : ''}`}
        onClick={event => {
          event.preventDefault();
          action('specialOffer');
        }}
      >
        <span className="radio"></span>
        <span className="title">{radio2}</span>
      </button>
    </div>
  );
};
export default HeroFilterRadioButtons;
