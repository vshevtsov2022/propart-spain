'use client'
import { SquarePeg } from '@/fonts';
import './AttentionSection.scss';
import Image from 'next/image';
import React from 'react';
import { useTranslation } from 'react-i18next';
const AttentionSection = () => {
  const { t } = useTranslation('home');

  const handleDownload = () => {
    const link = document.createElement('a');

    link.href = '/pdf/top5en.pdf';  // Вказуємо шлях до вашого PDF файлу
    link.download = 'top-projects.pdf';  // Ім'я файлу, яке користувач побачить при збереженні
    document.body.appendChild(link);
    link.click();
    document.body.removeChild(link);
  };

  return (
    <section className="attentionSection">
      <div className="content">
        <h2 className="title">{t('attention.title')}</h2>
        <p>{t('attention.description')}</p>
        <div className={`${SquarePeg.className} exclusive`}>Exclusive selection from ProPart</div>
        <button className="button" onClick={handleDownload}>{t('attention.button')}</button>
      </div>
      <div className="image">
        <Image src='/main/section4attention/Mockup1.png' fill sizes='100%' alt='sd'/>
      </div>
      <div className="contentMobile">
        <h2 className="titlemobile">{t('attention.title-mob')}</h2>
        <button className="buttonmobile" onClick={handleDownload}>{t('attention.button-mob')}</button>
      </div>
    </section>
  );
};

export default AttentionSection;

