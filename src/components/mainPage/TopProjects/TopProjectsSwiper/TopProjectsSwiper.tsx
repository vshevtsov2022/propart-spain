'use client';
import Image from 'next/image';
import Link from 'next/link';
import { Swiper, SwiperSlide } from 'swiper/react';
import { Pagination } from 'swiper/modules';
import 'swiper/css';
import 'swiper/css/pagination';
import './TopProjectsSwiper.scss';

interface TopProjectsSwiperProps {
  swiperRef: React.MutableRefObject<any | null>;
  topProjects: any;
}

const TopProjectsSwiper = ({ swiperRef, topProjects }: TopProjectsSwiperProps) => {
  // console.log(topProjects);

  return (
    <>

    </>
  );
};
export default TopProjectsSwiper;
