'use client';
import { useState, useEffect } from 'react';
import MainMapMap from './MainMapMap/MainMapMap';
import AreasSwiperSlide from '../AreasSwiper3/AreasSwiperSlide/AreasSwiperSlide';
import { useMediaQuery } from 'react-responsive';
import './MainMap.scss';
import { useTranslation } from 'react-i18next';
import { allAreasList } from '@/data/allAreasList';
import { getProjectByMap } from '@/api/properties';

export interface MapDataProps {
  projectId: string;
  picture: string;
  name: string;
  price_from: number;
  coordinates: number[];
}

const MainMap = () => {
  const { t } = useTranslation('home');
  const [isClient, setIsClient] = useState(false);
  const tablet = useMediaQuery({ minWidth: 768 });
  const [isLoading, setIsLoading] = useState<boolean>(true);

  const [mapData, setMapData] = useState<MapDataProps[]>([]);

  useEffect(() => {
    const fetchData = async () => {
      try {
        setIsLoading(true);

        const data = {
          bedrooms: ['Studio', '1', '2', '3', '4', '5', '6', '7'],
          completion: ['Ready', 'Off plan', 'Special Offer'],
          location: '',
          type: ['Apartment', 'Townhouse', 'Villa', 'Duplex', 'Flat', 'Penthouse', 'Ground floor'],
          priceFrom: 0,
          priceTo: 999999999,
          areas: allAreasList,
          sizeFrom: 0,
          sizeTo: 99999999999,
          namePrefix: '',
        };

        const response = await getProjectByMap(data);

        const convertedData = response.map((project: any) => ({
          nameId: project._id,
          projectId: project._id,
          picture:
            project.pictures && project.pictures.length > 0
              ? project.pictures[0]
              : '/path/to/default/image.png',
          name: project.name,
          price_from: project.price_from,
          coordinates:
            project.coordinates && project.coordinates.lng && project.coordinates.lat
              ? [parseFloat(project.coordinates.lng), parseFloat(project.coordinates.lat)]
              : [-4.919074478458782, 36.511572005181534],
        }));

        setMapData(convertedData);
      } catch (error) {
        console.error('Failed to fetch data', error);
        setMapData([]);
      } finally {
        setIsLoading(false);
      }
    };

    fetchData();
  }, []);

  useEffect(() => {
    setIsClient(true);
  }, []);

  return (
    isClient &&
    tablet && (
      <section className="container">
        <div className="main-map">
          <div className="main-map__active-area">
            <AreasSwiperSlide
              title="Estepona"
              price={t('areas.project1-price')}
              bestFor={t('areas.project1-best-for')}
              airport={t('areas.project1-airport')}
              description={t('areas.map-description')}
              path="/main/section3areaSlider/aboutAreaSliderImg1.png"
              linkPath="areas/estepona"
            />
          </div>
          {isLoading ? (
            <></>
          ) : (
            <MainMapMap mapData={mapData} mapCoordinates={[-5.146848, 36.426807]} mapZoom={11} />
          )}
        </div>
      </section>
    )
  );
};

export default MainMap;
