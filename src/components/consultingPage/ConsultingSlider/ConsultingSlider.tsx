'use client';

import Image from 'next/image';
import Link from 'next/link';
import 'swiper/css';
import { Swiper, SwiperSlide } from 'swiper/react';
import './ConsultingSlider.scss';
import { useEffect, useState } from 'react';
import { getAllFilteredProjects, getProjectByMap } from '@/api/properties';
import { allAreasList } from '@/data/allAreasList';

const ConsultingSlider = () => {
  const [sliderData, setSliderData] = useState<any>([]);

  useEffect(() => {
    const fetchData = async () => {
      try {
        const data = {
          bedrooms: ['Studio', '1', '2', '3', '4', '5', '6', '7'],
          completion: ['Ready', 'Off plan', 'Special Offer'],
          location: '',
          type: ['Apartment', 'Townhouse', 'Villa', 'Duplex', 'Flat', 'Penthouse', 'Ground floor'],
          priceFrom: 400000,
          priceTo: 700000,
          areas: allAreasList,
          sizeFrom: 0,
          sizeTo: 999999999,
          namePrefix: '',
        };

        const response = await getProjectByMap(data);

        const getRandomElements = (arr: any[], count: number) => {
          const shuffled = arr.sort(() => 0.5 - Math.random());
          return shuffled.slice(0, count);
        };

        const randomProjects = getRandomElements(response, 3);

        const convertedData = randomProjects.slice(0, 3).map((project: any) => ({
          ...project,
          id: project._id,
          name: project.name,
          area: project.location,
          price: project.price_from,
          path:
            project.pictures && project.pictures.length > 0
              ? project.pictures[0]
              : '/path/to/default/image.png',
        }));

        setSliderData(convertedData);
      } catch (error) {
        setSliderData([]);
        console.error('Failed to fetch data', error);
      }
    };

    fetchData();
  }, []);

  return (
    <Swiper
      className="consulting-swiper"
      slidesPerView="auto"
      spaceBetween={35}
      breakpoints={{
        1024: {
          slidesPerView: 3,
          direction: 'vertical',
        },
      }}
    >
      {sliderData &&
        Array.isArray(sliderData) &&
        sliderData.length > 0 &&
        sliderData.map(slide => (
          <SwiperSlide key={slide.id} className="slide">
            <Link href={`/properties/${slide._id}`} className="link">
              <div className="image">
                <div
                  style={{
                    backgroundImage: `url(${slide.path})`,
                    backgroundRepeat: 'no-repeat',
                    backgroundSize: 'cover',
                    backgroundPosition: 'center',
                    width: '100%',
                    height: '100%',
                  }}
                ></div>
                <div className="bg"></div>
              </div>
              <div className="text">
                <span className="title">{slide.name}</span>
                <div className="bottom">
                  <span className="area">{slide.area}</span>
                  <span className="price">
                    € {slide.price.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',')}
                  </span>
                </div>
              </div>
            </Link>
          </SwiperSlide>
        ))}
    </Swiper>
  );
};

export default ConsultingSlider;
