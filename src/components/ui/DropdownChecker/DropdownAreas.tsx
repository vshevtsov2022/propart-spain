// 'use client';
// import { useState } from 'react';
// import './DropdownChecker.scss';

// interface PriceCheckerProps {
//   data: any;
//   title: string;
//   selectValue: string[];
//   action: (values: string[]) => void;
//   isVisibleText?: boolean;
// }

// const DropdownAreas: React.FC<PriceCheckerProps> = ({
//   data,
//   title,
//   selectValue,
//   action,
//   isVisibleText,
// }) => {
//   const [showBlock, setShowBlock] = useState<boolean>(false);
//   const [expandedAreas, setExpandedAreas] = useState<string[]>([]);

//   const handleCheckClick = (value: string) => {
//     let updatedValues;
//     if (selectValue.includes(value)) {
//       // Видалити якщо є у selectedAreas
//       updatedValues = selectValue.filter(selected => selected !== value);
//     } else {
//       // Додати якщо нема у selectedAreas
//       updatedValues = [...selectValue, value];
//     }
//     action(updatedValues);
//   };

//   const handleExpandClick = (value: string) => {
//     if (expandedAreas.includes(value)) {
//       setExpandedAreas(expandedAreas.filter(area => area !== value));
//     } else {
//       setExpandedAreas([...expandedAreas, value]);
//     }
//   };

//   return (
//     <div
//       onMouseEnter={() => setShowBlock(true)}
//       onMouseLeave={() => setShowBlock(false)}
//       className="checker"
//     >
//       <button
//         className="trigger"
//         onClick={event => {
//           event.preventDefault();
//           setShowBlock(!showBlock);
//         }}
//       >
//         <div>
//           {title}
//           <svg
//             width="16"
//             height="16"
//             viewBox="0 0 19 18"
//             fill="none"
//             xmlns="http://www.w3.org/2000/svg"
//           >
//             <path
//               d="M9.86764 11.8764L14.6899 6.9035C14.9908 6.5932 14.8084 6 14.4121 6H4.76757C4.37126 6 4.18886 6.5932 4.48976 6.9035L9.31203 11.8764C9.47189 12.0412 9.70779 12.0412 9.86764 11.8764Z"
//               fill="currentColor"
//             />
//           </svg>
//         </div>
//         {isVisibleText ? (
//           <span className={`${selectValue.length !== 0 ? 'activeVisible' : ''}`}>
//             {selectValue.length > 3
//               ? selectValue.slice(0, 3).join(', ') + '...'
//               : selectValue.join(', ')}
//           </span>
//         ) : (
//           <span className={`${selectValue.length !== 0 ? 'activeVisible' : ''}`}>
//             {selectValue.length > 2
//               ? selectValue.slice(0, 2).join(', ') + '...'
//               : selectValue.join(', ')}
//           </span>
//         )}
//         {isVisibleText && (
//           <div className="activeVisible">{selectValue.length > 3 && 'New level of filter'}</div>
//         )}
//       </button>
//       <div className={`checker-list-container ${showBlock ? 'active' : ''}`}>
//         <ul className="checker-ul">
//           {data &&
//             data.map((item: any) => (
//               <li key={item.label}>
//                 <div
//                   onClick={() => handleExpandClick(item.value)}
//                   className={`checker-list-item ${
//                     expandedAreas.includes(item.value) ? 'expanded' : ''
//                   }`}
//                 >
//                   <span className="checker-icon">
//                     {item.subAreas && item.subAreas.length === 0 ? (
//                       <svg
//                         className="hide"
//                         xmlns="http://www.w3.org/2000/svg"
//                         width="20"
//                         height="20"
//                         viewBox="0 0 24 24"
//                         fill="none"
//                       >
//                         <path
//                           d="M2 12C2 7.28595 2 4.92893 3.46447 3.46447C4.92893 2 7.28595 2 12 2C16.714 2 19.0711 2 20.5355 3.46447C22 4.92893 22 7.28595 22 12C22 16.714 22 19.0711 20.5355 20.5355C19.0711 22 16.714 22 12 22C7.28595 22 4.92893 22 3.46447 20.5355C2 19.0711 2 16.714 2 12Z"
//                           stroke="currentColor"
//                           strokeWidth="1.5"
//                         />
//                       </svg>
//                     ) : (
//                       <svg
//                         width="16"
//                         height="16"
//                         viewBox="0 0 19 18"
//                         fill="none"
//                         xmlns="http://www.w3.org/2000/svg"
//                       >
//                         <path
//                           d="M9.86764 11.8764L14.6899 6.9035C14.9908 6.5932 14.8084 6 14.4121 6H4.76757C4.37126 6 4.18886 6.5932 4.48976 6.9035L9.31203 11.8764C9.47189 12.0412 9.70779 12.0412 9.86764 11.8764Z"
//                           fill="currentColor"
//                         />
//                       </svg>
//                     )}
//                   </span>
//                   {item.label}
//                 </div>
//                 {expandedAreas.includes(item.value) && item.subAreas.length > 0 && (
//                   <ul className="subArea-list">
//                     {item.subAreas.map((subItem: any) => (
//                       <li
//                         onClick={e => {
//                           e.stopPropagation();
//                           handleCheckClick(subItem.value);
//                         }}
//                         className={`checker-list-item ${
//                           selectValue.includes(subItem.value) ? 'active' : ''
//                         }`}
//                         key={subItem.label}
//                       >
//                         <span className="checker-icon">
//                           {selectValue.includes(subItem.value) ? (
//                             <svg
//                               className="hide"
//                               xmlns="http://www.w3.org/2000/svg"
//                               width="20"
//                               height="20"
//                               viewBox="0 0 24 24"
//                               fill="none"
//                             >
//                               <path
//                                 fillRule="evenodd"
//                                 clipRule="evenodd"
//                                 d="M12 22C7.28595 22 4.92893 22 3.46447 20.5355C2 19.0711 2 16.714 2 12C2 7.28595 2 4.92893 3.46447 3.46447C4.92893 2 7.28595 2 12 2C16.714 2 19.0711 2 20.5355 3.46447C22 4.92893 22 7.28595 22 12C22 16.714 22 19.0711 20.5355 20.5355C19.0711 22 16.714 22 12 22ZM16.0303 8.96967C16.3232 9.26256 16.3232 9.73744 16.0303 10.0303L11.0303 15.0303C10.7374 15.3232 10.2626 15.3232 9.96967 15.0303L7.96967 13.0303C7.67678 12.7374 7.67678 12.2626 7.96967 11.9697C8.26256 11.6768 8.73744 11.6768 9.03033 11.9697L10.5 13.4393L14.9697 8.96967C15.2626 8.67678 15.7374 8.67678 16.0303 8.96967Z"
//                                 fill="#DBA77B"
//                               />
//                             </svg>
//                           ) : (
//                             <svg
//                               className="hide"
//                               xmlns="http://www.w3.org/2000/svg"
//                               width="20"
//                               height="20"
//                               viewBox="0 0 24 24"
//                               fill="none"
//                             >
//                               <path
//                                 d="M2 12C2 7.28595 2 4.92893 3.46447 3.46447C4.92893 2 7.28595 2 12 2C16.714 2 19.0711 2 20.5355 3.46447C22 4.92893 22 7.28595 22 12C22 16.714 22 19.0711 20.5355 20.5355C19.0711 22 16.714 22 12 22C7.28595 22 4.92893 22 3.46447 20.5355C2 19.0711 2 16.714 2 12Z"
//                                 stroke="currentColor"
//                                 strokeWidth="1.5"
//                               />
//                             </svg>
//                           )}
//                         </span>
//                         {subItem.label}
//                       </li>
//                     ))}
//                   </ul>
//                 )}
//               </li>
//             ))}
//         </ul>
//       </div>
//     </div>
//   );
// };

// export default DropdownAreas;

'use client';
import { useState } from 'react';
import './DropdownChecker.scss';

interface SubArea {
  value: string;
  label: string;
}

interface AreaOption {
  value: string;
  label: string;
  subAreas?: SubArea[];
}

interface PriceCheckerProps {
  data: AreaOption[];
  title: string;
  selectValue: string[];
  action: (values: string[]) => void;
  isVisibleText?: boolean;
}

const DropdownAreas: React.FC<PriceCheckerProps> = ({
  data,
  title,
  selectValue,
  action,
  isVisibleText,
}) => {
  const [showBlock, setShowBlock] = useState<boolean>(false);
  const [expandedAreas, setExpandedAreas] = useState<string[]>([]);

  const handleCheckClick = (value: string) => {
    let updatedValues;
    if (selectValue.includes(value)) {
      // Remove if present in selectedAreas
      updatedValues = selectValue.filter(selected => selected !== value);
    } else {
      // Add if not present in selectedAreas
      updatedValues = [...selectValue, value];
    }
    action(updatedValues);
  };

  const handleExpandClick = (value: string) => {
    if (expandedAreas.includes(value)) {
      setExpandedAreas(expandedAreas.filter(area => area !== value));
    } else {
      setExpandedAreas([...expandedAreas, value]);
    }
  };

  return (
    <div
      onMouseEnter={() => setShowBlock(true)}
      onMouseLeave={() => setShowBlock(false)}
      className="checker"
    >
      <button
        className="trigger"
        onClick={event => {
          event.preventDefault();
          setShowBlock(!showBlock);
        }}
      >
        <div>
          {title}
          <svg
            width="16"
            height="16"
            viewBox="0 0 19 18"
            fill="none"
            xmlns="http://www.w3.org/2000/svg"
          >
            <path
              d="M9.86764 11.8764L14.6899 6.9035C14.9908 6.5932 14.8084 6 14.4121 6H4.76757C4.37126 6 4.18886 6.5932 4.48976 6.9035L9.31203 11.8764C9.47189 12.0412 9.70779 12.0412 9.86764 11.8764Z"
              fill="currentColor"
            />
          </svg>
        </div>
        {isVisibleText ? (
          <span className={`${selectValue.length !== 0 ? 'activeVisible' : ''}`}>
            {selectValue.length > 3
              ? selectValue.slice(0, 3).join(', ') + '...'
              : selectValue.join(', ')}
          </span>
        ) : (
          <span className={`${selectValue.length !== 0 ? 'activeVisible' : ''}`}>
            {selectValue.length > 2
              ? selectValue.slice(0, 2).join(', ') + '...'
              : selectValue.join(', ')}
          </span>
        )}
        {isVisibleText && (
          <div className="activeVisible">{selectValue.length > 3 && 'New level of filter'}</div>
        )}
      </button>
      <div className={`checker-list-container ${showBlock ? 'active' : ''}`}>
        <ul className="checker-ul">
          {data &&
            data.map(item => (
              <li key={item.label}>
                <div
                  onClick={() => {
                    if (item.subAreas && item.subAreas.length > 0) {
                      handleExpandClick(item.value);
                    } else {
                      handleCheckClick(item.value);
                    }
                  }}
                  className={`checker-list-item ${
                    selectValue.includes(item.value) ? 'active' : ''
                  }`}
                >
                  <span className="checker-icon">
                    {item.subAreas && item.subAreas.length > 0 ? (
                      <svg
                        width="16"
                        height="16"
                        viewBox="0 0 19 18"
                        fill="none"
                        xmlns="http://www.w3.org/2000/svg"
                      >
                        <path
                          d="M9.86764 11.8764L14.6899 6.9035C14.9908 6.5932 14.8084 6 14.4121 6H4.76757C4.37126 6 4.18886 6.5932 4.48976 6.9035L9.31203 11.8764C9.47189 12.0412 9.70779 12.0412 9.86764 11.8764Z"
                          fill="currentColor"
                        />
                      </svg>
                    ) : (
                      <span className="checkbox-icon">
                        {selectValue.includes(item.value) ? (
                          <svg
                            className="hide"
                            xmlns="http://www.w3.org/2000/svg"
                            width="20"
                            height="20"
                            viewBox="0 0 24 24"
                            fill="none"
                          >
                            <path
                              fillRule="evenodd"
                              clipRule="evenodd"
                              d="M12 22C7.28595 22 4.92893 22 3.46447 20.5355C2 19.0711 2 16.714 2 12C2 7.28595 2 4.92893 3.46447 3.46447C4.92893 2 7.28595 2 12 2C16.714 2 19.0711 2 20.5355 3.46447C22 4.92893 22 7.28595 22 12C22 16.714 22 19.0711 20.5355 20.5355C19.0711 22 16.714 22 12 22ZM16.0303 8.96967C16.3232 9.26256 16.3232 9.73744 16.0303 10.0303L11.0303 15.0303C10.7374 15.3232 10.2626 15.3232 9.96967 15.0303L7.96967 13.0303C7.67678 12.7374 7.67678 12.2626 7.96967 11.9697C8.26256 11.6768 8.73744 11.6768 9.03033 11.9697L10.5 13.4393L14.9697 8.96967C15.2626 8.67678 15.7374 8.67678 16.0303 8.96967Z"
                              fill="#DBA77B"
                            />
                          </svg>
                        ) : (
                          <svg
                            className="hide"
                            xmlns="http://www.w3.org/2000/svg"
                            width="20"
                            height="20"
                            viewBox="0 0 24 24"
                            fill="none"
                          >
                            <path
                              d="M2 12C2 7.28595 2 4.92893 3.46447 3.46447C4.92893 2 7.28595 2 12 2C16.714 2 19.0711 2 20.5355 3.46447C22 4.92893 22 7.28595 22 12C22 16.714 22 19.0711 20.5355 20.5355C19.0711 22 16.714 22 12 22C7.28595 22 4.92893 22 3.46447 20.5355C2 19.0711 2 16.714 2 12Z"
                              stroke="currentColor"
                              strokeWidth="1.5"
                            />
                          </svg>
                        )}
                      </span>
                    )}
                  </span>
                  {item.label}
                </div>
                {expandedAreas.includes(item.value) &&
                  item.subAreas &&
                  item.subAreas.length > 0 && (
                    <ul className="subArea-list">
                      {item.subAreas.map(subItem => (
                        <li
                          onClick={e => {
                            e.stopPropagation();
                            handleCheckClick(subItem.value);
                          }}
                          className={`checker-list-item ${
                            selectValue.includes(subItem.value) ? 'active' : ''
                          }`}
                          key={subItem.label}
                        >
                          <span className="checker-icon">
                            {selectValue.includes(subItem.value) ? (
                              <svg
                                className="hide"
                                xmlns="http://www.w3.org/2000/svg"
                                width="20"
                                height="20"
                                viewBox="0 0 24 24"
                                fill="none"
                              >
                                <path
                                  fillRule="evenodd"
                                  clipRule="evenodd"
                                  d="M12 22C7.28595 22 4.92893 22 3.46447 20.5355C2 19.0711 2 16.714 2 12C2 7.28595 2 4.92893 3.46447 3.46447C4.92893 2 7.28595 2 12 2C16.714 2 19.0711 2 20.5355 3.46447C22 4.92893 22 7.28595 22 12C22 16.714 22 19.0711 20.5355 20.5355C19.0711 22 16.714 22 12 22ZM16.0303 8.96967C16.3232 9.26256 16.3232 9.73744 16.0303 10.0303L11.0303 15.0303C10.7374 15.3232 10.2626 15.3232 9.96967 15.0303L7.96967 13.0303C7.67678 12.7374 7.67678 12.2626 7.96967 11.9697C8.26256 11.6768 8.73744 11.6768 9.03033 11.9697L10.5 13.4393L14.9697 8.96967C15.2626 8.67678 15.7374 8.67678 16.0303 8.96967Z"
                                  fill="#DBA77B"
                                />
                              </svg>
                            ) : (
                              <svg
                                className="hide"
                                xmlns="http://www.w3.org/2000/svg"
                                width="20"
                                height="20"
                                viewBox="0 0 24 24"
                                fill="none"
                              >
                                <path
                                  d="M2 12C2 7.28595 2 4.92893 3.46447 3.46447C4.92893 2 7.28595 2 12 2C16.714 2 19.0711 2 20.5355 3.46447C22 4.92893 
22 7.28595 22 12C22 16.714 22 19.0711 20.5355 20.5355C19.0711 22 16.714 22 12 22C7.28595 22 4.92893 22 3.46447 20.5355C2 19.0711 2 16.714 2 12Z"
                                  stroke="currentColor"
                                  strokeWidth="1.5"
                                />
                              </svg>
                            )}
                          </span>
                          {subItem.label}
                        </li>
                      ))}
                    </ul>
                  )}
              </li>
            ))}
        </ul>
      </div>
    </div>
  );
};

export default DropdownAreas;
