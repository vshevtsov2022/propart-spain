'use client';
import React, { useState, useEffect, useRef } from 'react';
import { usePathname, useRouter } from 'next/navigation';
import { useClickOutside } from '@/utils/useClickOutside';
import { useMediaQuery } from 'react-responsive';
import { FilterProjectNameType } from '@/types';
import './MapSearch.scss';
import { useTranslation } from 'react-i18next';
import useFilterStore from '@/store/filterStore';

interface SearchBarProps {
  projectsNameData: FilterProjectNameType[];
  reset: boolean;
  //виконується при кліку по елементу списку
  handleNameChange: (name: string, coordinates: [number, number], zoom: number) => void;
  //виконується при зміні значення input
  handleSearch: (value: string) => void;
}

const MapSearch = ({ handleNameChange, handleSearch, projectsNameData, reset }: SearchBarProps) => {
  const { t } = useTranslation('map-filters');
  const currentPathname = usePathname();
  const router = useRouter();
  const [data, setData] = useState<FilterProjectNameType[]>(projectsNameData);
  const [visible, setVisible] = useState(false);
  const tablet = useMediaQuery({ maxWidth: 1150 });
  const searchRef = useRef<HTMLDivElement>(null);
  const { searchParams } = useFilterStore();
  // const [query, setQuery] = useState(searchParams);
  const [query, setQuery] = useState('');
  useClickOutside(visible, [searchRef], () => setVisible(false));
  const activeVisible = () => {
    if (!visible && tablet) {
      setVisible(true);
    }
  };
  const handleSearchSubmit = (e: React.FormEvent<HTMLFormElement>) => {
    e.preventDefault();
  };
  //очистити інпут та оновити дані мапи
  const handleClearQuery = () => {
    handleNameChange('', [-4.883333, 36.516666], 11);
    setQuery('');
    handleSearch('');
    setVisible(false);
  };
  const projectsMapRegex = /\/projects-map/;
  //клік по обраному елементу списку
  const handleProjectChoise = (name: string, coordinates: [number, number], id: string) => {
    // Перевірка на те, що currentPathname не є null
    if (currentPathname) {
      // Реалізувати перевірку, якщо сторінка карти одна функція, або інша
      if (projectsMapRegex.test(currentPathname)) {
        handleNameChange(name, coordinates, 15); // Оновлює карту
      }
      // Якщо сторінка проперті треба реалізувати щоб відбувався перехід по id
      else {
        router.push(`/properties/${id}`);
      }
    } else {
      // Якщо currentPathname є null, обробити це відповідно
      console.error('currentPathname is null, unable to perform regex test or routing.');
    }

    setQuery(name);
    setVisible(false);
  };

  //
  const handleSearchQuery = (event: React.ChangeEvent<HTMLInputElement>) => {
    setQuery(event.target.value);
    handleSearch(event.target.value);
  };

  // Очищення query при функції reset
  useEffect(() => {
    setQuery(searchParams);
    handleSearch(searchParams);
    //eslint-disable-next-line
  }, [reset]);

  //фільтрація даних списку
  useEffect(() => {
    if (projectsNameData && projectsNameData.length > 0) {
      const filteredData = projectsNameData.filter(
        item => item.name?.toLowerCase().includes(query.toLowerCase()) // Додаємо перевірку на існування name
      );
      setData(filteredData);
    }
    //eslint-disable-next-line
  }, [query]);

  return (
    <div className="map-search-block-container" ref={searchRef} onClick={activeVisible}>
      <form
        className={`map-search-block-form ${
          tablet && visible && data && data.length > 0 ? 'active' : ''
        }`}
        onSubmit={handleSearchSubmit}
      >
        <span className="search-icon">
          <svg
            xmlns="http://www.w3.org/2000/svg"
            width="20"
            height="20"
            viewBox="0 0 20 20"
            fill="none"
          >
            <g clipPath="url(#clip0_2153_16900)">
              <circle
                cx="9.58073"
                cy="9.58463"
                r="7.91667"
                stroke="currentColor"
                strokeWidth="1.5"
              ></circle>
              <path
                d="M15.4141 15.418L18.3307 18.3346"
                stroke="currentColor"
                strokeWidth="1.5"
                strokeLinecap="round"
              ></path>
            </g>
            <defs>
              <clipPath id="clip0_2153_16900">
                <rect width="20" height="20" fill="white"></rect>
              </clipPath>
            </defs>
          </svg>
        </span>

        <input
          className="map-search-block-input"
          value={query}
          onChange={e => handleSearchQuery(e)}
          placeholder={t('search_placeholder')}
          type="text"
          onFocus={() => setVisible(true)}
        />
        {/* {query.length > 0 && (
          <button
            className={`map-search-block-input-close ${tablet && visible ? 'active' : ''}`}
            onClick={handleClearQuery}
          >
            <svg
              width="16px"
              height="16px"
              viewBox="0 0 16 16"
              fill="currentColor"
              data-category="application"
            >
              <path d="M2.784 2.089l.069.058 5.146 5.147 5.146-5.147a.5.5 0 01.765.638l-.058.069L8.705 8l5.147 5.146a.5.5 0 01-.638.765l-.069-.058-5.146-5.147-5.146 5.147a.5.5 0 01-.765-.638l.058-.069L7.293 8 2.146 2.854a.5.5 0 01.638-.765z"></path>
            </svg>
          </button>
        )} */}
      </form>

      {visible && currentPathname && projectsMapRegex.test(currentPathname) && data.length > 0 && (
        <div className="map-search-block-searchList">
          {Array.isArray(data) && data.length > 0 ? (
            <ul className="map-search-block-searchList-ul">
              {data.map((item, i) => (
                <li
                  className={query === item.name ? 'active' : ''}
                  onClick={() => {
                    handleProjectChoise(item.name, item.coordinates, item.id);
                  }}
                  key={i}
                >
                  {item.name}
                </li>
              ))}
            </ul>
          ) : (
            <div className="map-search-block-searchList-error">{t('search_list_error')}</div>
          )}
        </div>
      )}
    </div>
  );
};

export default MapSearch;
