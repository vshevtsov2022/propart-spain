'use client';

import { useState } from 'react';
import {
  handlePriceChange,
  handleInputChange,
  clearValue,
  formatNumber,
} from '@/utils/filterMapData';
import './PriceChecker.scss';
import { PriceType } from '@/store/filterStore';

interface PriceCheckerProps {
  data: {
    value: number;
    label: string;
  }[];
  title: string;
  type: string;
  selectValue: PriceType;
  action: (values: PriceType) => void;
  isSize?: boolean;
}

const PriceChecker = ({ data, title, type, selectValue, action, isSize }: PriceCheckerProps) => {
  const [showBlock, setShowBlock] = useState<boolean>(false);
  //оновлення значень
  const handleInputWithFormat = (e: React.ChangeEvent<HTMLInputElement>, type: 'min' | 'max') => {
    // Видаляємо коми з рядка та перетворюємо його на число
    const rawValue = e.target.value.replace(/,/g, '');
    const value = parseInt(rawValue, 8);

    // Якщо значення валідне і довжина рядка не перевищує 10 символів
    if (rawValue.length <= 8) {
      handleInputChange(e.target.value, type, selectValue, action);
    }
  };
  return (
    <div
      onMouseEnter={() => setShowBlock(true)}
      onMouseLeave={() => setShowBlock(false)}
      className={`price-checker ${showBlock ? 'active' : ''}`}
    >
      {!showBlock ? (
        <div className="trigger">
          <div>
            {title}
            <svg
              width="16"
              height="16"
              viewBox="0 0 19 18"
              fill="none"
              xmlns="http://www.w3.org/2000/svg"
            >
              <path
                d="M9.86764 11.8764L14.6899 6.9035C14.9908 6.5932 14.8084 6 14.4121 6H4.76757C4.37126 6 4.18886 6.5932 4.48976 6.9035L9.31203 11.8764C9.47189 12.0412 9.70779 12.0412 9.86764 11.8764Z"
                fill="currentColor"
              />
            </svg>
          </div>

          {isSize ? (
            <span className="activeVisible">
              {selectValue.min} {selectValue.min ? 'm²' : ''}{' '}
              {selectValue.min && selectValue.max ? '-' : ''} {selectValue.max}{' '}
              {selectValue.max ? 'm²' : ''}
            </span>
          ) : (
            <span className="activeVisible">
              {selectValue?.min && '€'}
              {selectValue?.min &&
                String(selectValue?.min).replace(/\B(?=(\d{3})+(?!\d))/g, ',')}{' '}
              {selectValue.min && selectValue.max ? '-' : ''} {selectValue?.max && '€'}
              {selectValue?.max && String(selectValue?.max).replace(/\B(?=(\d{3})+(?!\d))/g, ',')}
            </span>
          )}
          <span className={`${selectValue.min || selectValue.max ? 'activeVisible' : ''}`}></span>
        </div>
      ) : (
        <div className="trigger-active">
          <div className="price-block">
            <span> {type === 'Price' ? '€' : 'sq.m'}</span>
            <input
              type="text"
              placeholder="EUR"
              value={selectValue.min !== null ? formatNumber(selectValue.min) : 0}
              onChange={e => handleInputWithFormat(e, 'min')}
            />
          </div>
          <span>to</span>
          <div className="price-block">
            <span> {type === 'Price' ? '€' : 'sq.m'}</span>
            <input
              type="text"
              placeholder=""
              value={selectValue.max !== null ? formatNumber(selectValue.max) : 0}
              onChange={e => handleInputWithFormat(e, 'max')}
            />
          </div>
        </div>
      )}
      {showBlock && (
        <div className="price-checker-list-container">
          <ul className="price-checker-ul">
            {data &&
              data.map(item => (
                <li
                  onClick={() => {
                    if (Object.values(selectValue).some(value => value === item.value)) {
                      clearValue(item.value, selectValue, action);
                    } else {
                      handlePriceChange(item.value, selectValue, action);
                    }
                  }}
                  className={`price-checker-list-item ${
                    Object.values(selectValue).some(value => value === item.value) ? 'active' : ''
                  } ${selectValue.min && item.value < selectValue.min ? 'lower' : ''}`}
                  key={item.label}
                >
                  {item.label}
                  {type === 'Price' ? ' EUR' : ' sq.m'}
                  {Object.values(selectValue).some(value => value === item.value) ? (
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      width="12"
                      height="12"
                      viewBox="0 0 12 12"
                      fill="none"
                    >
                      <rect x="0.5" y="0.5" width="11" height="11" rx="1.5" fill="#DBA77B"></rect>
                      <rect x="0.5" y="0.5" width="11" height="11" rx="1.5" stroke="#DBA77B"></rect>
                      <rect x="4" y="4" width="4" height="4" rx="2" fill="white"></rect>
                    </svg>
                  ) : (
                    <span className="check-item"></span>
                  )}
                </li>
              ))}
          </ul>
        </div>
      )}
    </div>
  );
};

export default PriceChecker;
