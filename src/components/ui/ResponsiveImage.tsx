'use client';
import Image from 'next/image';
import './ResponsiveImage.scss';

export interface ResponsiveImageProps {
  src: string;
  alt: string;
  width: number;
  height: number;
  className?: string;
}

const ResponsiveImage = ({ src, alt, width, height, className }: ResponsiveImageProps) => {
  return (
    <div
      className={className}
      style={{
        position: 'relative',
        width: '100%',
        height: '100%',
        paddingTop: `${(height / width) * 100}%`,
      }}
    >
      <img
        src={src}
        alt={alt}
        style={{
          position: 'absolute',
          top: '50%',
          left: '50%',
          width: '100%',
          height: '100%',
          transform: 'translate(-50%, -50%)', 
          objectFit: 'cover',
        }}
      />
    </div>
  );
};

export default ResponsiveImage;
