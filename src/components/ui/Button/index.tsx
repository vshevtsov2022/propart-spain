import './button.scss';

export default function Button({ children, handler, className }: any) {
  return (
    <button onClick={handler} className={`button ${className}`}>
      {children}
    </button>
  );
}
