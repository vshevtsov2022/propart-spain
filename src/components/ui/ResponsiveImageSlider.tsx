import { ResponsiveImageProps } from "./ResponsiveImage";

const ResponsiveImageSlider = ({ src, alt, width, height, className }: ResponsiveImageProps) => {
  return (
    <div
      className={className}
      style={{
        position: 'relative',
        width: '100%',
        height: '100%',
      }}
    >
      <div
        style={{
          backgroundImage: `url(${src})`,
          backgroundRepeat: 'no-repeat',
          backgroundSize: 'cover',
          backgroundPosition: 'center',
          width: '100%',
          height: '100%',
        }}
      ></div>
    </div>
  );
};

export default ResponsiveImageSlider;
