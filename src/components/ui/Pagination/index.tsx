import React, { useState, useEffect } from 'react';
import Button from '../Button';
import './pagination.scss';

export default function Pagination({ children, pageNumber, toTalPages, handeChangePage }: any) {
  const [isWideScreen, setIsWideScreen] = useState(false);

  useEffect(() => {
    const handleResize = () => {
      setIsWideScreen(window.innerWidth > 768);
    };

    handleResize();

    window.addEventListener('resize', handleResize);

    return () => {
      window.removeEventListener('resize', handleResize);
    };
  }, []);

  const renderPaginationItems = () => {
    const items = [];
    const visiblePages = 3;
    const halfVisible = Math.floor(visiblePages / 2);
    let startPage = pageNumber - halfVisible + 1;
    let endPage = pageNumber + halfVisible + 1;

    if (startPage <= 1) {
      startPage = 1;
      endPage = visiblePages;
    }

    if (endPage > toTalPages) {
      endPage = toTalPages;
      startPage = toTalPages - visiblePages + 1;
      if (startPage < 1) {
        startPage = 1;
      }
    }

    for (let i = startPage; i <= endPage; i++) {
      items.push(
        <li className="pagination__btn" key={i}>
          <Button
            className={pageNumber === i - 1 ? 'button--purple' : 'button--gradientBorder'}
            handler={() => handeChangePage(i - 1)}
          >
            {i}
          </Button>
        </li>
      );
    }

    if (isWideScreen) {
      if (startPage > 1) {
        items.unshift(
          <li className="pagination__btn" key="start-1">
            <Button className="button--gradientBorder" handler={() => handeChangePage(0)}>
              1
            </Button>
          </li>
        );
        if (startPage > 2) {
          items.splice(
            1,
            0,
            <li className="pagination__btn" key="start-dots">
              <Button className="button">...</Button>
            </li>
          );
        }
      }
      if (endPage < toTalPages) {
        items.push(
          <li className="pagination__btn" key="end-1">
            <Button
              className="button--gradientBorder"
              handler={() => handeChangePage(toTalPages - 1)}
            >
              {toTalPages}
            </Button>
          </li>
        );
        if (endPage < toTalPages - 1) {
          items.splice(
            items.length - 1,
            0,
            <li className="pagination__btn" key="end-dots">
              <button className="button">...</button>
            </li>
          );
        }
      }
    }

    return items;
  };

  useEffect(() => {}, [toTalPages, pageNumber]);

  return (
    <div className="pagination__box">
      <div className="pagination__data">{children}</div>
      <ul className="pagination">
        <li className="pagination__arrow">
          <Button
            className="button--gradientBorder"
            handler={() => (pageNumber > 0 ? handeChangePage(pageNumber - 1) : '')}
          >
            <svg
              width="24"
              height="24"
              viewBox="0 0 24 24"
              fill="none"
              xmlns="http://www.w3.org/2000/svg"
            >
              <desc>Created with Pixso.</desc>
              <defs>
                <clipPath id="clip328_20112">
                  <rect
                    id="Bold Duotone / Arrows / Arrow Left"
                    width="24"
                    height="24"
                    transform="matrix(1 1.22465e-16 1.22465e-16 -1 0 24)"
                    fill="white"
                    fillOpacity="0"
                  />
                </clipPath>
              </defs>
              <g clipPath="url(#clip328_20112)">
                <g opacity="0.5">
                  <path
                    id="Union"
                    d="M20.75 12C20.75 12.4143 20.4141 12.75 20 12.75L10.75 12.75L10.75 11.25L20 11.25C20.4141 11.25 20.75 11.5857 20.75 12Z"
                    fill="#dba77b"
                    fillOpacity="1"
                    fillRule="evenodd"
                  />
                </g>
                <path
                  id="Union"
                  d="M10.75 6C10.75 5.69653 10.5674 5.4231 10.2871 5.30713C10.0068 5.19092 9.68408 5.25513 9.46973 5.46973L3.46973 11.4697C3.3291 11.6104 3.25 11.801 3.25 12C3.25 12.199 3.3291 12.3896 3.46973 12.5303L9.46973 18.5303C9.68408 18.7449 10.0068 18.8091 10.2871 18.6929C10.5674 18.5769 10.75 18.3032 10.75 18L10.75 6Z"
                  fill="#dba77b"
                  fillOpacity="1"
                  fillRule="nonzero"
                />
              </g>
            </svg>
          </Button>
        </li>
        {renderPaginationItems()}
        <li className="pagination__arrow">
          <Button
            className="button--gradientBorder"
            handler={() => (pageNumber < toTalPages - 1 ? handeChangePage(pageNumber + 1) : '')}
          >
            <svg
              width="24"
              height="24"
              viewBox="0 0 24 24"
              fill="none"
              xmlns="http://www.w3.org/2000/svg"
              style={{ transform: 'rotate(180deg)' }}
            >
              <desc>Created with Pixso.</desc>
              <defs>
                <clipPath id="clip328_20112">
                  <rect
                    id="Bold Duotone / Arrows / Arrow Left"
                    width="24"
                    height="24"
                    transform="matrix(1 1.22465e-16 1.22465e-16 -1 0 24)"
                    fill="white"
                    fillOpacity="0"
                  />
                </clipPath>
              </defs>
              <g clipPath="url(#clip328_20112)">
                <g opacity="0.5">
                  <path
                    id="Union"
                    d="M20.75 12C20.75 12.4143 20.4141 12.75 20 12.75L10.75 12.75L10.75 11.25L20 11.25C20.4141 11.25 20.75 11.5857 20.75 12Z"
                    fill="#dba77b"
                    fillOpacity="1"
                    fillRule="evenodd"
                  />
                </g>
                <path
                  id="Union"
                  d="M10.75 6C10.75 5.69653 10.5674 5.4231 10.2871 5.30713C10.0068 5.19092 9.68408 5.25513 9.46973 5.46973L3.46973 11.4697C3.3291 11.6104 3.25 11.801 3.25 12C3.25 12.199 3.3291 12.3896 3.46973 12.5303L9.46973 18.5303C9.68408 18.7449 10.0068 18.8091 10.2871 18.6929C10.5674 18.5769 10.75 18.3032 10.75 18L10.75 6Z"
                  fill="#dba77b"
                  fillOpacity="1"
                  fillRule="nonzero"
                />
              </g>
            </svg>
          </Button>
        </li>
      </ul>
    </div>
  );
}
