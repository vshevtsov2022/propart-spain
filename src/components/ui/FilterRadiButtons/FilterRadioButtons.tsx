'use client';
import './FilterRadioButtons.scss';

interface FilterRadioButtonsProps {
  radio1: string;
  radio2: string;
  selectedType: string;
  action: (value: string) => void;
}

const FilterRadioButtons = ({ selectedType, action, radio1, radio2 }: FilterRadioButtonsProps) => {
  return (
    <div className="filter-radio-buttons">
      <button
        className={`radio-button ${selectedType === 'newBuilding' ? 'active' : ''}`}
        onClick={event => {
          event.preventDefault();
          action('newBuilding');
        }}
      >
        {radio1}
      </button>

      <button
        className={`radio-button ${selectedType === 'specialOffer' ? 'active' : ''}`}
        onClick={event => {
          event.preventDefault();
          action('specialOffer');
        }}
      >
        {radio2}
      </button>
    </div>
  );
};
export default FilterRadioButtons;
