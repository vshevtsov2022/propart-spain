'use client';
import { useState, useRef } from 'react';
import { useClickOutside } from '@/utils/useClickOutside';
import './DropdownCheckerMob.scss';

interface PriceCheckerProps {
  data: {
    value: string;
    label: string;
  }[];
  title: string;
  text: string;
  selectValue: string[];
  action: (values: string[]) => void;
}

const DropdownCheckerMob = ({ data, title, text, selectValue, action }: PriceCheckerProps) => {
  const [showBlock, setShowBlock] = useState<boolean>(false);
  const blockRef = useRef(null);
  useClickOutside(showBlock, blockRef, () => setShowBlock(false));
  const handleCheckClick = (value: string) => {
    let updatedValues;
    if (selectValue.includes(value)) {
      // Видалити якщо є у selectedAreas
      updatedValues = selectValue.filter(selected => selected !== value);
    } else {
      // Додати якщо нема у selectedAreas
      updatedValues = [...selectValue, value];
    }
    action(updatedValues);
  };

  // Створення масиву обраних елементів для виведення
  const selectedLabels = data
    .filter(item => selectValue.includes(item.value))
    .map(item => item.label)
    .join(', ');

  return (
    <div ref={blockRef} className="checker-mob">
      <button
        className={`trigger ${
          (selectValue && selectValue.length > 0) || showBlock ? 'active' : ''
        }`}
        onClick={event => {
          event.preventDefault();
          setShowBlock(!showBlock);
        }}
      >
        {title}

        <span className='text'>
          {text}
          <span className={`trigger-icon ${showBlock ? 'active' : ''}`}>
            <svg
              width="16"
              height="16"
              viewBox="0 0 19 18"
              fill="none"
              xmlns="http://www.w3.org/2000/svg"
            >
              <path
                d="M9.86764 11.8764L14.6899 6.9035C14.9908 6.5932 14.8084 6 14.4121 6H4.76757C4.37126 6 4.18886 6.5932 4.48976 6.9035L9.31203 11.8764C9.47189 12.0412 9.70779 12.0412 9.86764 11.8764Z"
                fill="currentColor"
              />
            </svg>
          </span>
        </span>
      </button>
      {selectValue && selectValue.length > 0 && !showBlock && (
        <div className="selectedList">{selectedLabels}</div>
      )}

      <div className={`checker-list-container ${showBlock ? 'active' : ''}`}>
        <ul className="checker-ul">
          {data &&
            data.map(item => (
              <li
                onClick={() => handleCheckClick(item.value)}
                className={`checker-list-item ${selectValue.includes(item.value) ? 'active' : ''}`}
                key={item.label}
              >
                {item.label}
                <span className="checker-icon"></span>
              </li>
            ))}
        </ul>
      </div>
    </div>
  );
};

export default DropdownCheckerMob;
