import React from 'react';
import AreaRecomendationCard from '../../AreaRecomendationCard';
import DragableSlider from '../../DragableSlider';
import './AreaRecomendation.scss';
import initTranslations from '@/app/i18n';

type AreaRecomendationProps = {
  locale: string;
  data: {
    title: string;
    subTitle: string;
    photo: string;
  }[];
};

const AreaRecomendation = async ({ data, locale }: AreaRecomendationProps) => {
  const { t } = await initTranslations(locale, ['area-page']);
  return (
    <div className="area__recomendation">
      <h2 className="section-title">{t('common.recomendations')}</h2>
      <div className="card__wrapper">
        {data.map((item, idx) => (
          <AreaRecomendationCard hoursVisible={true} card={item} key={idx} />
        ))}
      </div>
      <div className="card__slider">
        <DragableSlider
          cardsArray={data.map((item, idx) => (
            <AreaRecomendationCard hoursVisible={true} card={item} key={idx} />
          ))}
        />
      </div>
    </div>
  );
};

export default AreaRecomendation;
