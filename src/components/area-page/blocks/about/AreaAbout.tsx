import ResponsiveImage from '@/components/ui/ResponsiveImage';
import React from 'react';
import './AreaAbout.scss';
import initTranslations from '@/app/i18n';

type AreaAboutProps = {
  locale: string;
  areaUrl: string;
  areaName: string;
  aboutLocation: {
    gallery: string[];
    paragraphs: string[];
  };
};

const AreaAbout = async ({ aboutLocation, areaName, areaUrl, locale }: AreaAboutProps) => {
  const { t } = await initTranslations(locale, ['area-page']);
  return (
    <div className="area__about">
      <h2 className="title">
        {t('common.about-title')} {areaName.charAt(0).toUpperCase() + areaName.slice(1).toLowerCase()}
      </h2>
      <div className="area__preview">
        {aboutLocation.gallery.map((image, idx) => (
          <ResponsiveImage
            alt={'About' + areaName}
            className="image"
            key={idx}
            width={885}
            height={190}
            src={image}
          />
        ))}
      </div>
      <div className="area__about-text">
        {aboutLocation.paragraphs.map((p, idx) => (
          <p key={idx} className="text">
            {p}
          </p>
        ))}
      </div>
    </div>
  );
};

export default AreaAbout;
