'use client';

import MainMapMap from '@/components/mainPage/MainMap8/MainMapMap/MainMapMap';
import React, { useEffect, useState } from 'react';
import './AreaMap.scss';
import { useTranslation } from 'react-i18next';
import { MapDataProps } from '@/components/mainPage/MainMap8/MainMap';
import { allAreasList } from '@/data/allAreasList';
import { getProjectByMap } from '@/api/properties';

type AreaMapProps = {
  geographicalInfo: string[];
  lng: string;
  lat: string;
  locale: string;
};

const AreaMap = ({ geographicalInfo, lat, lng }: AreaMapProps) => {
  const [mapLoad, setLoad] = useState(true);
  const { t } = useTranslation('area-page');

  const [isLoading, setIsLoading] = useState<boolean>(true);

  const [mapData, setMapData] = useState<MapDataProps[]>([]);

  useEffect(() => {
    const fetchData = async () => {
      try {
        setIsLoading(true);

        const data = {
          bedrooms: ['Studio', '1', '2', '3', '4', '5', '6', '7'],
          completion: ['Ready', 'Off plan', 'Special Offer'],
          location: '',
          type: ['Apartment', 'Townhouse', 'Villa', 'Duplex', 'Flat', 'Penthouse', 'Ground floor'],
          priceFrom: 0,
          priceTo: 999999999,
          areas: allAreasList,
          sizeFrom: 0,
          sizeTo: 99999999999,
          namePrefix: '',
        };
        const response = await getProjectByMap(data);

        const convertedData = response.map((project: any) => ({
          nameId: project._id,
          projectId: project._id,
          picture:
            project.pictures && project.pictures.length > 0
              ? project.pictures[0]
              : '/path/to/default/image.png',
          name: project.name,
          price_from: project.price_from,
          coordinates:
            project.coordinates && project.coordinates.lng && project.coordinates.lat
              ? [parseFloat(project.coordinates.lng), parseFloat(project.coordinates.lat)]
              : [-4.919074478458782, 36.511572005181534],
        }));

        setMapData(convertedData);
      } catch (error) {
        console.error('Failed to fetch data', error);
        setMapData([]);
      } finally {
        setIsLoading(false);
      }
    };

    fetchData();
  }, []);

  return (
    <div className="area__map-block">
      <div className="area__location-info">
        <h2 className="section-title">{t('common.map-title')}</h2>
        {geographicalInfo.map((p, idx) => (
          <p key={idx} className="loaction-text">
            {p}
          </p>
        ))}
      </div>
      {/* <div className="area__map">
        {mapLoad && (
          <div className="map-loader">
            <div className="loader"></div>
          </div>
        )}

        {isLoading ? (
          <></>
        ) : (
          <MainMapMap
            mapData={mapData}
            mapCoordinates={[parseFloat(lng), parseFloat(lat)]}
            mapZoom={11}
            setLoad={setLoad}
          />
        )}
      </div> */}
    </div>
  );
};

export default AreaMap;
