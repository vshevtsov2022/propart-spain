'use client';
import React, { useEffect, useState } from 'react';
import Link from 'next/link';
import './OffersList.scss';
import Image from 'next/image';
import OfferCard from '../OfferCard';
import { useTranslation } from 'react-i18next';
import CardSkeleton from '../OfferCard/CardSkeleton';
import { allAreasList } from '@/data/allAreasList';
import { getProjectByMap } from '@/api/properties';
import Pagination from '@/components/ui/Pagination';
import useFilterStore from '@/store/filterStore';

interface OffersListProps {
  offersData: any;
  hideButtons: boolean;
  showMessage?: boolean;
  isLoading?: boolean;
  visibleProjects?: number;
  allProjectsLength?: number;
}

interface SecondaryOffersListProps {
  offersData: any;
  isLoading?: boolean;
  visibleProjects?: number;
  allProjectsLength?: number;
}

export const OffersList = ({
  offersData,
  hideButtons,
  showMessage,
  isLoading,
  visibleProjects,
  allProjectsLength,
}: OffersListProps) => {
  const { t } = useTranslation('properties');
  const { page, setPage, polygonsProjects } = useFilterStore();
  const [visiblePolygonsProjects, setVisiblePolygonsProjects] = useState(24);

  const handleShowMoreClick = () => {
    const remainingProjects = polygonsProjects.length - visiblePolygonsProjects;
    const projectsToAdd = remainingProjects > 8 ? 8 : remainingProjects;
    setVisiblePolygonsProjects(
      Math.min(visiblePolygonsProjects + projectsToAdd, polygonsProjects.length)
    );
  };

  return (
    <>
      {showMessage && <span className="projects__offers-noSearch">{t('noSearch')}</span>}
      {polygonsProjects.length <= 0 ? (
        <div className="offers-list-container">
          <div className="projects__offers-list">
            {offersData &&
              Array.isArray(offersData) &&
              offersData.length > 0 &&
              offersData.slice(0, visibleProjects).map((item: any, i) => {
                return <OfferCard properties={item} key={item.projectId + i} />;
              })}
            {isLoading && (
              <>
                <CardSkeleton />
                <CardSkeleton />
                <CardSkeleton />
                <CardSkeleton />
                <CardSkeleton />
                <CardSkeleton />
              </>
            )}
          </div>
          <Pagination pageNumber={page} handeChangePage={setPage} toTalPages={allProjectsLength} />
          {!hideButtons && (
            <div className="projects__btn-group">
              <Link href={'/projects-map'} className="map__btn">
                <div className="circle-mask" />
                <Image src="/assets/icons/earth.svg" alt="Map Icon" width={24} height={24} />
                {t('offers-list.map-btn')}
              </Link>
            </div>
          )}
        </div>
      ) : (
        <div className="offers-list-container">
          <div className="projects__offers-list">
            {polygonsProjects &&
              Array.isArray(polygonsProjects) &&
              polygonsProjects.length > 0 &&
              polygonsProjects.slice(0, visiblePolygonsProjects).map((item: any, i) => {
                return <OfferCard properties={item} key={item.projectId + i} />;
              })}
            {isLoading && (
              <>
                <CardSkeleton />
                <CardSkeleton />
                <CardSkeleton />
                <CardSkeleton />
                <CardSkeleton />
                <CardSkeleton />
              </>
            )}
          </div>
          {!hideButtons && (
            <div className="projects__btn-group">
              <Link href={'/projects-map'} className="map__btn">
                <div className="circle-mask" />
                <Image src="/assets/icons/earth.svg" alt="Map Icon" width={24} height={24} />
                {t('offers-list.map-btn')}
              </Link>
              {polygonsProjects.length >= 24 &&
                visiblePolygonsProjects !== polygonsProjects.length && (
                  <button onClick={handleShowMoreClick} className="show__btn">
                    {t('offers-list.more-btn')}
                  </button>
                )}
            </div>
          )}
        </div>
      )}
    </>
  );
};

export const SecondaryOffersList = ({
  offersData,
  isLoading,
  visibleProjects,
  allProjectsLength,
}: SecondaryOffersListProps) => {
  const { pageSecondary, setPageSecondary } = useFilterStore();

  return (
    <div className="offers-list-container">
      <div className="projects__offers-list">
        {offersData &&
          Array.isArray(offersData) &&
          offersData.length > 0 &&
          offersData.slice(0, visibleProjects).map((item: any, i) => {
            return <OfferCard properties={item} key={item.projectId + i} secondary={true} />;
          })}
        {isLoading && (
          <>
            <CardSkeleton />
            <CardSkeleton />
            <CardSkeleton />
            <CardSkeleton />
            <CardSkeleton />
            <CardSkeleton />
          </>
        )}
      </div>
      <Pagination
        pageNumber={pageSecondary}
        handeChangePage={setPageSecondary}
        toTalPages={allProjectsLength}
      />
    </div>
  );
};

export const OffersListProjectsAreas = ({
  offersData,
  hideButtons,
  showMessage,
  isLoading,
  visibleProjects,
  allProjectsLength,
}: OffersListProps) => {
  const { t } = useTranslation('properties');
  const { page, setPage, polygonsProjects } = useFilterStore();
  const [visiblePolygonsProjects, setVisiblePolygonsProjects] = useState(24);

  const handleShowMoreClick = () => {
    const remainingProjects = polygonsProjects.length - visiblePolygonsProjects;
    const projectsToAdd = remainingProjects > 8 ? 8 : remainingProjects;
    setVisiblePolygonsProjects(
      Math.min(visiblePolygonsProjects + projectsToAdd, polygonsProjects.length)
    );
  };

  return (
    <div className="offers-list-container">
      <div className="projects__offers-list">
        {offersData &&
          Array.isArray(offersData) &&
          offersData.length > 0 &&
          offersData.slice(0, visibleProjects).map((item: any, i) => {
            return <OfferCard properties={item} key={item.projectId + i} />;
          })}
        {isLoading && (
          <>
            <CardSkeleton />
            <CardSkeleton />
            <CardSkeleton />
            <CardSkeleton />
            <CardSkeleton />
            <CardSkeleton />
          </>
        )}
      </div>
    </div>
  );
};
