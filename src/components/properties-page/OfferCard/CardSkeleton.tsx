const CardSkeleton = () => {
  return (
    <svg
      aria-labelledby="zjexxqb-aria"
      role="img"
      height="100%"
      width="100%"
      viewBox="0 0 400 440"
      style={{ width: '100%', height: '100%' }}
    >
      <title id="zjexxqb-aria">Loading...</title>
      <rect
        role="presentation"
        x="0"
        y="0"
        width="100%"
        height="100%"
        clipPath="url(#zjexxqb-diff)"
        style={{ fill: 'url(#zjexxqb-animated-diff)' }}
      ></rect>
      <defs>
        <clipPath id="zjexxqb-diff">
          <rect x="0" y="0" rx="8" ry="8" width="400" height="340"></rect>
          <rect x="0" y="349" rx="3" ry="3" width="197" height="13"></rect>
          <rect x="0" y="370" rx="3" ry="3" width="197" height="18"></rect>
          <rect x="300" y="370" rx="3" ry="3" width="100" height="18"></rect>
        </clipPath>
        <linearGradient id="zjexxqb-animated-diff" gradientTransform="translate(-2 0)">
          <stop offset="0%" stopColor="#e8e8e8" stopOpacity="1"></stop>
          <stop offset="50%" stopColor="#ecebeb" stopOpacity="1"></stop>
          <stop offset="100%" stopColor="#e8e8e8" stopOpacity="1"></stop>
          <animateTransform
            attributeName="gradientTransform"
            type="translate"
            values="-2 0; 0 0; 2 0"
            dur="2s"
            repeatCount="indefinite"
          ></animateTransform>
        </linearGradient>
      </defs>
    </svg>
  );
};

export default CardSkeleton;
