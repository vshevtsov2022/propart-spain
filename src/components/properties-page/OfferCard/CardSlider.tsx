'use client';

import ResponsiveImage from '@/components/ui/ResponsiveImage';
import React, { useRef } from 'react';
import { Navigation } from 'swiper/modules';
import { Swiper, SwiperSlide } from 'swiper/react';
import Image from 'next/image';

import 'swiper/css';
import 'swiper/css/navigation';
import ResponsiveImageSlider from '@/components/ui/ResponsiveImageSlider';

interface CardSliderProps {
  swiperActive: boolean;
  pictures: string[];
  name: string;
  isAnother?: boolean;
}

const CardSlider = ({ swiperActive, pictures, name, isAnother }: CardSliderProps) => {
  const sliderRef = useRef<any>(null);
  const validPictures = Array.isArray(pictures) ? pictures : [];

  const allValidUrls = validPictures.every(
    picture =>
      typeof picture === 'string' &&
      (picture.startsWith('http://') || picture.startsWith('https://'))
  );

  const imagesToDisplay = allValidUrls ? validPictures : ['/images/areas/marbella.jpg'];

  return (
    <>
      <ResponsiveImage
        className="card-image"
        src={imagesToDisplay[0]}
        alt={name ? name : 'properties photo'}
        width={588}
        height={388}
      />
    </>
  );
};

export default CardSlider;

const arrowR = (
  <svg xmlns="http://www.w3.org/2000/svg" width="23" height="23" viewBox="0 0 23 23" fill="none">
    <rect width="23" height="23" rx="11.5" transform="matrix(-1 0 0 1 23 0)" fill="white"></rect>
    <path
      d="M10 7L14.1464 11.1464C14.3417 11.3417 14.3417 11.6583 14.1464 11.8536L10 16"
      stroke="black"
      strokeLinecap="round"
    ></path>
  </svg>
);

const arrowL = (
  <svg xmlns="http://www.w3.org/2000/svg" width="23" height="23" viewBox="0 0 23 23" fill="none">
    <rect width="23" height="23" rx="11.5" transform="matrix(-1 0 0 1 23 0)" fill="white"></rect>
    <path
      d="M13 7L8.85355 11.1464C8.65829 11.3417 8.65829 11.6583 8.85355 11.8536L13 16"
      stroke="black"
      strokeLinecap="round"
    ></path>
  </svg>
);
