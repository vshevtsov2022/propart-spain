'use client';
import { useState, useEffect } from 'react';
import Image from 'next/image';
import FilterRadioButtons from '@/components/ui/FilterRadiButtons/FilterRadioButtons';
import MapSearch from '@/components/ui/MapSearch/MapSearch';
import { useMediaQuery } from 'react-responsive';
import { useTranslation } from 'react-i18next';
import useFilterStore from '@/store/filterStore';
import useSortStore from '@/store/sortStore';
import { FilterState } from '@/store/filterStore';
import './FilterBlockProperties.scss';
import { PriceType } from '@/store/filterStore';
import PriceChecker from '@/components/ui/PriceChecker/PriceChecker';
import DropdownChecker from '@/components/ui/DropdownChecker/DropdownChecker';
//для запиту отримання списку проектів
import {
  getTypeOptions,
  getBedroomOptions,
  priceOptions,
  sizeOptions,
  areasOptions,
  areasOptionsSubareas,
} from '@/constants/filters';
import PopupErrorPolygons from '@/components/PopupErrorPolygons';
import DropdownAreas from '@/components/ui/DropdownChecker/DropdownAreas';

export const FilterBlockProperties = () => {
  const [isClient, setIsClient] = useState<boolean>(false);
  const isMobile = useMediaQuery({ maxWidth: 767 });
  useEffect(() => {
    setIsClient(true);
  }, []);
  const { t } = useTranslation('map-filters');
  const typeOptions = getTypeOptions(t);
  const bedroomOptions = getBedroomOptions(t);

  const {
    projects, //список проектів
    selectedRadio,
    selectedType,
    selectedBeds,
    selectedPrice,
    selectedSize,
    selectedArea,
    reset,
    setMapObject,
    setFilter,
    toggleReset,
    clearFilters,
    setProjects,
    setTempArea,
    setPage,
    polygonsProjects,
  } = useFilterStore();
  const { clearSort } = useSortStore();
  const [popupShow, setPopupShow] = useState(false);
  const handleFilterChange = (field: keyof FilterState, value: any) => {
    setFilter(field, value);
  };

  const handleClearFilter = () => {
    clearFilters();
    clearSort();
    toggleReset();
  };

  const handleClickPolygons = (e: React.MouseEvent) => {
    if (polygonsProjects.length > 0) {
      setPopupShow(true);
      // e.preventDefault();
      e.stopPropagation();
    }
  };

  return (
    isClient &&
    (isMobile ? (
      <div className="filter-block-properties-mob-search container">
        <MapSearch
          handleSearch={value => handleFilterChange('searchParams', value)}
          handleNameChange={setMapObject}
          projectsNameData={projects}
          reset={reset}
        />
      </div>
    ) : (
      <div className="filter-block-properties">
        <div className="filter-block-properties-filters container">
          <div className="row1" onClick={handleClickPolygons}>
            <div
              className="radio"
              style={polygonsProjects.length > 0 ? { pointerEvents: 'none' } : {}}
            >
              <FilterRadioButtons
                radio1={t('hero.filter-radio1')}
                radio2={t('hero.filter-radio2')}
                selectedType={selectedRadio}
                action={(value: string) => handleFilterChange('selectedRadio', value)}
              />
            </div>
            <div
              className="search"
              style={polygonsProjects.length > 0 ? { pointerEvents: 'none' } : {}}
            >
              <MapSearch
                handleSearch={value => handleFilterChange('searchParams', value)}
                handleNameChange={setMapObject}
                projectsNameData={projects}
                reset={reset}
              />
            </div>
            <button
              onClick={(e: any) => {
                handleClearFilter();
                setPage(0);
                e.stopPropagation();
              }}
              className="clear"
            >
              <Image src="/assets/icons/clear.svg" alt="Clear Icon" width={20} height={20} />
              {t('clean')}
            </button>
          </div>
          <div className="row2" onClick={handleClickPolygons}>
            <div
              className="dropdown"
              style={polygonsProjects.length > 0 ? { pointerEvents: 'none' } : {}}
            >
              <DropdownChecker
                data={typeOptions}
                title={t('type_title')}
                selectValue={selectedType}
                action={(values: string[]) => handleFilterChange('selectedType', values)}
                isVisibleText={true}
              />
            </div>

            <div
              className="dropdown"
              style={polygonsProjects.length > 0 ? { pointerEvents: 'none' } : {}}
            >
              <DropdownChecker
                data={bedroomOptions}
                title={t('beds_title')}
                selectValue={selectedBeds}
                action={(values: string[]) => handleFilterChange('selectedBeds', values)}
                isVisibleText={true}
              />
            </div>

            <div
              className="dropdown"
              style={polygonsProjects.length > 0 ? { pointerEvents: 'none' } : {}}
            >
              <PriceChecker
                data={priceOptions}
                title={t('price_title')}
                type="Price"
                selectValue={selectedPrice}
                action={(values: PriceType) => handleFilterChange('selectedPrice', values)}
              />
            </div>

            <div
              className="dropdown"
              style={polygonsProjects.length > 0 ? { pointerEvents: 'none' } : {}}
            >
              <PriceChecker
                data={sizeOptions}
                title={t('size_title')}
                type="Size"
                selectValue={selectedSize}
                action={(values: PriceType) => handleFilterChange('selectedSize', values)}
                isSize={true}
              />
            </div>

            <div
              className="dropdown"
              style={polygonsProjects.length > 0 ? { pointerEvents: 'none' } : {}}
            >
              <DropdownAreas
                data={areasOptionsSubareas}
                title={t('area_title')}
                selectValue={selectedArea}
                action={(values: string[]) => {
                  handleFilterChange('selectedArea', values);
                  setTempArea(values);
                }}
                isVisibleText={true}
              />
            </div>
          </div>
          <PopupErrorPolygons handler={setPopupShow} isVisible={popupShow} />
        </div>
      </div>
    ))
  );
};

export const SmallFilterBlockProperties = () => {
  const [isClient, setIsClient] = useState<boolean>(false);
  const isMobile = useMediaQuery({ maxWidth: 767 });
  useEffect(() => {
    setIsClient(true);
  }, []);
  const { t } = useTranslation('map-filters');
  const typeOptions = getTypeOptions(t);
  const bedroomOptions = getBedroomOptions(t);

  const {
    projects, //список проектів
    reset,
    setMapObject,
    setFilter,
    toggleReset,
    clearFilters,
    setPageSecondary,
    polygonsProjects,
    searchSecondary,
  } = useFilterStore();
  const { clearSort } = useSortStore();
  const [popupShow, setPopupShow] = useState(false);
  const handleFilterChange = (field: keyof FilterState, value: any) => {
    setFilter(field, value);
  };

  const handleClearFilter = () => {
    clearFilters();
    clearSort();
    toggleReset();
  };

  const handleClickPolygons = (e: React.MouseEvent) => {
    if (polygonsProjects.length > 0) {
      setPopupShow(true);
      // e.preventDefault();
      e.stopPropagation();
    }
  };

  return (
    isClient &&
    (isMobile ? (
      <div className="filter-block-properties-mob-search container">
        <MapSearch
          handleSearch={value => handleFilterChange('searchSecondary', value)}
          handleNameChange={setMapObject}
          projectsNameData={projects}
          reset={reset}
        />
      </div>
    ) : (
      <div className="filter-block-properties">
        <div className="filter-block-properties-filters container">
          <div className="row1" onClick={handleClickPolygons}>
            <div
              className="search"
              style={polygonsProjects.length > 0 ? { pointerEvents: 'none' } : {}}
            >
              <MapSearch
                handleSearch={value => handleFilterChange('searchSecondary', value)}
                handleNameChange={setMapObject}
                projectsNameData={projects}
                reset={reset}
              />
            </div>
            <button
              onClick={(e: any) => {
                handleClearFilter();
                setPageSecondary(0);
                e.stopPropagation();
              }}
              className="clear"
            >
              <Image src="/assets/icons/clear.svg" alt="Clear Icon" width={20} height={20} />
              {t('clean')}
            </button>
          </div>
          <PopupErrorPolygons handler={setPopupShow} isVisible={popupShow} />
        </div>
      </div>
    ))
  );
};
