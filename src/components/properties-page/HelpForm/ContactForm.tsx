'use client';
import { useState, useEffect, ChangeEvent, FormEvent } from 'react';
import { postFormData } from '@/api/form';
import './HelpForm.scss';
import Image from 'next/image';
import { useTranslation } from 'react-i18next';

const ContactForm = () => {
  const { t } = useTranslation('properties');

  const [name, setName] = useState<string>('');
  const [phone, setPhone] = useState<string>('');
  const [isFormValid, setIsFormValid] = useState<boolean>(false);
  const [buttonText, setButtonText] = useState<string>(t('form.submit-btn'));

  const validateName = (name: string): boolean => {
    return name.trim().length >= 2;
  };

  const validatePhone = (phone: string): boolean => {
    // Регулярний вираз для валідації міжнародного формату телефону
    const phoneRegex = /^\+?[1-9]\d{8,14}$/;
    return phoneRegex.test(phone);
  };

  useEffect(() => {
    if (validateName(name) && validatePhone(phone)) {
      setIsFormValid(true);
    } else {
      setIsFormValid(false);
    }
  }, [name, phone]);

  const handleNameChange = (event: ChangeEvent<HTMLInputElement>): void => {
    setName(event.target.value);
  };
  const handlePhoneChange = (event: ChangeEvent<HTMLInputElement>): void => {
    const value = event.target.value;
    // Дозволяємо вводити лише цифри після знака "+"
    if (/^\+?\d*$/.test(value)) {
      setPhone(value);
    }
  };
  const handlePhoneFocus = (): void => {
    if (!phone.startsWith('+')) {
      setPhone('+' + phone);
    }
  };
  const handleSubmit = async (event: FormEvent<HTMLFormElement>): Promise<void> => {
    event.preventDefault();
    if (isFormValid) {
      setButtonText('Sending'); // Зміна тексту кнопки на "Відправка форми"
      const formData = { name, phone };
      try {
        await postFormData(formData);
        setButtonText('Success'); // Зміна тексту кнопки на "Форма відправлена"
        setTimeout(() => {
          setButtonText(t('zoom.button')); // Відновлення початкового тексту кнопки через 5 секунд
        }, 5000);
        setName('');
        setPhone('');
      } catch (error) {
        console.error('Error submitting form:', error);
        setButtonText(t('zoom.button')); // Відновлення початкового тексту кнопки у разі помилки
      }
    }
  };

  return (
    <div className="contact">
      <h2 className="contact__title">{t('form.title')}</h2>
      <p className="contact__text">{t('form.text')}</p>
      <form className="contact__form" onSubmit={e => handleSubmit(e)}>
        <input
          type="text"
          name="name"
          id="name"
          value={name}
          placeholder={t('name-placeholder')}
          onChange={handleNameChange}
          onBlur={e => validateName(e.target.value)}
          className={`contact__form__name ${validateName(name) ? 'valid' : 'invalid'}`}
        ></input>
        <input
          name="tel"
          className={`contact__form__phone ${validatePhone(phone) ? 'valid' : 'invalid'}`}
          type="tel"
          placeholder={t('phone-placeholder')}
          value={phone}
          onChange={handlePhoneChange}
          onFocus={handlePhoneFocus}
          onBlur={e => validatePhone(e.target.value)}
        ></input>
        <button className="contact__form__btn" type="submit" disabled={!isFormValid}>
          {buttonText}
          <Image src="/assets/icons/done.svg" alt="Submit form" width={20} height={20} />
        </button>
        <span className="contact__form__privacy">{t('form.privacy')}</span>
      </form>
    </div>
  );
};
export default ContactForm;
