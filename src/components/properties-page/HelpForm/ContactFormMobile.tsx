'use client';
import { useState, useEffect, ChangeEvent, FormEvent } from 'react';
import { postFormData } from '@/api/form';
import './HelpForm.scss';
import Image from 'next/image';
import { useTranslation } from 'react-i18next';

const ContactFormMobile = () => {
  const { t } = useTranslation('properties');

  const [name, setName] = useState<string>('');
  const [phone, setPhone] = useState<string>('');
  const [email, setEmail] = useState<string>('');
  const [isFormValid, setIsFormValid] = useState<boolean>(false);
  const [buttonText, setButtonText] = useState<string>(t('form-mob.submit-btn'));

  const validateName = (name: string): boolean => {
    return name.trim().length >= 2;
  };

  const validatePhone = (phone: string): boolean => {
    // Регулярний вираз для валідації міжнародного формату телефону
    const phoneRegex = /^\+?[1-9]\d{8,14}$/;
    return phoneRegex.test(phone);
  };

  useEffect(() => {
    if (validateName(name) && validatePhone(phone)) {
      setIsFormValid(true);
    } else {
      setIsFormValid(false);
    }
  }, [name, phone]);

  const handleNameChange = (event: ChangeEvent<HTMLInputElement>): void => {
    setName(event.target.value);
  };
  const handlePhoneChange = (event: ChangeEvent<HTMLInputElement>): void => {
    const value = event.target.value;
    // Дозволяємо вводити лише цифри після знака "+"
    if (/^\+?\d*$/.test(value)) {
      setPhone(value);
    }
  };
  const handlePhoneFocus = (): void => {
    if (!phone.startsWith('+')) {
      setPhone('+' + phone);
    }
  };
  const handleSubmit = async (event: FormEvent<HTMLFormElement>): Promise<void> => {
    event.preventDefault();
    if (isFormValid) {
      setButtonText('Sending'); // Зміна тексту кнопки на "Відправка форми"
      const formData = { name, phone };
      try {
        await postFormData(formData);
        setButtonText('Success'); // Зміна тексту кнопки на "Форма відправлена"
        setTimeout(() => {
          setButtonText(t('zoom.button')); // Відновлення початкового тексту кнопки через 5 секунд
        }, 5000);
        setName('');
        setPhone('');
      } catch (error) {
        console.error('Error submitting form:', error);
        setButtonText(t('zoom.button')); // Відновлення початкового тексту кнопки у разі помилки
      }
    }
  };

  return (
    <div className="contact__mobile">
      <h3 className="contact__mobile__title">{t('form-mob.title')}</h3>
      <p className="contact__mobile__text">{t('form-mob.text')}</p>
      <form className="contact__mobile__form" onSubmit={e => handleSubmit(e)}>
        <div className="contact__mobile__form__name">
          <input
            name="name"
            placeholder={t('name-placeholder')}
            type="text"
            id="name"
            value={name}
            onChange={handleNameChange}
            onBlur={e => validateName(e.target.value)}
            className={validateName(name) ? 'valid' : 'invalid'}
          ></input>
          <Image src="/assets/icons/user.svg" alt="User icon" width={24} height={24} />
        </div>
        <div className="contact__mobile__form__phone">
          <input
            name="tel"
            className={validatePhone(phone) ? 'valid' : 'invalid'}
            type="tel"
            placeholder={t('phone-placeholder')}
            value={phone}
            onChange={handlePhoneChange}
            onFocus={handlePhoneFocus}
            onBlur={e => validatePhone(e.target.value)}
          ></input>
          <Image src="/assets/icons/phone.svg" alt="Phone icon" width={24} height={24} />
        </div>
        <div className="contact__mobile__form__email">
          <input
            onChange={e => setEmail(e.target.value)}
            value={email}
            name="email"
            placeholder={t('email-placeholder')}
            type="text"
          ></input>
          <Image src="/assets/icons/mail.svg" alt="Mail icon" width={24} height={24} />
        </div>

        <button className="contact__mobile__form__btn" type="submit" disabled={!isFormValid}>
          {buttonText}
          <Image src="/assets/icons/done.svg" alt="Submit form" width={20} height={20} />
        </button>
      </form>
    </div>
  );
};

export default ContactFormMobile;
