'use client';
import './HelpForm.scss';
import ResponsiveImage from '@/components/ui/ResponsiveImage';
import ContactForm from './ContactForm';
import ContactFormMobile from './ContactFormMobile';

const HelpForm = () => {
  return (
    <div className="project__help-form">
      <div className="image">
        <ResponsiveImage
          src="/assets/images/form-bg.jpg"
          alt="Consulting form"
          width={550}
          height={430}
        />
      </div>
      <ContactForm />
      <ContactFormMobile />
    </div>
  );
};

export default HelpForm;
