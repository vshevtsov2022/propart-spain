'use client';
import { useRef, useState } from 'react';
import ImageGallery from 'react-image-gallery';
import 'react-image-gallery/styles/css/image-gallery.css';
import './galleryMainPhotos.scss';

interface GalleryMainPhotosProps {
  pictures: string[];
  openGallery: () => void;
  closeGallery: () => void;
  isVisible: boolean;
  galleryRef: any;
}

export const GalleryMainPhotos: React.FC<GalleryMainPhotosProps> = ({
  pictures,
  openGallery,
  closeGallery,
  isVisible,
  galleryRef,
}) => {
  const images = pictures.map(url => ({
    original: url,
    thumbnail: url,
  }));

  return (
    <>
      {isVisible && (
        <ImageGallery
          ref={galleryRef}
          items={images}
          showPlayButton={false}
          showFullscreenButton={true}
          showBullets={true}
          useBrowserFullscreen={true}
          onScreenChange={isFullScreen => {
            if (!isFullScreen) closeGallery();
          }}
        />
      )}
    </>
  );
};
