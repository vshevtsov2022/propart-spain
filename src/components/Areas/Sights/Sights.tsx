import React from 'react';
import Image from 'next/image';
import TouristsChoice from './TouristsChoice';
import './sights.scss';
import initTranslations from '@/app/i18n';
import { getAreasData } from '@/constants/areasData';
import Link from 'next/link';

export type Sight = {
  title: string;
  description: string;
  image: string;
  choice: boolean;
  url: string;
};

interface AreaHeaderProps {
  locale: string;
}

const Sights: React.FC<AreaHeaderProps> = async ({ locale }: any) => {
  const { t } = await initTranslations(locale, ['area']);
  const sights = getAreasData(t);
  return (
    <ul className="sights">
      {sights.map((sight, i) => (
        <li key={i} className="sight">
          <Link href={`/areas/${sight.url}`}>
            {sight.choice && <TouristsChoice locale={locale} />}

            <Image src={sight.image} alt={sight.title} fill sizes="100%" className="image" />
            <div className="sight-about">
              <h2 className="sight_name">{sight.title}</h2>
              <p className="sight_descr">{sight.description}</p>
            </div>
            <div className="gradient"></div>
          </Link>
        </li>
      ))}
    </ul>
  );
};

export default Sights;
