// import React, { useEffect, useRef, useState } from 'react';
// import Mapboxgl from 'mapbox-gl';
// import './propertyMap.scss';
// import 'mapbox-gl/dist/mapbox-gl.css';

// if (process.env.NEXT_PUBLIC_MAPBOX_ACCESS_TOKEN) {
//   Mapboxgl.accessToken = process.env.NEXT_PUBLIC_MAPBOX_ACCESS_TOKEN;
// } else {
//   console.error('Mapbox access token is not provided or invalid.');
// }

// interface PropertyMapProps {
//   lat: number;
//   lng: number;
// }

// const PropertyMap = ({ lat, lng }: PropertyMapProps) => {
//   const mapContainer = useRef<HTMLDivElement>(null);
//   const map = useRef<Mapboxgl.Map | null>(null);
//   const [widthScreen, setWidthScreen] = useState(window.innerWidth);

//   useEffect(() => {
//     const handleResize = () => setWidthScreen(window.innerWidth);
//     window.addEventListener('resize', handleResize);

//     return () => {
//       window.removeEventListener('resize', handleResize);
//     };
//   }, []);

//   useEffect(() => {
//     if (!map.current && mapContainer.current) {
//       map.current = new Mapboxgl.Map({
//         container: mapContainer.current,
//         style: 'mapbox://styles/abiespana/cltantp6a00ut01pj4v9h0srk',
//         center: [lng, lat],
//         zoom: 12,
//         dragPan: widthScreen > 767,
//       });

//       map.current.addControl(new Mapboxgl.NavigationControl(), 'top-right');

//       map.current.on('wheel', event => {
//         if (
//           event.originalEvent.ctrlKey ||
//           event.originalEvent.metaKey ||
//           event.originalEvent.altKey
//         ) {
//           return;
//         }
//         event.preventDefault();
//       });

//       map.current.on('load', () => {
//         new Mapboxgl.Marker({ color: '#DBA77B' })
//           .setLngLat([lng, lat])
//           .addTo(map.current!)
//           .getElement()
//           .classList.add('custom-marker');
//       });
//     }
//   }, [lng, lat]);

//   useEffect(() => {
//     if (map.current) {
//       if (widthScreen > 767) {
//         map.current.dragPan.enable();
//       } else {
//         map.current.dragPan.disable();
//       }
//       map.current.resize();
//     }
//   }, [widthScreen]);

//   return (
//     <div className="area__map-block">
//       <div ref={mapContainer} className="area__map"></div>
//     </div>
//   );
// };

// export default PropertyMap;

import React, { useEffect, useRef, useState } from 'react';
import Mapboxgl from 'mapbox-gl';
import './propertyMap.scss';
import 'mapbox-gl/dist/mapbox-gl.css';

if (process.env.NEXT_PUBLIC_MAPBOX_ACCESS_TOKEN) {
  Mapboxgl.accessToken = process.env.NEXT_PUBLIC_MAPBOX_ACCESS_TOKEN;
} else {
  console.error('Mapbox access token is not provided or invalid.');
}

interface PropertyMapProps {
  lat: number;
  lng: number;
}

const PropertyMap = ({ lat, lng }: PropertyMapProps) => {
  const mapContainer = useRef<HTMLDivElement>(null);
  const map = useRef<Mapboxgl.Map | null>(null);
  const [widthScreen, setWidthScreen] = useState(window.innerWidth);

  useEffect(() => {
    const handleResize = () => setWidthScreen(window.innerWidth);
    window.addEventListener('resize', handleResize);

    return () => {
      window.removeEventListener('resize', handleResize);
    };
  }, []);

  useEffect(() => {
    if (!map.current && mapContainer.current) {
      const mapOptions: Mapboxgl.MapboxOptions = {
        container: mapContainer.current,
        style: 'mapbox://styles/abiespana/cltantp6a00ut01pj4v9h0srk',
        zoom: 12,
        dragPan: widthScreen > 767,
      };

      if (!isNaN(lat) && !isNaN(lng)) {
        mapOptions.center = [lng, lat];
      } else {
        mapOptions.center = [0, 0]; // Some default center
        mapOptions.zoom = 1; // Default zoom if no coordinates provided
      }

      map.current = new Mapboxgl.Map(mapOptions);

      map.current.addControl(new Mapboxgl.NavigationControl(), 'top-right');

      map.current.on('wheel', event => {
        if (
          event.originalEvent.ctrlKey ||
          event.originalEvent.metaKey ||
          event.originalEvent.altKey
        ) {
          return;
        }
        event.preventDefault();
      });

      if (!isNaN(lat) && !isNaN(lng)) {
        map.current.on('load', () => {
          new Mapboxgl.Marker({ color: '#DBA77B' })
            .setLngLat([lng, lat])
            .addTo(map.current!)
            .getElement()
            .classList.add('custom-marker');
        });
      }
    }
  }, [lng, lat, widthScreen]);

  useEffect(() => {
    if (map.current) {
      if (widthScreen > 767) {
        map.current.dragPan.enable();
      } else {
        map.current.dragPan.disable();
      }
      map.current.resize();
    }
  }, [widthScreen]);

  return (
    <div className="area__map-block">
      <div ref={mapContainer} className="area__map"></div>
    </div>
  );
};

export default PropertyMap;
