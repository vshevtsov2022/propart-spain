'use client';

import { addToSelected } from '@/utils/addToSelected';
import Image from 'next/image';
import { useEffect, useState } from 'react';
import './OtherProjectsNew.scss';
import Link from 'next/link';
import CardSlider from '@/components/properties-page/OfferCard/CardSlider';
import { priceFormatter } from '@/utils/formatter';

export type OfferCardT = {
  id: string;
  title: string;
  price: string;
  currency: 'eur' | 'usd';
  beds: number[];
  flors: number;
  costPerSquareMeter: number;
  image: string;
  location: string;
};
export interface PropertiesProject {
  projectId: string;
  pictures: string[];
  name: string;
  price_from: number;
  size_m2: number;
  location: string;
  total_floors: number;
  floorPlanNames: string[];
  projectName: string;
}

interface DesctopCardProps {
  properties: PropertiesProject;
  setSelected: (flag: boolean) => void;
  selected: boolean;
}
interface OfferCardProps {
  // properties: PropertiesProject;
  properties: any;
}

export const OtherProjectsNewCard = ({ properties }: OfferCardProps) => {
  const [isSelected, setIsSelected] = useState(false);

  useEffect(() => {
    if (typeof window !== 'undefined') {
      const selected = JSON.parse(localStorage.getItem('selected') || '[]');
      setIsSelected(
        selected.some((item: PropertiesProject) => item.projectId === properties.projectId)
      );
    }
  }, [properties]);

  return (
    <OfferCardMobile selected={isSelected} setSelected={setIsSelected} properties={properties} />
  );
};

const ExtraInfoBlock = ({ properties }: OfferCardProps) => {
  const { floorPlanNames, size_m2, total_floors } = properties;
  return (
    <div className="info-extra">
      <p>
        <Image src="/assets/icons/beds.svg" alt="Beds count" width={18} height={18} />
        {Object.keys(properties.floorPlans).join(', ')} bed
      </p>
      <p>
        <Image src="/assets/icons/m2.svg" alt="cost Per Square Meter" width={18} height={18} />${' '}
        {String(properties.size_m2).replace(/\B(?=(\d{3})+(?!\d))/g, ',')} m²
      </p>
      {/*total_floors && total_floors > 0 && (
        <p>
          <Image src="/assets/icons/floors.svg" alt="Floors count" width={18} height={18} />
          {total_floors} floors
        </p>
      )*/}
    </div>
  );
};

export const OfferCardMobile = ({ properties, selected, setSelected }: DesctopCardProps) => {
  // const { location, name, pictures, price_from, projectId } = properties;
  const [swiperActive, setActive] = useState(true);
  const handleSelected = () => {
    addToSelected(properties);
    setSelected(!selected);
  };
  const formatProjectName = (name: string) => {
    if (!name) {
      return '';
    }
    const parts = name.split(',');
    const filteredParts = parts[0].replace('Development', '').trim();
    return filteredParts;
  };

  const formattedName = formatProjectName(properties.name);
  return (
    <div
      onMouseEnter={() => setActive(true)}
      onMouseLeave={() => setActive(false)}
      title={properties.name}
      className="projects__offer-card--mobile"
    >
      <Link href={`/properties/${properties.projectId}`}>
        <div className="image-wrapper">
          <CardSlider
            name={properties.name}
            pictures={properties.pictures}
            swiperActive={swiperActive}
            isAnother={true}
          />
          <ExtraInfoBlock properties={properties} />
        </div>
        <div className="info">
          <p className="info__title">{formattedName}</p>
          <p title={properties.location} className="info__location">
            {properties.location}
          </p>
          <p className="info__price">EUR {priceFormatter(properties.price_from, 'EUR')}</p>
        </div>
      </Link>
      <button className={`favorite-btn ${selected ? 'selected' : ''}`} onClick={handleSelected}>
        <svg
          width="21"
          height="22"
          viewBox="0 0 21 22"
          fill="none"
          xmlns="http://www.w3.org/2000/svg"
        >
          <path
            fillRule="evenodd"
            clipRule="evenodd"
            d="M20.2334 10.0206V15.4301C20.2334 18.7847 20.2334 20.462 19.4381 21.1949C19.0588 21.5445 18.5801 21.764 18.0701 21.8224C17.0007 21.9448 15.752 20.8403 13.2544 18.6312C12.1504 17.6548 11.5984 17.1665 10.9597 17.0379C10.6453 16.9746 10.3215 16.9746 10.0071 17.0379C9.36841 17.1665 8.81639 17.6548 7.71241 18.6312C5.21484 20.8403 3.96609 21.9448 2.89672 21.8224C2.38672 21.764 1.90796 21.5445 1.52868 21.1949C0.733399 20.462 0.733398 18.7847 0.733398 15.4301V10.0206C0.733398 5.37459 0.733399 3.05157 2.16125 1.60825C3.58911 0.164917 5.88721 0.164917 10.4834 0.164917C15.0796 0.164917 17.3777 0.164917 18.8055 1.60825C20.2334 3.05157 20.2334 5.37459 20.2334 10.0206ZM6.4209 4.49826C6.4209 4.04952 6.78469 3.68576 7.2334 3.68576H13.7334C14.1821 3.68576 14.5459 4.04952 14.5459 4.49826C14.5459 4.94696 14.1821 5.31076 13.7334 5.31076H7.2334C6.78469 5.31076 6.4209 4.94696 6.4209 4.49826Z"
            fill="currentColor"
          />
        </svg>
      </button>
    </div>
  );
};
