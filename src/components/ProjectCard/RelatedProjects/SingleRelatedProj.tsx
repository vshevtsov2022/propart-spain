// import React from 'react';
// import Image from 'next/image';
// import motion from 'framer-motion';
// import { IoIosArrowForward } from 'react-icons/io';

// const SingleRelatedProj = ({ obj }) => {
//   if (!obj) {
//     return null;
//   }
//   return (
//     <div>
//       <ul className="projects-list-item">
//         <li>
//           {' '}
//           <Image src={obj.image} alt={obj.type} width={226} height={84} className="image" />
//         </li>
//         <li>
//           <span>{obj.price}</span>
//         </li>
//         <li>
//           {' '}
//           <span>{obj.bed}</span>
//         </li>
//         <li>
//           {' '}
//           <span>{obj.bathrooms}</span>
//         </li>
//         <li>
//           {' '}
//           <span>{obj.size}</span>
//         </li>
//         <li>
//           {' '}
//           <span>{obj.projectFloor}</span>
//         </li>
//         <li className="arrow-flex-item">
//           {' '}
//           <span>{obj.type}</span>
//           <div>
//             <div
//               style={{
//                 backgroundColor: 'black',
//                 display: 'flex',
//                 justifyContent: 'center',
//                 alignItems: 'center',
//                 width: '34px',
//                 height: '34px',
//                 borderRadius: '100px',
//               }}
//             >
//               <div
//               // initial={{ x: 0 }}
//               // whileHover={{
//               //   x: 5,
//               //   transition: {
//               //     duration: 0.5,
//               //     type: 'spring',
//               //     stiffness: 300,
//               //     bounce: 0.7,
//               //   },
//               // }}
//               // exit={{ x: 0 }}
//               // style={{
//               //   display: 'flex',
//               //   justifyContent: 'center',
//               //   alignItems: 'center',
//               // }}
//               >
//                 <IoIosArrowForward fill="white" size={24} />
//               </div>
//             </div>
//           </div>
//         </li>
//       </ul>
//     </div>
//   );
// };

// export default SingleRelatedProj;
