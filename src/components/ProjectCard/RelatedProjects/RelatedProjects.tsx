'use client';

import React, { useState } from 'react';
import { useTranslation } from 'react-i18next';
import Image from 'next/image';
import { AnimatePresence, motion } from 'framer-motion';
import Modal from './Modal/Modal';

import { IoIosArrowForward } from 'react-icons/io';
import './relatedProjects.scss';
import RelatedProjectsTablet from './RelatedProjectsTablet';

interface Plan {
  image: string;
  name: string;
  location: string;
  description: {
    totalFloors: number | null;
    type: string | null;
    floor: number | null;
    area: number;
  };
  price_from: number;
  pricem2: number | null;
  planInfo: {
    room_name: string | null;
    condition: string | null;
    type: string | null;
    name: string;
    woodwork: string | null;
    floor: number | null;
    heatingType: string | null;
    hotWater: string | null;
    bedrooms: number;
    bathrooms: number;
    toilets: number;
    terrace: boolean;
    kitchen: string;
    buildedSurface: number;
    usefulSurface: number | null;
    terraceSurface: number;
  };
}

interface FloorPlans {
  [key: string]: {
    price_from: number;
    plans: Plan[];
  };
}

interface Project {
  floorPlans: FloorPlans;
}

interface RelatedProps {
  handleModalOpen: (plan: Plan) => void;
  handleModalClose: () => void;
  projects: Project;
}

const RelatedProjects: React.FC<RelatedProps> = ({ projects }) => {
  const { t } = useTranslation('project-card');
  const [modalOpen, setModalOpen] = useState(false);
  const [selectedProject, setSelectedProject] = useState<Plan | null>(null);

  const handleModalOpen = (plan: Plan) => {
    setSelectedProject(plan);
    setModalOpen(true);
  };

  const handleModalClose = () => {
    setModalOpen(false);
  };

  const params = [
    t('card.related.photo'),
    t('card.related.euro'),
    t('card.related.bed'),
    t('card.related.bathrooms'),
    t('card.related.size'),
    // t('card.related.floors'),
    // t('card.related.type-floors'),
  ];

  const floorPlansKeys = Object.keys(projects.floorPlans);
  const matchedFloorPlans = floorPlansKeys.map(key => projects.floorPlans[key].plans).flat();

  return (
    <>
      <div className="related">
        <AnimatePresence>
          {modalOpen && (
            <Modal
              modalOpen={modalOpen}
              handleModalClose={handleModalClose}
              selected={selectedProject}
            />
          )}
        </AnimatePresence>
        <div className="features">
          <ul className="related-objects-list">
            {floorPlansKeys.map((key, index) => (
              <li key={index} className="related-objects-block">
                <div className="related-objects">
                  <p>
                    <span>
                      {key} {t('card.related.bedrooms')}
                    </span>{' '}
                    {t('card.related.bedrooms-from')} {projects.floorPlans[key].price_from} €
                  </p>
                  <span>
                    {projects.floorPlans[key].plans.length} {t('card.related.dwellings')}
                  </span>
                </div>
                <ul className="related-features">
                  {params.map((param, i) => (
                    <li key={i}>
                      <span>{param}</span>
                    </li>
                  ))}
                  <li className="arrow-flex-item"></li>
                </ul>
                <div className="projects">
                  <ul className="projects-list">
                    {projects.floorPlans[key].plans.map((plan, i) => (
                      <li key={i} className="project-item" onClick={() => handleModalOpen(plan)}>
                        <ul className="projects-list-item">
                          <li className="image">
                            {/* <Image
                              src={plan?.image}
                              alt={plan?.name ? plan?.name : 'property photo'}
                              fill
                              sizes="100%"
                            /> */}
                            <div
                              style={{
                                backgroundImage: `url(${plan?.image})`,
                                backgroundRepeat: 'no-repeat',
                                backgroundSize: 'cover',
                                backgroundPosition: 'center',
                                width: '100%',
                                height: '100%',
                              }}
                            ></div>
                          </li>
                          <li>
                            {/* <span>
                              {plan?.price_from.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',')}
                            </span> */}
                            <span>
                              {plan?.price_from != null
                                ? plan.price_from.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',')
                                : ''}
                            </span>
                          </li>
                          <li>
                            <span>{plan?.planInfo?.bedrooms}</span>
                          </li>
                          <li>
                            <span>{plan?.planInfo?.bathrooms}</span>
                          </li>
                          <li>
                            <span>
                              {plan?.planInfo?.buildedSurface &&
                                plan?.planInfo?.buildedSurface
                                  .toString()
                                  .replace(/\B(?=(\d{3})+(?!\d))/g, ',')}{' '}
                              sq.m
                            </span>
                          </li>
                          {/* <li>
                            <span>{plan?.planInfo?.floor}</span>
                          </li>
                          <li>
                            <span>{plan?.planInfo?.type}</span>
                          </li> */}
                          <li className="arrow-flex-item">
                            <div>
                              <div
                                style={{
                                  backgroundColor: 'black',
                                  display: 'flex',
                                  justifyContent: 'center',
                                  alignItems: 'center',
                                  width: '34px',
                                  height: '34px',
                                  borderRadius: '100px',
                                }}
                              >
                                <motion.div
                                  initial={{ x: 0 }}
                                  whileHover={{
                                    x: 5,
                                    transition: {
                                      duration: 0.5,
                                      type: 'spring',
                                      stiffness: 300,
                                      bounce: 0.7,
                                    },
                                  }}
                                  exit={{ x: 0 }}
                                  style={{
                                    display: 'flex',
                                    justifyContent: 'center',
                                    alignItems: 'center',
                                  }}
                                >
                                  <IoIosArrowForward fill="white" size={24} />
                                </motion.div>
                              </div>
                            </div>
                          </li>
                        </ul>
                      </li>
                    ))}
                  </ul>
                </div>
              </li>
            ))}
          </ul>
        </div>
      </div>
      <RelatedProjectsTablet
        projects={projects}
        handleModalClose={handleModalClose}
        handleModalOpen={handleModalOpen}
      />
    </>
  );
};

export default RelatedProjects;
