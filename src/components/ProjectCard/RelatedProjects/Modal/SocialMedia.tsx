import React from 'react';
import Image from 'next/image';
import './modal.scss';

const media = [
  { title: 'WhatsApp', icon: '/images/whatsapp.svg', link: 'https://api.whatsapp.com/send/?phone=34695113333&text&type=phone_number&app_absent=0' },
  { title: 'Telegram', icon: '/images/telegram.svg', link: 'https://t.me/ppspain' },
  // {
  //   title: 'Via Messenger',
  //   icon: '/images/messenger.svg',
  //   link: 'https://m.me/your_facebook_page_id',
  // },
  { title: 'Contact', icon: '/images/contact.svg', link: 'mailto:noreply@pro-part.es' },
];

const SocialMedia: React.FC = () => {
  return (
    <div className="social">
      <ul className="social-media">
        {media.map((item, i) => (
          <li key={i} className="social-media-item">
            <a href={item.link} target="_blank" rel="noopener noreferrer">
              <Image src={item.icon} alt={item.title} width={25} height={25} />
              <span>{item.title}</span>
            </a>
          </li>
        ))}
      </ul>
      <a href="tel:+34695113333">
        <button className="call-button">Call us now</button>
      </a>
    </div>
  );
};

export default SocialMedia;
