'use client';
import { useRef } from 'react';
import { motion } from 'framer-motion';
import Image from 'next/image';
import { IoCloseOutline } from 'react-icons/io5';
import { Points } from '../../MainInfo/PropertyDetails/propertyPoints';
import PropertyDetails from '../../MainInfo/PropertyDetails/PropertyDetails';
import SocialMedia from './SocialMedia';
import { useClickOutside } from '@/utils/useClickOutside';
import './modal.scss';
import '../../MainInfo/PropertyDetails/propertyDetails.scss';
import useBodyScrollLock from '@/utils/useBodyScrollLock';

interface ModalProps {
  handleModalClose: () => void;
  selected: any;
  modalOpen: boolean;
}

const Modal = ({ selected, handleModalClose, modalOpen }: ModalProps) => {
  const refModal = useRef(null);
  useClickOutside(modalOpen, refModal, handleModalClose);
  useBodyScrollLock(modalOpen);
  if (!selected) return null;

  return (
    <motion.div
      className="backdrop"
      initial={{ x: -50, opacity: 0 }}
      animate={{ x: -10, opacity: 1 }}
      transition={{ duration: 0.2, type: 'keyframes' }}
      exit={{ x: -100, opacity: 0 }}
    >
      <div ref={refModal} className="modal">
        <button onClick={handleModalClose} className="close-icon">
          <IoCloseOutline size={24} color="#00000033" />
        </button>
        <div className="main-info">
          <div className="main-info-image">
            <div className="mobile-header">
              <h2 className="mobile-section-title">{selected?.name}</h2>
              <div className="mobile-location">
                <Image src="/images/location.svg" alt="location" width={25} height={25} />
                <h3>{selected?.location}</h3>
              </div>
            </div>
            <div className="image">
              {/* <Image
                src={selected.image}
                alt={selected.name ? selected.name : 'properties'}
                fill
                sizes="100%"
              /> */}
              <div
                style={{
                  backgroundImage: `url(${selected.image})`,
                  backgroundRepeat: 'no-repeat',
                  backgroundSize: 'contain',
                  backgroundPosition: 'center',
                  width: '100%',
                  height: '100%',
                }}
              ></div>
            </div>
          </div>
          <div className="main-info-points">
            <h2 className="section-title">{selected?.name}</h2>
            <div className="location">
              <Image src="/images/location.svg" alt="location" width={25} height={25} />
              <h3>{selected?.location}</h3>
            </div>
            <ul className="points-list">
              {Points.slice(0, -1).map((item, i) => (
                <PropertyDetails point={item} key={i} />
              ))}
            </ul>
            <h4 className="price-from">
              From <span className="price"> €{String(selected?.price_from).replace(/\B(?=(\d{3})+(?!\d))/g, ',')}</span>
            </h4>
            <h4 className="price-per-meter">Price per m² €{selected?.pricem2}</h4>
          </div>
        </div>
        <div className="secondary-info">
          <h2 className="section-title">Information about property</h2>
          <ul className="secondary-list">
            <li className="list-item">
              <h4>Name</h4>
              <h3>{selected?.planInfo?.name}</h3>
            </li>
            <li className="list-item">
              <h4>Floors</h4>
              <h3>{selected?.planInfo?.floor}</h3>
            </li>
            <li className="list-item">
              <h4>Bedrooms</h4>
              <h3>{selected.planInfo?.bedrooms}</h3>
            </li>
            <li className="list-item">
              <h4>Bathrooms</h4>
              <h3>{selected.planInfo?.bathrooms}</h3>
            </li>
            <li className="list-item">
              <h4>Builded surface</h4>
              <h3>{selected.planInfo?.buildedSurface}</h3>
            </li>
            <li className="list-item">
              <h4>Useful surface</h4>
              <h3>{selected.planInfo?.usefulSurface}</h3>
            </li>
            <li className="list-item">
              <h4>Type</h4>
              <h3>{selected.planInfo?.type}</h3>
            </li>
          </ul>
        </div>

        <SocialMedia />
      </div>
    </motion.div>
  );
};

export default Modal;
