// 'use client';

// import React, { useState, useRef } from 'react';
// import Image from 'next/image';
// import { AnimatePresence, motion } from 'framer-motion';
// import Modal from './Modal/Modal';

// import { IoIosArrowForward } from 'react-icons/io';
// import './relatedProjects.scss';
// import { useTranslation } from 'react-i18next';
// import { FloorPlan, Plan, Project } from '../MainInfo/types';
// import { useClickOutside } from '@/utils/useClickOutside';
// interface RelatedProps {
//   handleModalOpen: (plan: Plan) => void;
//   handleModalClose: () => void;
//   projects: Project;
// }

// const RelatedProjectsTablet: React.FC<any> = ({ projects }) => {
//   const refModal = useRef(null);
//   const { t } = useTranslation('project-card');
//   const [modalOpen, setModalOpen] = useState(false);
//   const [selectedProject, setSelectedProject] = useState<Plan | null>(null);

//   const handleModalOpen = (obj: Plan) => {
//     if (obj) {
//       setSelectedProject(obj);
//       setModalOpen(true);
//     }
//   };

//   const handleModalClose = () => {
//     setModalOpen(false);
//   };
//   useClickOutside(modalOpen, refModal, handleModalClose);
//   const params = [
//     t('card.related.photo'),
//     t('card.related.euro'),
//     t('card.related.bed'),
//     t('card.related.bathrooms'),
//     t('card.related.size'),
//     t('card.related.floors'),
//     t('card.related.type-floors'),
//   ];

//   const filledFloorPlanNames = projects.floorPlans.filledFloorPlanNames;
//   const floorPlans = projects.floorPlans;

//   const matchedFloorPlans = filledFloorPlanNames
//     .map(floorKey => {
//       const floorPlan = floorPlans[`_${floorKey}` as keyof typeof floorPlans];
//       return floorPlan ? floorPlan : null;
//     })
//     .filter(Boolean) as FloorPlan[];

//   return (
//     <>
//       <p className="projects-title">Property floor plans</p>
//       <div className="related-objects-tablet">
//         <AnimatePresence>
//           {modalOpen && (
//             <Modal
//               modalOpen={modalOpen}
//               handleModalClose={handleModalClose}
//               selected={selectedProject}
//             />
//           )}
//         </AnimatePresence>

//         <div>
//           <ul>
//             {projects.floorPlans.filledFloorPlanNames.map((item, i) => (
//               <li className="related-objects-tablet-list-item" key={i}>
//                 <div className="related-highlight">
//                   <p>
//                     <span>
//                       {item} {t('card.related.bedrooms')}
//                     </span>{' '}
//                     {t('card.related.bedrooms-from')} {matchedFloorPlans[0].priceFrom} €{' '}
//                   </p>
//                   <span>
//                     {matchedFloorPlans[i].plans.length} {t('card.related.dwellings')}
//                   </span>
//                 </div>
//                 <div className="projects">
//                   <ul className="projects-list">
//                     {matchedFloorPlans[i].plans.map((obj, i) => (
//                       <li key={i} className="project-item" onClick={() => handleModalOpen(obj)}>
//                         <div className="project-item-displayed">
//                           <Image src={obj.image} alt={obj.name} fill sizes="100%" />
//                         </div>

//                         <div className="project-item-options">
//                           <ul className="options-list">
//                             <li>
//                               <h3>Property Price</h3>
//                               <span>{obj.priceFrom}</span>
//                             </li>
//                             <li>
//                               <h3>Total bedrooms</h3>
//                               <span>{obj.planInfo?.bedrooms}</span>
//                             </li>
//                             <li>
//                               <h3>Total bathrooms</h3>
//                               <span>{obj.planInfo?.bedrooms}</span>
//                             </li>
//                             <li>
//                               <h3>Living space</h3>
//                               <span>{obj.planInfo?.buildedSurface}</span>
//                             </li>
//                             <li>
//                               <h3>Total floors</h3>
//                               <span>{obj.description?.floor}</span>
//                             </li>
//                             <li>
//                               <h3>Property type</h3>
//                               <span>{obj.description?.type}</span>
//                             </li>
//                           </ul>

//                           <button
//                             className="view-info"
//                             type="button"
//                             onClick={() => handleModalOpen(obj)}
//                           >
//                             View all information
//                             <IoIosArrowForward fill="white" size={24} />
//                           </button>
//                         </div>
//                       </li>
//                     ))}
//                   </ul>
//                 </div>
//               </li>
//             ))}
//           </ul>
//         </div>
//       </div>
//     </>
//   );
// };

// export default RelatedProjectsTablet;

'use client';

import React, { useState, useRef } from 'react';
import Image from 'next/image';
import { AnimatePresence, motion } from 'framer-motion';
import Modal from './Modal/Modal';

import { IoIosArrowForward } from 'react-icons/io';
import './relatedProjects.scss';
import { useTranslation } from 'react-i18next';
import { useClickOutside } from '@/utils/useClickOutside';

interface Plan {
  image: string;
  name: string;
  location: string;
  description: {
    totalFloors: number | null;
    type: string | null;
    floor: number | null;
    area: number;
  };
  price_from: number;
  pricem2: number | null;
  planInfo: {
    room_name: string | null;
    condition: string | null;
    type: string | null;
    name: string;
    woodwork: string | null;
    floor: number | null;
    heatingType: string | null;
    hotWater: string | null;
    bedrooms: number;
    bathrooms: number;
    toilets: number;
    terrace: boolean;
    kitchen: string;
    buildedSurface: number;
    usefulSurface: number | null;
    terraceSurface: number;
  };
}

interface FloorPlans {
  [key: string]: {
    price_from: number;
    plans: Plan[];
  };
}

interface Project {
  floorPlans: FloorPlans;
}

interface RelatedProps {
  handleModalOpen: (plan: Plan) => void;
  handleModalClose: () => void;
  projects: Project;
}

const RelatedProjectsTablet: React.FC<RelatedProps> = ({ projects }) => {
  const refModal = useRef(null);
  const { t } = useTranslation('project-card');
  const [modalOpen, setModalOpen] = useState(false);
  const [selectedProject, setSelectedProject] = useState<any | null>(null);

  const handleModalOpen = (plan: Plan) => {
    setSelectedProject(plan);
    setModalOpen(true);
  };

  const handleModalClose = () => {
    setModalOpen(false);
  };

  useClickOutside(modalOpen, refModal, handleModalClose);

  return (
    <>
      <p className="projects-title">Property floor plans</p>
      <div className="related-objects-tablet">
        <AnimatePresence>
          {modalOpen && (
            <Modal
              modalOpen={modalOpen}
              handleModalClose={handleModalClose}
              selected={selectedProject}
            />
          )}
        </AnimatePresence>
        <div>
          <ul>
            {Object.keys(projects.floorPlans).map((key, index) => (
              <li className="related-objects-tablet-list-item" key={index}>
                <div className="related-highlight">
                  <p>
                    <span>
                      {key} {t('card.related.bedrooms')}
                    </span>{' '}
                    {t('card.related.bedrooms-from')} {String(projects.floorPlans[key].price_from).replace(/\B(?=(\d{3})+(?!\d))/g, ',')} €{' '}
                  </p>
                  <span>
                    {projects.floorPlans[key].plans.length} {t('card.related.dwellings')}
                  </span>
                </div>
                <div className="projects">
                  <ul className="projects-list">
                    {projects.floorPlans[key].plans.map((plan, i) => (
                      <li key={i} className="project-item" onClick={() => handleModalOpen(plan)}>
                        <div className="project-item-displayed">
                          <div
                            style={{
                              backgroundImage: `url(${plan?.image})`,
                              backgroundRepeat: 'no-repeat',
                              backgroundSize: 'cover',
                              backgroundPosition: 'center',
                              width: '100%',
                              height: '100%',
                            }}
                          ></div>
                        </div>
                        <div className="project-item-options">
                          <ul className="options-list">
                            <li>
                              <h3>{t('card.related.item1')}</h3>
                              <span>
                                €{String(plan?.price_from).replace(/\B(?=(\d{3})+(?!\d))/g, ',')}
                              </span>
                            </li>
                            <li>
                              <h3>{t('card.related.item2')}</h3>
                              <span>{plan?.planInfo?.bedrooms}</span>
                            </li>
                            <li>
                              <h3>{t('card.related.item3')}</h3>
                              <span>{plan?.planInfo?.bathrooms}</span>
                            </li>
                            <li>
                              <h3>{t('card.related.item4')}</h3>
                              <span>{plan?.planInfo?.buildedSurface} m²</span>
                            </li>
                            <li>
                              <h3>{t('card.related.item5')}</h3>
                              <span>{plan?.description?.floor}</span>
                            </li>
                            <li>
                              <h3>{t('card.related.item6')}</h3>
                              <span>{plan?.description?.type}</span>
                            </li>
                          </ul>
                          <button
                            className="view-info"
                            type="button"
                            onClick={() => handleModalOpen(plan)}
                          >
                            {t('card.related.btn')}
                            <IoIosArrowForward fill="white" size={24} />
                          </button>
                        </div>
                      </li>
                    ))}
                  </ul>
                </div>
              </li>
            ))}
          </ul>
        </div>
      </div>
    </>
  );
};

export default RelatedProjectsTablet;
