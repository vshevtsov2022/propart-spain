'use client';
import { useTranslation } from 'react-i18next';
import { priceFormatter } from '@/utils/formatter';
import './mainInfo.scss';

interface MobileItemProps {
  item: { name: string; location: string; price_from: string; size_m2: string };
}

const MainMobileHeaderInfo: React.FC<MobileItemProps> = ({ item }) => {
  const { t } = useTranslation('project-card');

  return (
    <div className="mobile-header-info">
      <div className="mob-about">
        <h3 className="mob-title">{item.name}</h3>
        <h4 className="mob-location">{item.location}</h4>
      </div>
      <div className="mob-about" style={{ display: 'flex', flexDirection: 'column' }}>
        <span className="mob-price">
          {t('card.about.from-price')}{' '}
          <span className="mob-amount">€ {priceFormatter(+item.price_from, 'EUR')}</span>
        </span>
        <span className="mob-price-meter">
          € {priceFormatter(+item.size_m2, 'EUR')} {t('card.about.for-meter')} m<sup>2</sup>
        </span>
      </div>
    </div>
  );
};

export default MainMobileHeaderInfo;
