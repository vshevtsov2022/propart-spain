'use client';

import React, { useState } from 'react';
import { FaPhoneAlt } from 'react-icons/fa';
import { AnimatePresence } from 'framer-motion';
import { priceFormatter } from '@/utils/formatter';
import ImagesModal from '../ImagesModal/ImagesModal';
import SliderResponsive from '../Slider/SliderResponsive';
import PropertyDetails from '../PropertyDetails/PropertyDetails';
import useBodyScrollLock from '@/utils/useBodyScrollLock';
import './mainInfo.scss';
import { useTranslation } from 'react-i18next';
import MainMobileHeaderInfo from './MainMobileHeaderInfo';
import Link from 'next/link';

export interface ProjectType {
  id: string;
  image: string;
  name: string;
  size_m2: string;
  price_from: string;
  location: string;
  completion: string;
  handover: string;
  short_description: {
    total_floors: string;
    type: string;
    floor: string;
    rooms: string;
    area: string;
  };
  pricePerMeter: string;
  coordinates: {
    lng: string;
    lat: string;
  };
  bed: string;
  bathrooms: string;
  projectFloor: string;

  about: string;
  amenities: {
    id: string;
    image_code: string;
    name: string;
  }[];
  pictures: string[];
}

interface MainInfoProps {
  item: ProjectType;
  handleModalOpen: () => void;
  handleModalClose: () => void;
  secondary?: boolean;
}

const convertDateToQuarter = (dateString: string | null): string => {
  if (!dateString) {
    return '';
  }

  const date = new Date(dateString);
  if (isNaN(date.getTime())) {
    return '';
  }

  const year = date.getUTCFullYear();
  const month = date.getUTCMonth();

  const quarter = Math.floor(month / 3) + 1;

  return `Q${quarter} ${year}`;
};

const MainInfo: React.FC<MainInfoProps> = ({ item, secondary }) => {
  const [modalOpen, setModalOpen] = useState(false);
  const { t } = useTranslation('project-card');
  const handleModalOpen = () => {
    setModalOpen(true);
  };

  const handleModalClose = () => {
    setModalOpen(false);
  };
  useBodyScrollLock(modalOpen);

  const points = [
    {
      icon: '/images/floors.svg',
      pointName: t('card.about.total-floors'),
      pointDetail: item.short_description.total_floors || '-',
    },
    {
      icon: '/images/type.svg',
      pointName: t('card.about.type'),
      pointDetail: item.short_description.type || '-',
    },
    {
      icon: '/images/floor.svg',
      pointName: t('card.about.floor'),
      pointDetail: item.short_description.floor || '-',
    },
    {
      icon: '/images/size.svg',
      pointName: t('card.about.size'),
      pointDetail: `${item.short_description.area} m²` || '-',
    },

    {
      icon: '/images/typeNew.svg',
      pointName: t('card.about.plan'),
      pointDetail: item.completion || '-',
    },
    {
      icon: '/images/handover.svg',
      pointName: t('card.about.handover'),
      // pointDetail: item.handover,
      pointDetail: convertDateToQuarter(item?.handover) || '-',
    },

    {
      icon: '/images/rooms.svg',
      pointName: t('card.about.rooms'),
      pointDetail: item.short_description.rooms || '-',
    },
  ];

  const pointsSecondary = [
    {
      icon: '/images/size.svg',
      pointName: t('card.about.size'),
      pointDetail: `${item.short_description.area} m²` || '-',
    },
    {
      icon: '/images/typeNew.svg',
      pointName: t('card.about.plan'),
      pointDetail: item.completion || '-',
    },
    {
      icon: '/images/rooms.svg',
      pointName: t('card.about.rooms'),
      pointDetail: item.short_description.rooms || '-',
    },
  ];

  return (
    <div>
      <div className="project-hero">
        <MainMobileHeaderInfo item={item} />
        {/* <AnimatePresence>
          {modalOpen && <ImagesModal data={item.pictures} handleModalClose={handleModalClose} />}
        </AnimatePresence> */}

        <SliderResponsive data={item} handleModalOpen={handleModalOpen} secondary={secondary} />

        <div className="about-project">
          <span className="price">
            {t('card.about.from-price')}{' '}
            <span className="amount">€ {priceFormatter(+item.price_from, 'EUR')}</span>
          </span>
          <span className="price-meter">
            € {priceFormatter(+item.size_m2, 'EUR')} {t('card.about.for-meter')} m<sup>2</sup>
          </span>
          <h3 className="project-title">{item.name}</h3>
          <h4 className="project-location">{item.location}</h4>
          {secondary ? (
            <ul className={`points-list ${secondary ? 'secondary' : ''}`}>
              {pointsSecondary.map((point, i) => (
                <PropertyDetails point={point} key={i} />
              ))}
            </ul>
          ) : (
            <ul className={`points-list ${secondary ? 'secondary' : ''}`}>
              {points.map((point, i) => (
                <PropertyDetails point={point} key={i} />
              ))}
            </ul>
          )}

          <div className="buttons">
            <Link
              target="_blank"
              rel="noopener noreferrer nofollow"
              href="tel:+34695113333"
              className="buttons-contact"
            >
              {t('card.about.contact-btn')}
            </Link>
            <Link
              target="_blank"
              rel="noopener noreferrer nofollow"
              href="tel:+34695113333"
              className="buttons-icon"
            >
              <FaPhoneAlt width={18} height={18} className="phone" />
            </Link>
          </div>
        </div>

        <div className="about-project-tablet">
          {secondary ? (
            <ul className={`points-list ${secondary ? 'secondary' : ''}`}>
              {pointsSecondary.map((point, i) => (
                <PropertyDetails point={point} key={i} />
              ))}
            </ul>
          ) : (
            <ul className={`points-list ${secondary ? 'secondary' : ''}`}>
              {points.map((point, i) => (
                <PropertyDetails point={point} key={i} />
              ))}
            </ul>
          )}

          <div className="about-project-tablet-details">
            <div className="about">
              <h3 className="project-title">{item.name}</h3>
              <h4 className="project-location">{item.location}</h4>
            </div>
            <div className="about">
              <span className="price">
                {t('card.about.from-price')}{' '}
                <span className="amount">€ {priceFormatter(+item.price_from, 'EUR')}</span>
              </span>
              <span className="price-meter">
                € {priceFormatter(+item.size_m2, 'EUR')} {t('card.about.for-meter')} m<sup>2</sup>
              </span>
            </div>

            <div className="buttons">
              <Link
                target="_blank"
                rel="noopener noreferrer nofollow"
                href="tel:+34695113333"
                className="buttons-contact"
              >
                {t('card.about.contact-btn')}
              </Link>

              <Link
                target="_blank"
                rel="noopener noreferrer nofollow"
                href="tel:+34695113333"
                className="buttons-icon"
              >
                <FaPhoneAlt width={18} height={18} className="phone" />
              </Link>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default MainInfo;
