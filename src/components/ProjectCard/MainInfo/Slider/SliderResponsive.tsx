'use client';
import { useRef, useState, useEffect } from 'react';
import { Swiper, SwiperSlide } from 'swiper/react';
import 'swiper/css';
import 'swiper/css/navigation';
import 'swiper/css/pagination';
import 'swiper/css/scrollbar';
import './sliderResponsive.scss';
import { addToSelected } from '@/utils/addToSelected';
import { GalleryMainPhotos } from '@/components/GalleryMainPhotos';
import ImageGallery from 'react-image-gallery';

interface SliderProps {
  data: any;
  handleModalOpen: () => void;
  secondary?: boolean;
}

const useWidthScreenValue = () => {
  const [widthScreen, setWidthScreen] = useState(window.innerWidth);

  useEffect(() => {
    const handleResize = () => {
      setWidthScreen(window.innerWidth);
    };

    window.addEventListener('resize', handleResize);

    return () => {
      window.removeEventListener('resize', handleResize);
    };
  }, []);

  const getWidthScreenValue = () => {
    if (widthScreen > 1439) {
      return 585;
    } else if (widthScreen > 767) {
      return 390;
    } else {
      return 303;
    }
  };

  return getWidthScreenValue;
};

const SliderResponsive: React.FC<SliderProps> = ({ data, handleModalOpen, secondary }) => {
  const swiperRef = useRef<any>(null);
  const [currentIndex, setCurrentIndex] = useState(0);
  const { name, location, price_from, size_m2, short_description, floorPlans, pictures } = data;
  const [isVisible, setIsVisible] = useState(false);
  const galleryRef = useRef<ImageGallery>(null);
  const cardData = {
    projectId: data.projectId,
    name,
    location,
    price_from,
    size_m2,
    total_floors: short_description.total_floors,
    floorPlanNames: floorPlans.filledFloorPlanNames,
    pictures: pictures,
  };
  const widthScreenValue = useWidthScreenValue();

  const handlePrevButtonClick = (e: React.MouseEvent) => {
    e.stopPropagation();
    if (swiperRef.current && swiperRef.current.swiper) {
      setCurrentIndex(prevIndex => prevIndex - 1);
      swiperRef.current.swiper.slidePrev();
    }
  };

  const handleNextButtonClick = (e: React.MouseEvent) => {
    e.stopPropagation();
    if (swiperRef.current && swiperRef.current.swiper) {
      setCurrentIndex(prevIndex => prevIndex + 1);
      swiperRef.current.swiper.slideNext();
    }
  };

  const [isSelected, setIsSelected] = useState(() => {
    const selected = JSON.parse(localStorage.getItem('selected') || '[]');
    return selected.some((item: any) => item.projectId === data.projectId);
  });

  const handleSelected = () => {
    addToSelected(cardData);
    setIsSelected(!isSelected);
  };

  const openGallery = () => {
    setIsVisible(true);
    setTimeout(() => {
      if (galleryRef.current) {
        galleryRef.current.fullScreen();
      }
    }, 0);
  };

  const closeGallery = () => {
    setIsVisible(false);
    // if (galleryRef.current) {
    //   galleryRef.current.exitFullScreen();
    // }
    if (document.fullscreenElement != null) {
      // Проверяем, находится ли документ в полноэкранном режиме
      if (galleryRef.current) {
        galleryRef.current.exitFullScreen();
      }
    }
  };

  return (
    <div className="project-image-card">
      <div className="swiper__mainWrapper-buttonsWrapper" onClick={openGallery}>
        <button
          className="swiper__mainWrapper-btn"
          onClick={handlePrevButtonClick}
          disabled={currentIndex === 0}
        >
          <svg xmlns="http://www.w3.org/2000/svg" width="17" height="17" fill="none">
            <path
              fill="white"
              d="m4.328 7.5 5.364-5.364L8.278.722.5 8.5l7.778 7.778 1.414-1.414L4.328 9.5H16.5v-2H4.328Z"
            />
          </svg>
        </button>
        <button
          className="swiper__mainWrapper-btn"
          onClick={handleNextButtonClick}
          disabled={currentIndex === pictures.length}
        >
          <svg xmlns="http://www.w3.org/2000/svg" width="17" height="17" fill="none">
            <path
              fill="white"
              d="M12.672 7.5 7.308 2.136 8.722.722 16.5 8.5l-7.778 7.778-1.414-1.414L12.672 9.5H.5v-2h12.172Z"
            />
          </svg>
        </button>
      </div>
      <Swiper
        ref={swiperRef}
        spaceBetween={25}
        slidesPerView={'auto'}
        style={{ borderRadius: '16px' }}
        className={`swiper__mainWrapper ${secondary ? 'secondary' : ''}`}
        onSlideChange={swiper => {
          setCurrentIndex(swiper.realIndex);
        }}
      >
        {pictures.map((item: any, i: string) => (
          <SwiperSlide key={i} onClick={openGallery}>
            <img
              src={item}
              alt={`image ${i}`}
              style={{
                width: '100%',
                objectFit: 'cover',
                borderRadius: '16px',
                objectPosition: 'bottom',
                cursor: 'pointer',
              }}
              height={widthScreenValue()}
              className={`image-main ${secondary ? 'secondary' : ''}`}
            />
          </SwiperSlide>
        ))}
      </Swiper>
      <button className={`favoriteButton ${isSelected ? 'selected' : ''}`} onClick={handleSelected}>
        <svg
          width="21"
          height="22"
          viewBox="0 0 21 22"
          fill="none"
          xmlns="http://www.w3.org/2000/svg"
        >
          <path
            fillRule="evenodd"
            clipRule="evenodd"
            d="M20.2334 10.0206V15.4301C20.2334 18.7847 20.2334 20.462 19.4381 21.1949C19.0588 21.5445 18.5801 21.764 18.0701 21.8224C17.0007 21.9448 15.752 20.8403 13.2544 18.6312C12.1504 17.6548 11.5984 17.1665 10.9597 17.0379C10.6453 16.9746 10.3215 16.9746 10.0071 17.0379C9.36841 17.1665 8.81639 17.6548 7.71241 18.6312C5.21484 20.8403 3.96609 21.9448 2.89672 21.8224C2.38672 21.764 1.90796 21.5445 1.52868 21.1949C0.733399 20.462 0.733398 18.7847 0.733398 15.4301V10.0206C0.733398 5.37459 0.733399 3.05157 2.16125 1.60825C3.58911 0.164917 5.88721 0.164917 10.4834 0.164917C15.0796 0.164917 17.3777 0.164917 18.8055 1.60825C20.2334 3.05157 20.2334 5.37459 20.2334 10.0206ZM6.4209 4.49826C6.4209 4.04952 6.78469 3.68576 7.2334 3.68576H13.7334C14.1821 3.68576 14.5459 4.04952 14.5459 4.49826C14.5459 4.94696 14.1821 5.31076 13.7334 5.31076H7.2334C6.78469 5.31076 6.4209 4.94696 6.4209 4.49826Z"
            fill="currentColor"
          />
        </svg>
      </button>
      <button className="all-photos" onClick={openGallery}>
        See all photos
      </button>
      <GalleryMainPhotos
        pictures={pictures}
        openGallery={openGallery}
        closeGallery={closeGallery}
        isVisible={isVisible}
        galleryRef={galleryRef}
      />
    </div>
  );
};

export default SliderResponsive;
