'use client';
import React from 'react';
import Link from 'next/link';
import './OtherProjectsNew.scss';
import Image from 'next/image';
import { useTranslation } from 'react-i18next';
import { OtherProjectsNewCard } from '../OtherProjectsNewCard/OtherProjectsNew';
interface OtherProjectsNewProps {
  offersData: any;
  title: string;
}

const OtherProjectsNew = ({ offersData, title }: OtherProjectsNewProps) => {
  const { t } = useTranslation('properties');
  return (
    <div className="other-projects-new">
      <h2 className='title'>{title}</h2>
      <div className="projects__offers-list">
        {offersData &&
          Array.isArray(offersData) &&
          offersData.length > 0 &&
          offersData.map((item: any, i) => {
            return <OtherProjectsNewCard properties={item} key={item.projectId + i} />;
          })}
      </div>

      <Link href={'/projects-map'} className="map__btn">
        <div className="circle-mask" />
        <Image src="/assets/icons/earth.svg" alt="Map Icon" width={24} height={24} />
        {t('offers-list.map-btn')}
      </Link>
    </div>
  );
};

export default OtherProjectsNew;
