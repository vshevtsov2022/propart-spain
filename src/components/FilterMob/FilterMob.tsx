'use client'; //переробив компонент щоб store оновлювався при submit

import { useEffect, useState } from 'react';
import Image from 'next/image';
import { usePathname, useRouter } from 'next/navigation';
import MapSearch from '@/components/ui/MapSearch/MapSearch';
import useFilterStore, { FilterState, PriceType } from '@/store/filterStore';
import HeroFilterRadioButtons from '../mainPage/Hero/HeroFilterRadiButtons/HeroFilterRadioButtons';
import DropdownCheckerMob from '../ui/DropdownCheckerMob/DropdownCheckerMob';
import PriceCheckerMob from '../ui/PriceCheckerMob/PriceCheckerMob';
import { useTranslation } from 'react-i18next';
import './FilterMob.scss';
import {
  getTypeOptions,
  getBedroomOptions,
  priceOptions,
  sizeOptions,
  areasOptions,
  areasOptionsSubareas,
} from '@/constants/filters';
import { allAreasList } from '@/data/allAreasList';
import PopupErrorPolygons from '../PopupErrorPolygons';
import DropdownAreaMob from '../ui/DropdownCheckerMob/DropdownAreaMob';

interface FilterMobProps {
  onClose: () => void;
}

const FilterMob = ({ onClose }: FilterMobProps) => {
  const { t, i18n } = useTranslation('map-filters');
  const currentPathname = usePathname();
  const router = useRouter();
  const typeOptions = getTypeOptions(t);
  const bedroomOptions = getBedroomOptions(t);
  const [popupShow, setPopupShow] = useState(false);

  const {
    projects, // список проектів
    searchParams,
    selectedRadio,
    selectedName,
    selectedType,
    selectedBeds,
    selectedPrice,
    selectedSize,
    selectedArea,
    reset,
    setFilter,
    toggleReset,
    setProjects,
    setTempArea,
    tempArea,
    polygonsProjects,
  } = useFilterStore();

  // Локальний стан для тимчасового збереження значень фільтрів
  const [localFilters, setLocalFilters] = useState({
    searchParams,
    selectedRadio,
    selectedName,
    selectedType,
    selectedBeds,
    selectedPrice,
    selectedSize,
    selectedArea,
  });

  const handleFilterChange = (field: keyof FilterState, value: any) => {
    setLocalFilters(prevState => ({ ...prevState, [field]: value }));
  };

  const handleClearFilter = () => {
    setLocalFilters({
      searchParams: '',
      selectedRadio: 'newBuilding',
      selectedName: '',
      selectedType: [],
      selectedBeds: [],
      selectedPrice: { min: null, max: null },
      selectedSize: { min: null, max: null },
      selectedArea: [],
    });
    toggleReset();
    setTempArea(allAreasList);
  };

  const handleSubmitFilter = () => {
    // Оновлення state при submit
    Object.keys(localFilters).forEach(key => {
      setFilter(key as keyof FilterState, localFilters[key as keyof typeof localFilters]);
    });

    // Перевірка URL і відповідно до цього перенаправлення на різні сторінки
    const propertiesRegex = /\/properties/;
    const projectsMapRegex = /\/projects-map/;
    // Переконайтесь, що currentPathname є строкою перед виконанням перевірки
    if (
      currentPathname &&
      !propertiesRegex.test(currentPathname) &&
      !projectsMapRegex.test(currentPathname)
    ) {
      router.push('/properties');
    }
    onClose();
  };

  const handleClickPolygons = (e: React.MouseEvent) => {
    if (polygonsProjects.length > 0) {
      setPopupShow(true);
      e.stopPropagation();
    }
  };

  return (
    <div className="filter-mob">
      <div className="modal-content__filter">
        <div className="filters-block-mob-header">
          <button onClick={onClose} className="filters-block-mob-header-close">
            <svg
              xmlns="http://www.w3.org/2000/svg"
              width="24"
              height="24"
              viewBox="0 0 24 24"
              fill="none"
            >
              <path
                d="M7.828 11.0007H20V13.0007L7.828 13.0007L13.192 18.3647L11.778 19.7787L4 12.0007L11.778 4.22266L13.192 5.63666L7.828 11.0007Z"
                fill="#09121F"
              ></path>
            </svg>
          </button>
          <button onClick={handleClearFilter} className="filters-block-mob-clear-filter">
            {t('mob_clear')}
            <Image src="/assets/icons/clear.svg" alt="Clear Icon" width={20} height={20} />
          </button>
        </div>
        <h3 className="filters-block-mob-title">{t('mob_title')}</h3>
        <div className="filter-block-mob-form" onClick={handleClickPolygons}>
          <div
            className="filter-block-mob-form-radio"
            style={polygonsProjects.length > 0 ? { pointerEvents: 'none' } : {}}
          >
            <HeroFilterRadioButtons
              radio1={t('hero.filter-radio1')}
              radio2={t('hero.filter-radio2')}
              selectedType={localFilters.selectedRadio}
              action={(value: string) => handleFilterChange('selectedRadio', value)}
            />
          </div>
          <div
            className="search-mob"
            style={polygonsProjects.length > 0 ? { pointerEvents: 'none' } : {}}
          >
            <MapSearch
              handleSearch={value => handleFilterChange('searchParams', value)}
              handleNameChange={(name: string, coordinates: [number, number]) =>
                handleFilterChange('selectedName', name)
              }
              projectsNameData={projects}
              reset={reset}
            />
          </div>
          <div
            className="dropdown"
            style={polygonsProjects.length > 0 ? { pointerEvents: 'none' } : {}}
          >
            <DropdownCheckerMob
              data={typeOptions}
              title={t('type_title')}
              text={t('type_text')}
              selectValue={localFilters.selectedType}
              action={(values: string[]) => handleFilterChange('selectedType', values)}
            />
          </div>

          <div
            className="dropdown"
            style={polygonsProjects.length > 0 ? { pointerEvents: 'none' } : {}}
          >
            <DropdownCheckerMob
              data={bedroomOptions}
              title={t('beds_title')}
              text={t('beds_text')}
              selectValue={localFilters.selectedBeds}
              action={(values: string[]) => handleFilterChange('selectedBeds', values)}
            />
          </div>
          <div
            className="dropdown"
            style={polygonsProjects.length > 0 ? { pointerEvents: 'none' } : {}}
          >
            <DropdownAreaMob
              data={areasOptionsSubareas}
              title={t('area_title')}
              text={t('area_text')}
              selectValue={localFilters.selectedArea}
              action={(values: string[]) => {
                handleFilterChange('selectedArea', values);
                setTempArea(values);
              }}
            />
          </div>
          <div
            className="dropdown"
            style={polygonsProjects.length > 0 ? { pointerEvents: 'none' } : {}}
          >
            <PriceCheckerMob
              data={sizeOptions}
              title={t('size_title')}
              text={t('size_text')}
              type="Size"
              selectValue={localFilters.selectedSize}
              action={(values: PriceType) => handleFilterChange('selectedSize', values)}
            />
          </div>

          <div
            className="dropdown"
            style={polygonsProjects.length > 0 ? { pointerEvents: 'none' } : {}}
          >
            <PriceCheckerMob
              data={priceOptions}
              title={t('price_title')}
              text={t('price_text')}
              type="Price"
              selectValue={localFilters.selectedPrice}
              action={(values: PriceType) => handleFilterChange('selectedPrice', values)}
            />
          </div>

          <button className="filters-block-mob-submit" onClick={handleSubmitFilter}>
            {t('mob_submit')}
            <svg
              xmlns="http://www.w3.org/2000/svg"
              width="24"
              height="24"
              viewBox="0 0 24 24"
              fill="none"
            >
              <path
                d="M12 22C6.477 22 2 17.523 2 12C2 6.477 6.477 2 12 2C17.523 2 22 6.477 22 12C22 17.523 17.523 22 12 22ZM11.003 16L18.073 8.929L16.659 7.515L11.003 13.172L8.174 10.343L6.76 11.757L11.003 16Z"
                fill="white"
              ></path>
            </svg>
          </button>
        </div>
        <PopupErrorPolygons handler={setPopupShow} isVisible={popupShow} />
      </div>
    </div>
  );
};

export default FilterMob;
