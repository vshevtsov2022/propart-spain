const i18nConfig = {
  locales: ['en', 'uk', 'ru', 'da', 'fr', 'no', 'sw', 'de', 'es', 'pl', 'it', 'ar'],
  defaultLocale: 'en',
};

module.exports = i18nConfig;
