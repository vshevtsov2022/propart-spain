const siteUrl = process.env.SITE_URL || 'https://pro-part.es';

module.exports = {
  siteUrl,
  generateRobotsTxt: true,
  sitemapSize: 7000,
  changefreq: 'daily',
  priority: 0.7,
  alternateRefs: [
    {
      href: `${siteUrl}/en`,
      hreflang: 'en',
    },
    {
      href: `${siteUrl}/uk`,
      hreflang: 'uk',
    },
    {
      href: `${siteUrl}/ru`,
      hreflang: 'ru',
    },
    {
      href: `${siteUrl}/da`,
      hreflang: 'da',
    },
    {
      href: `${siteUrl}/fr`,
      hreflang: 'fr',
    },
    {
      href: `${siteUrl}/no`,
      hreflang: 'no',
    },
    {
      href: `${siteUrl}/sw`,
      hreflang: 'sw',
    },
    {
      href: `${siteUrl}/de`,
      hreflang: 'de',
    },
    {
      href: `${siteUrl}/es`,
      hreflang: 'es',
    },
    {
      href: `${siteUrl}/pl`,
      hreflang: 'pl',
    },
  ],
  additionalPaths: async config => {
    const staticPaths = [
      '/',
      '/properties',
      '/projects-map',
      '/areas',
      'areas/fuengirola',
      'areas/estepona',
      'areas/benahavis',
      'areas/mijas',
      'areas/marbella',
      'areas/new-golden-mile',
      'areas/golden-mile',
      'areas/nueva-andalucía',
      '/consulting',
      '/visa',
      '/services',
      '/insurance',
      '/repairs-and-furnishings',
      '/nota-simple',
      '/tourist-license',
      '/consierge-services',
      '/mortgage',
      '/student-visa',
      '/digital-nomand',
      '/construction',
      '/saved-projects',
      '/error',
      // '/wp-admin',
      // '/secondary-properties',
    ];
    return [...staticPaths].map(path => ({
      loc: `${siteUrl}${path}`,
      changefreq: 'daily',
      priority: 0.8,
      lastmod: new Date().toISOString(),
      alternateRefs: config.alternateRefs,
    }));
  },
  transform: async (config, path) => {
    let priority = config.priority;
    if (path === '/') {
      priority = 1.0; // Головна сторінка має найвищий пріоритет
    } else if (path.startsWith('/properties')) {
      priority = 0.8; // properties пріоритет
    }
    return {
      loc: `${siteUrl}${path}`,
      changefreq: config.changefreq,
      priority: priority,
      lastmod: config.autoLastmod ? new Date().toISOString() : undefined,
      alternateRefs: config.alternateRefs,
    };
  },
};
